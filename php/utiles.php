<?php
	$Conser =array(	"1"	=>	"1.00000",
			"2"	=>	"0.87500",
			"3"	=>	"0.75000",
			"4"	=>	"0.40000",
			"5"	=>	"0.25000",
			"6"	=>	"0.08000"
			);
	$Mesnualidad =array("2006"	=>	"0.0033",
			"2007"		=>	"0.0033",
			"2008"		=>	"0.0033",
			"2009"		=>	"0.0040",
			"2010"		=>	"0.0040",
			"2011"		=>	"0.0036",
			"2012"		=>	"0.0036");
	$Uso	=array("1"		=>	"INDUSTRIAL",
			"2"		=>	"COMERCIO",
			"3"		=>	"SERVICIOS",
			"4"		=>	"BODEGA");
	$Dimenciones =array("1"		=>	"MICRO",
			"2"		=>	"PEQUE&Ntilde;A",
			"3"		=>	"MEDIANA",
			"4"		=>	"GRANDE");
function CabTable($titulo,$width)
{
	$html='<table border="0" align="center" width="'.$width.'">
	<tr>
		<td align="center" width=8px" height="16px" style="background-image: url(imags/imagsinscripcion/isi.jpg)">
		</td>
            	<td align="center" style="background-image: url(imags/imagsinscripcion/is.jpg)">
		</td>
		<td align="center" width="7px" height="16px" style="background-image: url(imags/imagsinscripcion/isd.jpg)">
		</td>
	</tr>
	<tr>
		<td align="center" style="background-image: url(imags/imagsinscripcion/ili.jpg)">
		</td>
		<td style="text-align: center;"><b>'.$titulo.'</b>
		</td>
		<td align="center" style="background-image: url(imags/imagsinscripcion/ild.jpg)">
		</td>
	</tr>
	<tr>
		<td align="center" style="background-image: url(imags/imagsinscripcion/ili.jpg)">
		</td><td>';
	return $html;
}
function PieTable()
{
	$html='	</td>
		<td align="center" style="background-image: url(imags/imagsinscripcion/ild.jpg)">
		</td>
	</tr>
	<tr>
		<td align="center" width="8px" height="10px" style="background-image: url(imags/imagsinscripcion/iii.jpg); background-repeat:  no-repeat;">
		</td>
		<td align="center" style="background-image: url(imags/imagsinscripcion/ii.jpg);">
		</td>
		<td align="center" width="8px" height="10px" style="background-image: url(imags/imagsinscripcion/iid.jpg); background-repeat:  no-repeat;">
		</td>
	</tr>
</table>';
	return $html;
}

function BuscarRecibos($id)
{
	$consultas = new CONSULTAS;
	global $_MES;
	$campos = array("pago_predial.*","pago_predial_liq.*");
	$condiciones = array('status_p_p' => 2, 'id_predial'=>$id,'status_folio'=>'1');
	$inner=array("pago_predial:pago_predial_liq" => "pago_predial.id_pago=pago_predial_liq.id_solicitud");
	if($consultas->QueryINNERJOIN($inner, $campos, $condiciones, "", "id_pago DESC LIMIT 1") == CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$folio_a = $registro["folio_a"];
			$folio_n = $registro["folio_n"];
			$fecha_p = $registro["fecha"];
			list($fecha_pag,$hora)=split(" ",$fecha_p);
			list($ano_p,$mes_p,$dia_p)=split("-",$fecha_pag);
			$fec_pag = "$dia_p de ".$_MES[$mes_p]." de $ano_p";
			$html='<br />Fecha de Pago :<input name="fecha" type="text" id="fecha"  size="20" maxlength="20" value="'.$fec_pag.'"
				disabled=\'disabled\'  style="color: #000000; font-size: 13px; font-weight:bold;"><br /><br />
				Ultimo Recibo: <input name="foli_a" type="text" id="foli_a"  size="2" maxlength="2" value="'.$folio_a.'" disabled="disabled">
				<input name="folio_a" type="hidden" id="folio_a" value="'.$folio_a.'" >';
			$html.='<input name="foli_n" type="text" id="foli_n"  size="6" value="'.$folio_n.'" disabled="disabled">
				<input name="folio_n" type="hidden" id="folio_n"  value="'.$folio_n.'" ><br />';
		}
	}else{
		$consultas = new CONSULTAS;
		global $_MES;
		$campos = array("pago_predial_2012.*","pago_predial_liq_2012.*");
		$condiciones = array('status_p_p' => 2, 'id_predial'=>$id,'status_folio'=>'1');
		$inner=array("pago_predial_2012:pago_predial_liq_2012" => "pago_predial_2012.id_pago=pago_predial_liq_2012.id_solicitud");
		if($consultas->QueryINNERJOIN($inner, $campos, $condiciones, "", "id_pago DESC LIMIT 1") == CONSULTAS_SUCCESS)
		{
			while($registro =$consultas->Fetch())
			{
				$folio_a = $registro["folio_a"];
				$folio_n = $registro["folio_n"];
				$fecha_p = $registro["fecha"];
				list($fecha_pag,$hora)=split(" ",$fecha_p);
	                        list($ano_p,$mes_p,$dia_p)=split("-",$fecha_pag);
	                        $fec_pag = "$dia_p de ".$_MES[$mes_p]." de $ano_p";
				$html='<br />Fecha de Pago :<input name="fecha" type="text" id="fecha"  size="20" maxlength="20" value="'.$fec_pag.'"
					disabled=\'disabled\'  style="color: #000000; font-size: 13px; font-weight:bold;"><br /><br />
					Ultimo Recibo: <input name="foli_a" type="text" id="foli_a"  size="2" maxlength="2" value="'.$folio_a.'" disabled="disabled">
					<input name="folio_a" type="hidden" id="folio_a" value="'.$folio_a.'" >';
				$html.='<input name="foli_n" type="text" id="foli_n"  size="6" value="'.$folio_n.'" disabled="disabled">
					<input name="folio_n" type="hidden" id="folio_n"  value="'.$folio_n.'" ><br />';
                	}
		}else{
			$consultas = new CONSULTAS;
			global $_MES;
			$campos = array("pago_predial_2011.*","pago_predial_liq_2011.*");
			$condiciones = array('status_p_p' => 2, 'id_predial'=>$id,'status_folio'=>'1');
			$inner=array("pago_predial_2011:pago_predial_liq_2011" => "pago_predial_2011.id_pago=pago_predial_liq_2011.id_solicitud");
			if($consultas->QueryINNERJOIN($inner, $campos, $condiciones, "", "id_pago DESC LIMIT 1") == CONSULTAS_SUCCESS)
			{
				while($registro =$consultas->Fetch())
				{
					$folio_a = $registro["folio_a"];
					$folio_n = $registro["folio_n"];
					$fecha_p = $registro["fecha"];
					list($fecha_pag,$hora)=split(" ",$fecha_p);
			                list($ano_p,$mes_p,$dia_p)=split("-",$fecha_pag);
			                $fec_pag = "$dia_p de ".$_MES[$mes_p]." de $ano_p";
					$html='<br />Fecha de Pago :<input name="fecha" type="text" id="fecha"  size="20" maxlength="20" value="'.$fec_pag.'"
						disabled=\'disabled\'  style="color: #000000; font-size: 13px; font-weight:bold;"><br /><br />
						Ultimo Recibo: <input name="foli_a" type="text" id="foli_a"  size="2" maxlength="2" value="'.$folio_a.'" disabled="disabled">
						<input name="folio_a" type="hidden" id="folio_a" value="'.$folio_a.'" >';
					$html.='<input name="foli_n" type="text" id="foli_n"  size="6" value="'.$folio_n.'" disabled="disabled">
						<input name="folio_n" type="hidden" id="folio_n"  value="'.$folio_n.'" ><br />';
		        	}
			}
		}
	}
	if(empty($folio_a) && empty($folio_n))
	{
		$folio_a = "00";
		$folio_n = "00";
		$html='Ultimo Recibo: <input name="foli_a" type="text" id="foli_a"  size="2" maxlength="2" value="'.$folio_a.'" disabled="disabled">
			<input name="folio_a" type="hidden" id="folio_a" value="'.$folio_a.'" >';
		$html.='<input name="foli_n" type="text" id="foli_n"  size="6" value="'.$folio_n.'" disabled="disabled">
			<input name="folio_n" type="hidden" id="folio_n"  value="'.$folio_n.'" >';
	}
	return $html;
}

function PagoNormal($id,$res,$cor,$pago_ant1,$mes_imp1)
{
$desc_ene_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_feb_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_mar_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_abr_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_may_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_jun_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_jul_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_ago_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_sep_2010_2007	= ".40|.50,1.00,1.00,0";
$desc_oct_2010_2007	= ".40|.50,1.00,1.00,0";
$desc_nov_2010_2007	= ".40|.50,1.00,1.00,0";
$desc_dic_2010_2007	= ".40|.50,1.00,1.00,0";

$desc_ene_2011		= "0|.50,1.00,1.00,0";
$desc_feb_2011		= "0|.50,1.00,1.00,0";
$desc_mar_2011		= "0|.50,1.00,1.00,0";
$desc_abr_2011		= "0|.50,1.00,1.00,0";
$desc_may_2011		= "0|.50,1.00,1.00,0";
$desc_jun_2011		= "0|.50,1.00,1.00,0";
$desc_jul_2011		= "0|.50,1.00,1.00,0";
$desc_ago_2011		= "0|.50,1.00,1.00,0";
$desc_sep_2011		= "0|.50,1.00,1.00,0";
$desc_oct_2011		= "0|.50,1.00,1.00,0";
$desc_nov_2011		= "0|.50,1.00,1.00,0";
$desc_dic_2011		= "0|.50,1.00,1.00,0";

$desc_ene_2012		= ".10|.06,0,0,0";
$desc_feb_2012		= ".08|.04,0,0,0";
$desc_mar_2012		= ".06|.0,0,0,0";
$desc_abr_2012		= ".00,0,0,0";
$desc_may_2012		= ".00,0,0,0";
$desc_jun_2012		= ".00,0,0,0";
$desc_jul_2012		= ".00,1.00,1.00,0";
$desc_ago_2012		= ".00,1.00,1.00,0";
$desc_sep_2012		= ".00,1.00,1.00,0";
$desc_oct_2012		= ".00,1.00,1.00,0";
$desc_nov_2012		= ".00,1.00,1.00,0";
$desc_dic_2012		= ".00,1.00,1.00,0";


$descuentos_ene_hab	= array('2013'=>$desc_ene_2012,'2012'=>$desc_ene_2011,'2011'=>$desc_ene_2010_2007,'2010'=>$desc_ene_2010_2007,
				'2009'=>$desc_ene_2010_2007,'2008'=>$desc_ene_2010_2007,'2007'=>$desc_ene_2010_2007,'2006'=>$desc_ene_2010_2007);
$descuentos_feb_hab	= array('2012'=>$desc_feb_2012,'2011'=>$desc_feb_2011,'2010'=>$desc_feb_2010_2007,'2009'=>$desc_feb_2010_2007,
				'2008'=>$desc_feb_2010_2007,'2007'=>$desc_feb_2010_2007,'2006'=>$desc_feb_2010_2007);
$descuentos_mar_hab	= array('2012'=>$desc_mar_2012,'2011'=>$desc_mar_2011,'2010'=>$desc_mar_2010_2007,'2009'=>$desc_mar_2010_2007,
				'2008'=>$desc_mar_2010_2007,'2007'=>$desc_mar_2010_2007,'2006'=>$desc_mar_2010_2007);
$descuentos_abr_hab	= array('2012'=>$desc_abr_2012,'2011'=>$desc_abr_2011,'2010'=>$desc_abr_2010_2007,'2009'=>$desc_abr_2010_2007,
				'2008'=>$desc_abr_2010_2007,'2007'=>$desc_abr_2010_2007,'2006'=>$desc_abr_2010_2007);
$descuentos_may_hab	= array('2012'=>$desc_may_2012,'2011'=>$desc_may_2011,'2010'=>$desc_may_2010_2007,'2009'=>$desc_may_2010_2007,
				'2008'=>$desc_may_2010_2007,'2007'=>$desc_may_2010_2007,'2006'=>$desc_may_2010_2007);
$descuentos_jun_hab	= array('2012'=>$desc_jun_2012,'2011'=>$desc_jun_2011,'2010'=>$desc_jun_2010_2007,'2009'=>$desc_jun_2010_2007,
				'2008'=>$desc_jun_2010_2007,'2007'=>$desc_jun_2010_2007,'2006'=>$desc_jun_2010_2007);
$descuentos_jul_hab	= array('2012'=>$desc_jul_2012,'2011'=>$desc_jul_2011,'2010'=>$desc_jul_2010_2007,'2009'=>$desc_jul_2010_2007,
				'2008'=>$desc_jul_2010_2007,'2007'=>$desc_jul_2010_2007,'2006'=>$desc_jul_2010_2007);
$descuentos_ago_hab	= array('2012'=>$desc_ago_2012,'2011'=>$desc_ago_2011,'2010'=>$desc_ago_2010_2007,'2009'=>$desc_ago_2010_2007,
				'2008'=>$desc_ago_2010_2007,'2007'=>$desc_ago_2010_2007,'2006'=>$desc_ago_2010_2007);
$descuentos_sep_hab	= array('2012'=>$desc_sep_2012,'2011'=>$desc_sep_2011,'2010'=>$desc_sep_2010_2007,'2009'=>$desc_sep_2010_2007,
				'2008'=>$desc_sep_2010_2007,'2007'=>$desc_sep_2010_2007,'2006'=>$desc_sep_2010_2007);
$descuentos_oct_hab	= array('2012'=>$desc_oct_2012,'2011'=>$desc_oct_2011,'2010'=>$desc_oct_2010_2007,'2009'=>$desc_oct_2010_2007,
				'2008'=>$desc_oct_2010_2007,'2007'=>$desc_oct_2010_2007,'2006'=>$desc_oct_2010_2007);
$descuentos_nov_hab	= array('2012'=>$desc_nov_2012,'2011'=>$desc_nov_2011,'2010'=>$desc_nov_2010_2007,'2009'=>$desc_nov_2010_2007,
				'2008'=>$desc_nov_2010_2007,'2007'=>$desc_nov_2010_2007,'2006'=>$desc_nov_2010_2007);
$descuentos_dic_hab	= array('2012'=>$desc_dic_2012,'2011'=>$desc_dic_2011,'2010'=>$desc_dic_2010_2007,'2009'=>$desc_dic_2010_2007,
				'2008'=>$desc_dic_2010_2007,'2007'=>$desc_dic_2010_2007,'2006'=>$desc_dic_2010_2007);
$desc_c_ene_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_feb_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_mar_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_abr_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_may_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_jun_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_jul_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_ago_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_sep_2010_2007	= "0|0,0,0,0";
$desc_c_oct_2010_2007	= "0|0,0,0,0";
$desc_c_nov_2010_2007	= "0|0,0,0,0";
$desc_c_dic_2010_2007	= "0|0,0,0,0";

$desc_c_ene_2011		= "0|.50,1.00,1.00,0";
$desc_c_feb_2011		= "0|.50,1.00,1.00,0";
$desc_c_mar_2011		= "0|.50,1.00,1.00,0";
$desc_c_abr_2011		= "0|.50,1.00,1.00,0";
$desc_c_may_2011		= "0|.50,1.00,1.00,0";
$desc_c_jun_2011		= "0|.50,1.00,1.00,0";
$desc_c_jul_2011		= "0|.50,1.00,1.00,0";
$desc_c_ago_2011		= "0|.50,1.00,1.00,0";
$desc_c_sep_2011		= "0|0,0,0,0";
$desc_c_oct_2011		= "0|0,0,0,0";
$desc_c_nov_2011		= "0|0,0,0,0";
$desc_c_dic_2011		= "0|0,0,0,0";

$desc_c_ene_2012		= ".10,0,0,0";
$desc_c_feb_2012		= ".08,0,0,0";
$desc_c_mar_2012		= ".06,0,0,0";
$desc_c_abr_2012		= ".00,0,0,0";
$desc_c_may_2012		= ".00,0,0,0";
$desc_c_jun_2012		= ".00,0,0,0";
$desc_c_jul_2012		= ".00,1.00,1.00,0";
$desc_c_ago_2012		= ".00,1.00,1.00,0";
$desc_c_sep_2012		= ".00,0,0,0";
$desc_c_oct_2012		= ".00,0,0,0";
$desc_c_nov_2012		= ".00,0,0,0";
$desc_c_dic_2012		= ".00,0,0,0";

$descuentos_c_ene_hab	= array('2013'=>$desc_ene_2012,'2012'=>$desc_ene_2011,'2011'=>$desc_c_ene_2010_2007,'2010'=>$desc_c_ene_2010_2007,
				'2009'=>$desc_c_ene_2010_2007,'2008'=>$desc_c_ene_2010_2007,'2007'=>$desc_c_ene_2010_2007,'2006'=>$desc_c_ene_2010_2007);
$descuentos_c_feb_hab	= array('2012'=>$desc_feb_2012,'2011'=>$desc_feb_2011,'2010'=>$desc_c_feb_2010_2007,'2009'=>$desc_c_feb_2010_2007,
				'2008'=>$desc_c_feb_2010_2007,'2007'=>$desc_c_feb_2010_2007,'2006'=>$desc_c_feb_2010_2007);
$descuentos_c_mar_hab	= array('2012'=>$desc_mar_2012,'2011'=>$desc_mar_2011,'2010'=>$desc_c_mar_2010_2007,'2009'=>$desc_c_mar_2010_2007,
				'2008'=>$desc_c_mar_2010_2007,'2007'=>$desc_c_mar_2010_2007,'2006'=>$desc_c_mar_2010_2007);
$descuentos_c_abr_hab	= array('2012'=>$desc_c_abr_2012,'2011'=>$desc_abr_2011,'2010'=>$desc_c_abr_2010_2007,'2009'=>$desc_c_abr_2010_2007,
				'2008'=>$desc_c_abr_2010_2007,'2007'=>$desc_c_abr_2010_2007,'2006'=>$desc_c_abr_2010_2007);
$descuentos_c_may_hab	= array('2012'=>$desc_c_may_2012,'2011'=>$desc_may_2011,'2010'=>$desc_c_may_2010_2007,'2009'=>$desc_c_may_2010_2007,
				'2008'=>$desc_c_may_2010_2007,'2007'=>$desc_c_may_2010_2007,'2006'=>$desc_c_may_2010_2007);
$descuentos_c_jun_hab	= array('2012'=>$desc_c_jun_2012,'2011'=>$desc_jun_2011,'2010'=>$desc_c_jun_2010_2007,'2009'=>$desc_c_jun_2010_2007,
				'2008'=>$desc_c_jun_2010_2007,'2007'=>$desc_c_jun_2010_2007,'2006'=>$desc_c_jun_2010_2007);
$descuentos_c_jul_hab	= array('2012'=>$desc_jul_2012,'2011'=>$desc_jul_2011,'2010'=>$desc_c_jul_2010_2007,'2009'=>$desc_c_jul_2010_2007,
				'2008'=>$desc_c_jul_2010_2007,'2007'=>$desc_c_jul_2010_2007,'2006'=>$desc_c_jul_2010_2007);
$descuentos_c_ago_hab	= array('2012'=>$desc_ago_2012,'2011'=>$desc_ago_2011,'2010'=>$desc_c_ago_2010_2007,'2009'=>$desc_c_ago_2010_2007,
				'2008'=>$desc_c_ago_2010_2007,'2007'=>$desc_c_ago_2010_2007,'2006'=>$desc_c_ago_2010_2007);
$descuentos_c_sep_hab	= array('2012'=>$desc_c_sep_2012,'2011'=>$desc_c_sep_2011,'2010'=>$desc_c_sep_2010_2007,'2009'=>$desc_c_sep_2010_2007,
				'2008'=>$desc_c_sep_2010_2007,'2007'=>$desc_c_sep_2010_2007,'2006'=>$desc_c_sep_2010_2007);
$descuentos_c_oct_hab	= array('2012'=>$desc_c_oct_2012,'2011'=>$desc_c_oct_2011,'2010'=>$desc_c_oct_2010_2007,'2009'=>$desc_c_oct_2010_2007,
				'2008'=>$desc_c_oct_2010_2007,'2007'=>$desc_c_oct_2010_2007,'2006'=>$desc_c_oct_2010_2007);
$descuentos_c_nov_hab	= array('2012'=>$desc_c_nov_2012,'2011'=>$desc_c_nov_2011,'2010'=>$desc_c_nov_2010_2007,'2009'=>$desc_c_nov_2010_2007,
				'2008'=>$desc_c_nov_2010_2007,'2007'=>$desc_c_nov_2010_2007,'2006'=>$desc_c_nov_2010_2007);
$descuentos_c_dic_hab	= array('2012'=>$desc_c_dic_2012,'2011'=>$desc_c_dic_2011,'2010'=>$desc_c_dic_2010_2007,'2009'=>$desc_c_dic_2010_2007,
				'2008'=>$desc_c_dic_2010_2007,'2007'=>$desc_c_dic_2010_2007,'2006'=>$desc_c_dic_2010_2007);

$desc=array("1"=> $descuentos_ene_hab,"2"=> $descuentos_feb_hab,"3"=> $descuentos_mar_hab,
		"4"=> $descuentos_abr_hab,"5"=> $descuentos_may_hab,"6"=> $descuentos_jun_hab,
		"7"=> $descuentos_jul_hab,"8"=> $descuentos_ago_hab,"9"=> $descuentos_sep_hab,
		"10"=> $descuentos_oct_hab,"11"=> $descuentos_nov_hab,"12"=> $descuentos_dic_hab);
$desc_c=array("1"=> $descuentos_c_ene_hab,"2"=> $descuentos_c_feb_hab,"3"=> $descuentos_c_mar_hab,
		"4"=> $descuentos_c_abr_hab,"5"=> $descuentos_c_may_hab,"6"=> $descuentos_c_jun_hab,
		"7"=> $descuentos_c_jul_hab,"8"=> $descuentos_c_ago_hab,"9"=> $descuentos_c_sep_hab,
		"10"=> $descuentos_c_oct_hab,"11"=> $descuentos_c_nov_hab,"12"=> $descuentos_c_dic_hab);

$_meses=array("1"=> "Enero","2"=> "FEBRERO","3"=> "MARZO","4"=> "ABRIL","5"=>"MAYO","6"=>"JUNIO",
		"7"=>"JULIO","8"=>"AGOSTO","9"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$_mes_liq=date('n');
	$h=1;
	$iRow = 0;
	$MyCiclo = new CICLO;
	$collectiones = $MyCiclo->Coleccion('','10', '1','ano_ciclo', 'ASC',$id,'< '.date('Y'));
	$total_ciclos = $MyCiclo->getTotal();
	$debe_2010 = $MyCiclo->ExistAno_2010($id);
	$totales_ciclos_res=$total_ciclos;

		$MyCicloss = new CICLO;
		$coles = $MyCicloss->Coleccion('','7', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
		$total_ciclossss = $MyCicloss->getTotal();
		if($total_ciclossss > 0)
		{
			foreach($coles as $coles)
			{
				$id_ciclo_p		= $MyCicloss->getID($coles);
			}
			$MyTipologias = new TIPOLOGIA;
			$coleccion = $MyTipologias->Coleccion('','20','1','id_tipo_c','ASC',$id_ciclo_p);
			$total_tipo =$MyTipologias->getTotal();
			$construccion=array("HA","HA1","HA2","HA3","HB","HB1","HB2","HB3",
			"HC","HC1","HC2","HC3","HD","HD1","HD2","HD3",
			"HE","HE1","HE2","HE3","HF","HF1","HF2","HF3"/*,"EA1","EA2"*/,"EC2",
			"EC3","EC4"/*,"ED1","ED2"*/,"EL1","EB1","EG1","EG2"/*,"EH3","EH4","EI1","EJ2","EK3","EK4","EE1",
			"EF2"*/);
			$aux=0;
			$sup_comer=0;
			if($total_tipo > 0)
			{
				$html="<table  border=\"0\" align='center' width=\"800px\">";
				$html.="<tr class='THtitle' style='color: #000000;' ><td >Tipo de Construccion</td><td width=\"400px\">m&sup2;</td></tr>";
				foreach($coleccion as $coleccion)
				{
					$id_tipologia	= $MyTipologias->getID($coleccion);
					$sup		= $MyTipologias->getSup($coleccion);
					$tipo		= $MyTipologias->getTipo_const($coleccion);
					$aux = $sup+$aux;
					if(!in_array($tipo,$construccion))
					{
						$sup_comer=$sup+$sup_comer;
					}
				$html.="<tr><td class=\"DatProp\" >$tipo</td><td  class=\"datos\" >$sup</td></tr>";

				}

				if(!empty($aux))
				{
					$por_comercio =	(100*$sup_comer)/$aux;
				}
			}else
			{
				$aux=0;
				$por_comercio=0;
			}
			if($por_comercio==0)
                	{
                    	$tipo_c='HAB';
	                }elseif ($por_comercio>0 && $por_comercio <= 25) {
	                    $tipo_c='MIXTO';
        	        }elseif ($por_comercio > 25) {
        	            $tipo_c='COMERCIO';
        	        }
				$html.="<tr><td class=\"DatProp\" >$tipo_c</td><td  class=\"datos\" >$aux</td></tr>";
				$html.="</table><br />";
		}
	$html.="<input type='hidden' id='desc_res' name='desc_res' value='$res'>
            <table  border=\"0\" align='center' width=\"800px\">";
	$html.="<tr><td>A&ntilde;os anteriores</td></tr>";
	$html.="</table>";
        if($total_ciclos > 0)
	{

	$i=0;
	foreach($collectiones as $collectiones)
	{

		$id_ciclo		= $MyCiclo->getID($collectiones);
		$ano_ciclo		= $MyCiclo->getAno($collectiones);
		$const 			= $MyCiclo->getConst($collectiones);
		$ter 			= $MyCiclo->getTer($collectiones);
                $mes_pag		= $MyCiclo->getMes_pag($collectiones);
                $clase_pag		= $MyCiclo->getClase_pag($collectiones);
		$valor			= $MyCiclo->getValor($collectiones);
		$damnificado		= $MyCiclo->getDamnificados($collectiones);

		$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
		$thisClassC = $thisClass . "C";
		$iRow++;

           //     if($total_tipo > 0)
             //   {
		$col = Impuesto_pred($valor,$ano_ciclo);
		$col_imp = $col['impuesto'];
		$col_imp = Formato_de_numeros($col_imp);
		$multas  = $col_imp;

		$recargo = Recargos($ano_ciclo,$col_imp);
		$recargos = Formato_de_numeros($recargo[$ano_ciclo]);

		if($por_comercio <= 25){
			$_desc=$desc[$_mes_liq];
			$a_desc=split(",",$_desc[$ano_ciclo]);
			//print_r($a_desc);
			/*if($debe_2010==ANO_ERROR){
				$a_n_desc=split("\|",$a_desc[0]);
			//print_r($a_n_desc);
				$a_n_d=$a_n_desc[0];
			}else{*/
				$a_n_desc=split("\|",$a_desc[0]);
			//print_r($a_n_desc);
				$a_n_d=$a_n_desc[0];
			//}

		}else{
			$_desc=$desc_c[$_mes_liq];
			$a_desc=split(",",$_desc[$ano_ciclo]);
			/*if($debe_2010==ANO_ERROR){
				$a_n_desc=split("\|",$a_desc[0]);
				$a_n_d=$a_n_desc[0];
			}else{*/
				$a_n_desc=split("\|",$a_desc[0]);
				$a_n_d=$a_n_desc[0];
			//}
			//print_r($a_n_desc);
		}
		$bonificacion	= Formato_de_numeros($col_imp*$a_n_d);
		$est_rec	= Formato_de_numeros($recargos*$a_desc[2]);
		$est_mul	= Formato_de_numeros($multas*$a_desc[1]);
		//$html.="$bonificacion=$col_imp*$a_n_d";
		$gastos[$h] = $multas+$col_imp+$recargos;
		/*if($ano_ciclo!=2010){
			if($por_comercio <= 25){
				$descuentos=($col['impuesto']*$res)/100;
			}
		}*/
		$total		= $col_imp+$recargos+$multas;
		$total		= Formato_de_numeros($total);
		$subsidios	= $bonificacion+$est_rec+$est_mul;
		//$html.="<br />$total		= $col_imp+$recargos+$multas";
		//$html.="<br />$subsidios	= $bonificacion+$est_rec+$est_mul";
		$subsidios	= Formato_de_numeros($subsidios);
		$bonificacion	= Formato_de_numeros($bonificacion);

		$pag_tots=$total-$subsidios;
		if(($ano_ciclo==2007) && $pag_tots<142.80){
			$pag_tots   =142.80;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}elseif(($ano_ciclo==2006) && $pag_tots<137.43){
			$pag_tots=137.43;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}elseif(($ano_ciclo==2005) && $pag_tots<132.15){
			$pag_tots=132.15;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}

		$pag_tots	= Formato_de_numeros($pag_tots);
              	$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'><td>A&Ntilde;O</td><td>TIPO</td><td>VALOR <br />CATASTRAL</td><td>IMPUESTO <br />PREDIAL</td>\n
			<td>RECARGOS</td><td>MULTAS</td><td>BONIFICACION</td><td>TOTAL</td></tr>\n";
		$html.="<tr  >\n";
		$html.="<td width=\"100px\" >\n";
		$html.="";
		$html.="<input type='checkbox' id='ano' name='ano$h' checked='checked' value='$id_ciclo' onclick=\"ValorAno('ano$h','$h'); CalcularTotalParcial('ano$h','total_par$h'); \">";
		$html.="<input type='hidden' id='anos' name='anos$h' value='$ano_ciclo' >";
	        $html.= $ano_ciclo."</td>\n";
		$html.="<td>".$tipo_c."</td>\n";
			$html.="<td>".$valor."</td>\n";
			$html.="<td>".$col_imp."</td>\n";
			$html.="<td>".$recargos."</td>\n";
			$html.="<td>".$multas."</td>\n";
                        $html.="<input type='hidden' value='".$gastos[$h]." name='total_gast_ejec$h' id='total_gast_ejec$h' >\n";
			$html.="<td>$bonificacion</td>\n";
			$html.="<td>".$total."</td>\n";
	                $html.="</tr><tr >";
	                $html.="<td></td>\n";
	                $html.="<td></td>\n";
	                $html.="<td></td>\n";
                        $html.="<td></td>\n";
	                $html.="<td class='TDtotal' colspan='3'>CONDONACIONES</td>\n";
	                $html.="<td class='TDTotal' >-".$subsidios."</td>\n";
	                $html.="</tr><tr >\n";
                        $html.="<td></td>\n";
	                $html.="<td colspan='2'>\n";
	                $html.="</td><td>\n";
			$html.="</td>\n";
	                $html.="<td class='TDtotal' style='text-decoration: underline;' colspan='3'>\nTOTAL A PAGAR<br /> POR A&Ntilde;O $ano_ciclo</td>\n";
	                $html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
	                $html.="<input type='hidden' id='total_par$h' name='total_par$h' value='$pag_tots' >";
			$html.="<tr>\n";
	                $html.="</table><br />\n";
			$aux_pag_t=$pag_tots+$aux_pag_t;
			$descuentos="";
			$total="";
			$h++;
			$i++;
		if($total_tipo == 0)
		{
                      	$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'>\n
				<td>Su clave no cuenta con una tipologia o los metros de contruccion son 01</td>\n</tr>";
			$html.="</table>";
		}
	}
}else
{
	$html.="<table border=\"0\" align='center' width=\"800px\">\n<tr class='THtitle'>\n
		<td>Este predio no presenta adeudo en a&ntilde;o de Resago</td>\n</tr>\n";
	$html.="</table>";
}
$html.="<br />";
$html.="<table  border=\"0\" align='center' width=\"700px\"><tr><td>A&ntilde;o Corriente</td></tr></table>";
$collectiones = $MyCiclo->Coleccion('','6', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
$total_ciclos_1 = $MyCiclo->getTotal();
if($total_ciclos_1 > 0)
{
	$i=0;
	foreach($collectiones as $collectiones)
	{

		$id_ciclo		= $MyCiclo->getID($collectiones);
		$ano_ciclo		= $MyCiclo->getAno($collectiones);
		$const 			= $MyCiclo->getConst($collectiones);
		$ter 			= $MyCiclo->getTer($collectiones);
                $mes_pag		= $MyCiclo->getMes_pag($collectiones);
                $clase_pag		= $MyCiclo->getClase_pag($collectiones);
                $damnificado		= $MyCiclo->getDamnificados($collectiones);
                $pago_ant		= $MyCiclo->getImporte_2010($collectiones);
		$mes_imp		= $MyCiclo->getM_i_2010($collectiones);
		$valor			= $MyCiclo->getValor($collectiones);


		$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
		$thisClassC = $thisClass . "C";
		$iRow++;
                    ///echo "hola";
                    	$BIMESTRES=array("2"=>"Enero-Febrero",
					"4"=>"Marzo-Abril",
					"6"=>"Mayo-Junio",
					"8"=>"Julio-Agosto",
					"10"=>"Septiembre-Octubre",
					"12"=>"Noviembre-Diciembre"
					);

	        	$html.="<input type='hidden' id='desc_cor' name='desc_cor' value='$cor'>\n
                                <table border=\"0\" align='center' width=\"800px\">";
			$html.="<tr class='THtitle'><td >A&Ntilde;O</td><td >TIPO</td><td>VALOR<br /> CATASTRAL</td>\n
                                <td>IMPUESTO<br /> PREDIAL</td><td>RECARGOS</td><td>MULTAS</td><td>ESTIMULOS <br />FISCALES</td>\n
				<td>TOTAL</td></tr>";
			$descuentos	= 0.00;
			$col		= Impuesto_pred($valor,$ano_ciclo);
			$col_imp	= Formato_de_numeros($col['impuesto']);
			$recargo	= Recargos($ano_ciclo,$col_imp);
			$recargos	= Formato_de_numeros($recargo[$ano_ciclo]);
			$multas		= 0.00;
			#$multas		= $col_imp;
			$multas		= Formato_de_numeros($multas/2);
			$_desc=$desc[$_mes_liq];
			$a_desc=split(",",$_desc[$ano_ciclo]);
			//print_r($a_desc);
			//print "<br />".$a_desc[0];
			$a_a_desc=split("\|",$a_desc[0]);
			//print "<br />".$a_a_desc[0];
			#print_r($a_a_desc);
			$descuentos	= $col_imp*$a_a_desc[0];
			$est_rec = ($recargos*$a_desc[1]);

			$est_mul = ($multas*$a_desc[2]);
			#print $total_ciclos;
			if($total_ciclos==0)
			{
				$total['total']=ValorUltimoPago($id);
				#print $total['total'];
				if($total['total'] > 0)
				{
					$bonificacion	= $col_imp*$a_a_desc[1];
				}
			}
			$est_rec	= Formato_de_numeros($est_rec);
			$est_mul	= Formato_de_numeros($est_mul);
			$descuentos	= Formato_de_numeros($descuentos+$bonificacion);
			$total		= $col_imp+$recargos+$multas;
			$total		= Formato_de_numeros($total);
			if($damnificado==1){
				$subsidios	= $col_imp+$recargos+$multas;
				$subsidios	= number_format(redondear_dos_decimal($subsidios), 2, '.', '');
				$descuentos     = $col_imp;
			}else{
				$subsidios	= $descuentos+$est_rec+$est_mul;
				$subsidios	= number_format(redondear_dos_decimal($subsidios), 2, '.', '');
				//$descuentos	= $subsidios;
			}
			$pag_tots	= $total-$subsidios;
			$totales	= $total;
			$pag_tots	= number_format(redondear_dos_decimal($pag_tots), 2, '.', '');
			$totales	= number_format(redondear_dos_decimal($totales), 2, '.', '');
			$html.="<tr  >\n<td width=\"100px\" >\n";
        		$html.="<input type='checkbox' id='ano' checked='checked' name='ano$h' value='$id_ciclo' onclick=\"ValorAno('ano$h','$h');
                        CalcularTotalParcial('ano$h','total_par$h');\">\n";
        		$html.="<input type='hidden' id='anos' name='anos$h' value='$ano_ciclo' >\n";
        		$html.=$ano_ciclo."</td>\n";
                        $html.="<td>".$tipo_c."</td>\n";
			$html.="<td>".$valor."</td>\n";
			$html.="<td>".$col_imp."</td>\n";
			$html.="<td>".$recargos."</td>\n";
			$html.="<td>".$multas."</td>\n";
			$html.="<td>$descuentos</td>\n";
			$html.="<td>".$totales."</td>\n";
	                $html.="</tr><tr >";
	                $html.="<td></td>\n";
	                $html.="<td></td>\n";
	                $html.="<td></td>\n";
	                $html.="<td></td>\n";
	                $html.="<td class='TDtotal' colspan='3'>CONDONACIONES</td>\n";
        		$html.="<td class='TDTotal' >-".$subsidios."</td>\n";
        		$html.="</tr><tr >\n";
                        $html.="<td></td>\n";
        		$html.="<td colspan='2'>\n";
        		$html.="</td><td>\n";
			$html.="</td>";
        		$html.="\n";
        		$html.="<td class='TDtotal' style='text-decoration: underline;' colspan='3'>
				\nTOTAL A PAGAR<br /> POR A&Ntilde;O $ano_ciclo</td>\n";
	       		$html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
        		$html.="<input type='hidden' id='total_par$h' name='total_par$h' value='$pag_tots' >";
			$aux_pag_t=$pag_tots+$aux_pag_t;
			$html.="<tr>\n";
        		$html.="</table><br />\n";
			$descuentos="";
			$total="";
			$aux='';
			$h++;

			if($total_tipo == 0)
			{
				$html.="<table border=\"0\" align='center' width=\"800px\">
      		                        <tr class='THtitle'>\n<td style='font-zise:14px; color: red;'><b>\n
					Su clave no cuenta con una tipologia o los metros de construccion son 0</b></td></tr>";
				$html.="</table>";
			}
				$i++;
	}
}else
{

      		    $html.="<table border=\"0\" align='center' width=\"800px\">";
                    $MyC = new CICLO;
                    $collectiones = $MyC->ColeccionDamnificados('','6', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
                    $total_damnificados = $MyC->getTotal();
                    if($total_damnificados>0)
                    {
                            $html.="<tr class='THtitle' style='color: #000000'><td colspan='8'><b>Damnificado<b/></td>
        		    </tr>";
                    }
	$html.="<tr class='THtitle'><td>Este predio tiene pagado a&ntilde;o corriente.</td></tr>\n";
	$html.="</table>";
}
if($total_ciclos > 0)
{

	$html.="<br /><table border=\"0\" align='center' width=\"800px\">\n
		<tr><td align='center' colspan='2'></td></tr><tr>\n
		<td width='425px'></td><td><table border='0' width='425px' align='left' ><tr >\n
		<td class='TDtotal' >GATOS DE EJECUCI&Oacute;N:</td><td class='TDTotal' width='100px'><div id='total_gastos'>\n";
	$aux_gas_ej=0;
	if($totales_ciclos_res > 0)
	{

		for($l=1;$l<$h;$l++)
                {
			$aux_gas_ej=$aux_gas_ej+$gastos[$l];
		}
		$total_gas_ej = $aux_gas_ej*.02;
		$total_gas_ej = Formato_de_numeros($total_gas_ej);
		$salario=59*5;
		if($total_gas_ej<$salario)
		{
			$total_gas_ej=$salario;
		}
		$total_gas_ej = Formato_de_numeros($total_gas_ej);
                $html.= $total_gas_ej;
		$_SESSION['total_gas_ej']=$total_gas_ej;
	}
	$html.="<input type='hidden' name='gastos_ejec' id='gastos_ejec' value='$total_gas_ej'></div>\n
		</td></tr></table></td></tr><tr><td width='425px'></td><td>\n<table border='0' width='425px' align='left' ><tr >\n
		<td class='TDtotal' >*SUBTOTAL A PAGAR:</td>\n<td class='TDTotal' width='100px'>\n
		<div id='total_pacial'>";
		$aux_pag_t = $aux_pag_t+$total_gas_ej;
		$aux_pag_t = $aux_pag_t-$total_gas_ej;
		$aux_pag_t =Formato_de_numeros($aux_pag_t);
		$html.=$aux_pag_t;
		$_SESSION['total_par']=$aux_pag_t;
	$html.="</div></td></tr></table></td></tr></table>\n";
}
$html.="<input type='hidden' name='total' id='total' value='$h'>";
	return $html;
}
function PagoVulnerable($id,$res,$cor,$pago_ant1,$mes_imp1)
{
$desc_ene_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_feb_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_mar_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_abr_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_may_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_jun_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_jul_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_ago_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_sep_2010_2007	= ".50|.50,1.00,1.00,0";
$desc_oct_2010_2007	= ".40|.50,1.00,1.00,0";
$desc_nov_2010_2007	= ".40|.50,1.00,1.00,0";
$desc_dic_2010_2007	= ".40|.50,1.00,1.00,0";

$desc_ene_2011		= "0|.50,1.00,1.00,0";
$desc_feb_2011		= "0|.50,1.00,1.00,0";
$desc_mar_2011		= "0|.50,1.00,1.00,0";
$desc_abr_2011		= "0|.50,1.00,1.00,0";
$desc_may_2011		= "0|.50,1.00,1.00,0";
$desc_jun_2011		= "0|.50,1.00,1.00,0";
$desc_jul_2011		= "0|.50,1.00,1.00,0";
$desc_ago_2011		= "0|.50,1.00,1.00,0";
$desc_sep_2011		= "0|.50,1.00,1.00,0";
$desc_oct_2011		= "0|.50,1.00,1.00,0";
$desc_nov_2011		= "0|.50,1.00,1.00,0";
$desc_dic_2011		= "0|.50,1.00,1.00,0";

$desc_ene_2012		= ".10,0,0,0";
$desc_feb_2012		= ".08,0,0,0";
$desc_mar_2012		= ".06,0,0,0";
$desc_abr_2012		= ".00,0,0,0";
$desc_may_2012		= ".00,0,0,0";
$desc_jun_2012		= ".00,0,0,0";
$desc_jul_2012		= ".00,1.00,1.00,0";
$desc_ago_2012		= ".00,1.00,1.00,0";
$desc_sep_2012		= ".00,1.00,1.00,0";
$desc_oct_2012		= ".00,1.00,1.00,0";
$desc_nov_2012		= ".00,1.00,1.00,0";
$desc_dic_2012		= ".00,1.00,1.00,0";


$descuentos_ene_hab	= array('2013'=>$desc_ene_2012,'2012'=>$desc_ene_2011,'2011'=>$desc_ene_2010_2007,'2010'=>$desc_ene_2010_2007,
				'2009'=>$desc_ene_2010_2007,'2008'=>$desc_ene_2010_2007,'2007'=>$desc_ene_2010_2007,'2006'=>$desc_ene_2010_2007);
$descuentos_feb_hab	= array('2012'=>$desc_feb_2012,'2011'=>$desc_feb_2011,'2010'=>$desc_feb_2010_2007,'2009'=>$desc_feb_2010_2007,
				'2008'=>$desc_feb_2010_2007,'2007'=>$desc_feb_2010_2007,'2006'=>$desc_feb_2010_2007);
$descuentos_mar_hab	= array('2012'=>$desc_mar_2012,'2011'=>$desc_mar_2011,'2010'=>$desc_mar_2010_2007,'2009'=>$desc_mar_2010_2007,
				'2008'=>$desc_mar_2010_2007,'2007'=>$desc_mar_2010_2007,'2006'=>$desc_mar_2010_2007);
$descuentos_abr_hab	= array('2012'=>$desc_abr_2012,'2011'=>$desc_abr_2011,'2010'=>$desc_abr_2010_2007,'2009'=>$desc_abr_2010_2007,
				'2008'=>$desc_abr_2010_2007,'2007'=>$desc_abr_2010_2007,'2006'=>$desc_abr_2010_2007);
$descuentos_may_hab	= array('2012'=>$desc_may_2012,'2011'=>$desc_may_2011,'2010'=>$desc_may_2010_2007,'2009'=>$desc_may_2010_2007,
				'2008'=>$desc_may_2010_2007,'2007'=>$desc_may_2010_2007,'2006'=>$desc_may_2010_2007);
$descuentos_jun_hab	= array('2012'=>$desc_jun_2012,'2011'=>$desc_jun_2011,'2010'=>$desc_jun_2010_2007,'2009'=>$desc_jun_2010_2007,
				'2008'=>$desc_jun_2010_2007,'2007'=>$desc_jun_2010_2007,'2006'=>$desc_jun_2010_2007);
$descuentos_jul_hab	= array('2012'=>$desc_jul_2012,'2011'=>$desc_jul_2011,'2010'=>$desc_jul_2010_2007,'2009'=>$desc_jul_2010_2007,
				'2008'=>$desc_jul_2010_2007,'2007'=>$desc_jul_2010_2007,'2006'=>$desc_jul_2010_2007);
$descuentos_ago_hab	= array('2012'=>$desc_ago_2012,'2011'=>$desc_ago_2011,'2010'=>$desc_ago_2010_2007,'2009'=>$desc_ago_2010_2007,
				'2008'=>$desc_ago_2010_2007,'2007'=>$desc_ago_2010_2007,'2006'=>$desc_ago_2010_2007);
$descuentos_sep_hab	= array('2012'=>$desc_sep_2012,'2011'=>$desc_sep_2011,'2010'=>$desc_sep_2010_2007,'2009'=>$desc_sep_2010_2007,
				'2008'=>$desc_sep_2010_2007,'2007'=>$desc_sep_2010_2007,'2006'=>$desc_sep_2010_2007);
$descuentos_oct_hab	= array('2012'=>$desc_oct_2012,'2011'=>$desc_oct_2011,'2010'=>$desc_oct_2010_2007,'2009'=>$desc_oct_2010_2007,
				'2008'=>$desc_oct_2010_2007,'2007'=>$desc_oct_2010_2007,'2006'=>$desc_oct_2010_2007);
$descuentos_nov_hab	= array('2012'=>$desc_nov_2012,'2011'=>$desc_nov_2011,'2010'=>$desc_nov_2010_2007,'2009'=>$desc_nov_2010_2007,
				'2008'=>$desc_nov_2010_2007,'2007'=>$desc_nov_2010_2007,'2006'=>$desc_nov_2010_2007);
$descuentos_dic_hab	= array('2012'=>$desc_dic_2012,'2011'=>$desc_dic_2011,'2010'=>$desc_dic_2010_2007,'2009'=>$desc_dic_2010_2007,
				'2008'=>$desc_dic_2010_2007,'2007'=>$desc_dic_2010_2007,'2006'=>$desc_dic_2010_2007);

$desc_c_ene_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_feb_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_mar_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_abr_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_may_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_jun_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_jul_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_ago_2010_2007	= "0|0,1.00,1.00,0";
$desc_c_sep_2010_2007	= "0|0,0,0,0";
$desc_c_oct_2010_2007	= "0|0,0,0,0";
$desc_c_nov_2010_2007	= "0|0,0,0,0";
$desc_c_dic_2010_2007	= "0|0,0,0,0";

$desc_c_ene_2011		= "0|.50,1.00,1.00,0";
$desc_c_feb_2011		= "0|.50,1.00,1.00,0";
$desc_c_mar_2011		= "0|.50,1.00,1.00,0";
$desc_c_abr_2011		= "0|.50,1.00,1.00,0";
$desc_c_may_2011		= "0|.50,1.00,1.00,0";
$desc_c_jun_2011		= "0|.50,1.00,1.00,0";
$desc_c_jul_2011		= "0|.50,1.00,1.00,0";
$desc_c_ago_2011		= "0|.50,1.00,1.00,0";
$desc_c_sep_2011		= "0|0,0,0,0";
$desc_c_oct_2011		= "0|0,0,0,0";
$desc_c_nov_2011		= "0|0,0,0,0";
$desc_c_dic_2011		= "0|0,0,0,0";

$desc_c_ene_2012		= ".10,0,0,0";
$desc_c_feb_2012		= ".08,0,0,0";
$desc_c_mar_2012		= ".06,0,0,0";
$desc_c_abr_2012		= ".00,0,0,0";
$desc_c_may_2012		= ".00,0,0,0";
$desc_c_jun_2012		= ".00,0,0,0";
$desc_c_jul_2012		= ".00,1.00,1.00,0";
$desc_c_ago_2012		= ".00,1.00,1.00,0";
$desc_c_sep_2012		= ".00,0,0,0";
$desc_c_oct_2012		= ".00,0,0,0";
$desc_c_nov_2012		= ".00,0,0,0";
$desc_c_dic_2012		= ".00,0,0,0";

$descuentos_c_ene_hab	= array('2013'=>$desc_ene_2012,'2012'=>$desc_ene_2011,'2011'=>$desc_c_ene_2010_2007,'2010'=>$desc_c_ene_2010_2007,
				'2009'=>$desc_c_ene_2010_2007,'2008'=>$desc_c_ene_2010_2007,'2007'=>$desc_c_ene_2010_2007,'2006'=>$desc_c_ene_2010_2007);
$descuentos_c_feb_hab	= array('2012'=>$desc_feb_2012,'2011'=>$desc_feb_2011,'2010'=>$desc_c_feb_2010_2007,'2009'=>$desc_c_feb_2010_2007,
				'2008'=>$desc_c_feb_2010_2007,'2007'=>$desc_c_feb_2010_2007,'2006'=>$desc_c_feb_2010_2007);
$descuentos_c_mar_hab	= array('2012'=>$desc_mar_2012,'2011'=>$desc_mar_2011,'2010'=>$desc_c_mar_2010_2007,'2009'=>$desc_c_mar_2010_2007,
				'2008'=>$desc_c_mar_2010_2007,'2007'=>$desc_c_mar_2010_2007,'2006'=>$desc_c_mar_2010_2007);
$descuentos_c_abr_hab	= array('2012'=>$desc_c_abr_2012,'2011'=>$desc_abr_2011,'2010'=>$desc_c_abr_2010_2007,'2009'=>$desc_c_abr_2010_2007,
				'2008'=>$desc_c_abr_2010_2007,'2007'=>$desc_c_abr_2010_2007,'2006'=>$desc_c_abr_2010_2007);
$descuentos_c_may_hab	= array('2012'=>$desc_c_may_2012,'2011'=>$desc_may_2011,'2010'=>$desc_c_may_2010_2007,'2009'=>$desc_c_may_2010_2007,
				'2008'=>$desc_c_may_2010_2007,'2007'=>$desc_c_may_2010_2007,'2006'=>$desc_c_may_2010_2007);
$descuentos_c_jun_hab	= array('2012'=>$desc_c_jun_2012,'2011'=>$desc_jun_2011,'2010'=>$desc_c_jun_2010_2007,'2009'=>$desc_c_jun_2010_2007,
				'2008'=>$desc_c_jun_2010_2007,'2007'=>$desc_c_jun_2010_2007,'2006'=>$desc_c_jun_2010_2007);
$descuentos_c_jul_hab	= array('2012'=>$desc_jul_2012,'2011'=>$desc_jul_2011,'2010'=>$desc_c_jul_2010_2007,'2009'=>$desc_c_jul_2010_2007,
				'2008'=>$desc_c_jul_2010_2007,'2007'=>$desc_c_jul_2010_2007,'2006'=>$desc_c_jul_2010_2007);
$descuentos_c_ago_hab	= array('2012'=>$desc_ago_2012,'2011'=>$desc_ago_2011,'2010'=>$desc_c_ago_2010_2007,'2009'=>$desc_c_ago_2010_2007,
				'2008'=>$desc_c_ago_2010_2007,'2007'=>$desc_c_ago_2010_2007,'2006'=>$desc_c_ago_2010_2007);
$descuentos_c_sep_hab	= array('2012'=>$desc_c_sep_2012,'2011'=>$desc_c_sep_2011,'2010'=>$desc_c_sep_2010_2007,'2009'=>$desc_c_sep_2010_2007,
				'2008'=>$desc_c_sep_2010_2007,'2007'=>$desc_c_sep_2010_2007,'2006'=>$desc_c_sep_2010_2007);
$descuentos_c_oct_hab	= array('2012'=>$desc_c_oct_2012,'2011'=>$desc_c_oct_2011,'2010'=>$desc_c_oct_2010_2007,'2009'=>$desc_c_oct_2010_2007,
				'2008'=>$desc_c_oct_2010_2007,'2007'=>$desc_c_oct_2010_2007,'2006'=>$desc_c_oct_2010_2007);
$descuentos_c_nov_hab	= array('2012'=>$desc_c_nov_2012,'2011'=>$desc_c_nov_2011,'2010'=>$desc_c_nov_2010_2007,'2009'=>$desc_c_nov_2010_2007,
				'2008'=>$desc_c_nov_2010_2007,'2007'=>$desc_c_nov_2010_2007,'2006'=>$desc_c_nov_2010_2007);
$descuentos_c_dic_hab	= array('2012'=>$desc_c_dic_2012,'2011'=>$desc_c_dic_2011,'2010'=>$desc_c_dic_2010_2007,'2009'=>$desc_c_dic_2010_2007,
				'2008'=>$desc_c_dic_2010_2007,'2007'=>$desc_c_dic_2010_2007,'2006'=>$desc_c_dic_2010_2007);
$desc=array("1"=> $descuentos_ene_hab,"2"=> $descuentos_feb_hab,"3"=> $descuentos_mar_hab,
		"4"=> $descuentos_abr_hab,"5"=> $descuentos_may_hab,"6"=> $descuentos_jun_hab,
		"7"=> $descuentos_jul_hab,"8"=> $descuentos_ago_hab,"9"=> $descuentos_sep_hab,
		"10"=> $descuentos_oct_hab,"11"=> $descuentos_nov_hab,"12"=> $descuentos_dic_hab);
$desc_c=array("1"=> $descuentos_c_ene_hab,"2"=> $descuentos_c_feb_hab,"3"=> $descuentos_c_mar_hab,
		"4"=> $descuentos_c_abr_hab,"5"=> $descuentos_c_may_hab,"6"=> $descuentos_c_jun_hab,
		"7"=> $descuentos_c_jul_hab,"8"=> $descuentos_c_ago_hab,"9"=> $descuentos_c_sep_hab,
		"10"=> $descuentos_c_oct_hab,"11"=> $descuentos_c_nov_hab,"12"=> $descuentos_c_dic_hab);

$_meses=array("1"=> "Enero","2"=> "FEBRERO","3"=> "MARZO","4"=> "ABRIL","5"=>"MAYO","6"=>"JUNIO",
		"7"=>"JULIO","8"=>"AGOSTO","9"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$_mes_liq=date('n');
	$h=1;
	$iRow = 0;
	$MyCiclo = new CICLO;
	$collectiones = $MyCiclo->Coleccion('','10', '1','ano_ciclo', 'ASC',$id,'< '.date('Y'));
	$total_ciclos = $MyCiclo->getTotal();
	$totales_ciclos_res=$total_ciclos;

		$MyCicloss = new CICLO;
		$coles = $MyCicloss->Coleccion('','7', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
		$total_ciclossss = $MyCicloss->getTotal();
		if($total_ciclossss > 0)
		{
			foreach($coles as $coles)
			{
				$id_ciclo_p		= $MyCicloss->getID($coles);
			}
			$MyTipologias = new TIPOLOGIA;
			$coleccion = $MyTipologias->Coleccion('','20','1','id_tipo_c','ASC',$id_ciclo_p);
			$total_tipo =$MyTipologias->getTotal();
			$construccion=array("HA","HA1","HA2","HA3","HB","HB1","HB2","HB3",
			"HC","HC1","HC2","HC3","HD","HD1","HD2","HD3",
			"HE","HE1","HE2","HE3","HF","HF1","HF2","HF3"/*,"EA1","EA2"*/,"EC2",
			"EC3","EC4"/*,"ED1","ED2"*/,"EL1","EB1","EG1","EG2"/*,"EH3","EH4","EI1","EJ2","EK3","EK4","EE1",
			"EF2"*/);
			$aux=0;
			$sup_comer=0;
			if($total_tipo > 0)
			{
				$html="<table  border=\"0\" align='center' width=\"800px\">";
				$html.="<tr class='THtitle' style='color: #000000;' ><td >Tipo de Construccion</td><td width=\"400px\">m&sup2;</td></tr>";
				foreach($coleccion as $coleccion)
				{
					$id_tipologia	= $MyTipologias->getID($coleccion);
					$sup		= $MyTipologias->getSup($coleccion);
					$tipo		= $MyTipologias->getTipo_const($coleccion);
					$aux = $sup+$aux;
					if(!in_array($tipo,$construccion))
					{
						$sup_comer=$sup+$sup_comer;
					}

					$html.="<tr><td class=\"DatProp\" >$tipo</td><td  class=\"datos\" >$sup</td></tr>";
				}

				if(!empty($aux))
				{
					$por_comercio =	(100*$sup_comer)/$aux;
				}
			}else
			{
				$aux=0;
				$por_comercio=0;
			}
			if($por_comercio==0)
                	{
                    	$tipo_c='HAB';
	                }elseif ($por_comercio>0 && $por_comercio <= 25) {
	                    $tipo_c='MIXTO';
        	        }elseif ($por_comercio > 25) {
        	            $tipo_c='COMERCIO';
        	        }
				$html.="<tr><td class=\"DatProp\" >$tipo_c</td><td  class=\"datos\" >$aux</td></tr>";
				$html.="</table><br />";
		}
	$html.="<input type='hidden' id='desc_res' name='desc_res' value='$res'><table  border=\"0\" align='center' width=\"800px\">";
	$html.="<tr><td>A&ntilde;os anteriores</td></tr>";
	$html.="</table>";
        if($total_ciclos > 0)
	{

	$i=0;
	foreach($collectiones as $collectiones)
	{

		$est_rec = '';
		$est_mul = '';
		$id_ciclo		= $MyCiclo->getID($collectiones);
		$ano_ciclo		= $MyCiclo->getAno($collectiones);
		$const 			= $MyCiclo->getConst($collectiones);
		$ter 			= $MyCiclo->getTer($collectiones);
                $mes_pag		= $MyCiclo->getMes_pag($collectiones);
                $clase_pag		= $MyCiclo->getClase_pag($collectiones);
		$valor			= $MyCiclo->getValor($collectiones);
		$damnificado		= $MyCiclo->getDamnificados($collectiones);

		$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
		$thisClassC = $thisClass . "C";
		$iRow++;

		$col 		= Impuesto_pred($valor,$ano_ciclo);
		$col_imp 	= $col['impuesto'];
		$col_imp 	= Formato_de_numeros($col_imp);


		$recargo 	= Recargos($ano_ciclo,$col_imp);
		$recargos 	= Formato_de_numeros($recargo[$ano_ciclo]);
		$multas		= $col_imp;
		$multas		= Formato_de_numeros($multas);
		$gastos[$h] = $multas+$col_imp+$recargos;
		if($por_comercio <= 25){
			$_desc=$desc[$_mes_liq];
			$a_desc=split(",",$_desc[$ano_ciclo]);
			//print_r($a_desc);
			/*if($debe_2010==ANO_ERROR){
				$a_n_desc=split("\|",$a_desc[0]);
			//print_r($a_n_desc);
				$a_n_d=$a_n_desc[0];
			}else{*/
				$a_n_desc=split("\|",$a_desc[0]);
			//print_r($a_n_desc);
				$a_n_d=$a_n_desc[1];
			//}

		}else{
			$_desc=$desc_c[$_mes_liq];
			$a_desc=split(",",$_desc[$ano_ciclo]);
			/*if($debe_2010==ANO_ERROR){
				$a_n_desc=split("\|",$a_desc[0]);
				$a_n_d=$a_n_desc[0];
			}else{*/
				$a_n_desc=split("\|",$a_desc[0]);
				$a_n_d=$a_n_desc[0];
			//}
			//print_r($a_n_desc);
		}

		$bonificacion	= Formato_de_numeros($col_imp*$a_n_d);
		//$html.="$bonificacion	= $col_imp*$a_n_d";
		$est_rec	= Formato_de_numeros($recargos*$a_desc[2]);
		$est_mul	= Formato_de_numeros($multas*$a_desc[1]);
		$total		= $col_imp+$recargos+$multas;
		$total		= Formato_de_numeros($total);
		$subsidios	= $bonificacion+$est_rec+$est_mul;
		$subsidios	= Formato_de_numeros($subsidios);
		$descuentos	= $bonificacion;
		$descuentos	= Formato_de_numeros($descuentos);
		$pag_tots=$total-$subsidios;
		if(($ano_ciclo==2007) && $pag_tots<142.80){
			$pag_tots   =142.80;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}elseif(($ano_ciclo==2006) && $pag_tots<137.43){
			$pag_tots=137.43;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}elseif(($ano_ciclo==2005) && $pag_tots<132.15){
			$pag_tots=132.15;
			$diferencia =$pag_tots-$col_imp;
      			$col_imp=$col_imp+$difencia;
		}
		$bonificacion	= Formato_de_numeros($bonificacion);
		$pag_tots	= Formato_de_numeros($pag_tots);
		$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'><td>A&Ntilde;O</td><td>TIPO</td>
	                        <td>VALOR <br />CATASTRAL</td><td>IMPUESTO <br />PREDIAL</td><td>RECARGOS</td>
				<td>MULTAS</td><td>BONIFICACION</td><td>TOTAL</td></tr>";
			$html.="<tr  >\n";
			$html.="<td width=\"100px\" >\n";
	                $html.="<input type='checkbox' id='ano' name='ano$h' checked='checked' value='$id_ciclo'
				onclick=\"ValorAno('ano$h','$h'); CalcularTotalParcial('ano$h','total_par$h'); \">";
	                $html.="<input type='hidden' id='anos' name='anos$h' value='$ano_ciclo' >";
	                $html.= $ano_ciclo."</td>\n";
			$html.="<td>".$tipo_c."</td>\n";
			$html.="<td>".$valor."</td>\n";
			$html.="<td>".$col_imp."</td>\n";
			$html.="<td>".$recargos."</td>\n";
			$html.="<td>".$multas."</td>\n";
			$html.="<input type='hidden' value='".$gastos[$h]." name='total_gast_ejec$h' id='total_gast_ejec$h' >\n";
			$html.="<td>$descuentos</td>\n";
			$html.="<td>".$total."</td>\n</tr><tr >";
	                $html.="<td></td>\n<td></td>\n";
	                $html.="<td></td>\n<td></td>\n";
			$html.="<td class='TDtotal' colspan='3'>CONDONACIONES</td>\n";
	                $html.="<td class='TDTotal' >-".$subsidios."</td>\n";
	                $html.="</tr><tr >\n<td></td>\n";
	                $html.="<td colspan='2'>\n</td><td>\n";
			$html.="</td>\n";
	                $html.="<td class='TDtotal' style='text-decoration: underline;' colspan='3'>
				TOTAL A PAGAR<br /> POR A&Ntilde;O $ano_ciclo</td>\n";
        		$html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
        		$html.="<input type='hidden' id='total_par$h' name='total_par$h' value='$pag_tots' >";
			$html.="<tr>\n";
        		$html.="</table><br />";
			$aux_pag_t=$pag_tots+$aux_pag_t;
			$descuentos="";
			$total="";
			$h++;
			$i++;
		if($total_tipo == 0)
		{
                       	$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'>
			<td>Su clave no cuenta con una tipologia o los metros de contruccion son 01</td></tr>";
			$html.="</table>";
		}
        }
}else
{
	$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'>
	<td>Este predio no presenta adeudo en a&ntilde;o de Resago</td></tr>";
	$html.="</table>";
}
$html.="<br />";
$html.="<table  border=\"0\" align='center' width=\"700px\"><tr><td>A&ntilde;o Corriente</td></tr></table>";
$collectiones = $MyCiclo->Coleccion('','6', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
$total_ciclos = $MyCiclo->getTotal();
if($total_ciclos > 0)
{
	$i=0;
	foreach($collectiones as $collectiones)
	{

		$id_ciclo		= $MyCiclo->getID($collectiones);
		$ano_ciclo		= $MyCiclo->getAno($collectiones);
		$const 			= $MyCiclo->getConst($collectiones);
		$ter 			= $MyCiclo->getTer($collectiones);
                $mes_pag		= $MyCiclo->getMes_pag($collectiones);
                $clase_pag		= $MyCiclo->getClase_pag($collectiones);
                $damnificado		= $MyCiclo->getDamnificados($collectiones);
                $pago_ant		= $MyCiclo->getImporte_2010($collectiones);
		$mes_imp		= $MyCiclo->getM_i_2010($collectiones);
		$valor			= $MyCiclo->getValor($collectiones);

		$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
		$thisClassC = $thisClass . "C";
		$iRow++;

        	$html.="<input type='hidden' id='desc_cor' name='desc_cor' value='$cor'>
			<table border=\"0\" align='center' width=\"800px\">";
		$html.="<tr class='THtitle'><td >A&Ntilde;O</td><td >TIPO</td><td>VALOR<br /> CATASTRAL</td>
                        <td>IMPUESTO<br /> PREDIAL</td><td>RECARGOS</td><td>MULTAS</td><td>ESTIMULOS <br />FISCALES</td><td>TOTAL</td></tr>";
		$descuentos	= '';
		$col		= Impuesto_pred($valor,$ano_ciclo);
		$col_imp	= Formato_de_numeros($col['impuesto']);
		$recargo	= Recargos($ano_ciclo,$col_imp);
		$recargos	= Formato_de_numeros($recargo[$ano_ciclo]);
		#$multas		= $col_imp;
		$multas  = 0.00;
		$multas		= Formato_de_numeros($multas/2);
			$a_desc=split(",",$_desc[$ano_ciclo]);
			$v_ultimo_pago=array();
			//print_r($a_desc);
			//print "<br />".$a_desc[0];
			$a_a_desc=split("\|",$a_desc[0]);
			//print "<br />".$a_a_desc[0];
			//print_r($a_a_desc);
			$diferencia	= $col_imp*$a_a_desc[0];
			$est_rec = ($recargos*$a_desc[1]);
			$est_mul = ($multas*$a_desc[2]);
			if($total_ciclos==0)
			{
				$desc_ad=array("1"=> ".34","2"=> ".34","3"=> ".34","4"=> ".40","5"=> ".40","6"=> ".40","7"=> "0.50");
				$desc_ad_d=array("1"=> ".16","2"=> ".04","3"=> ".10","4"=> ".10","5"=> ".10","6"=> ".10","7"=> "0.0");
				$v_ultimo_pago=ValorUltimoPago($id);
				//print $total;
				if(BuscarGpo($v_ultimo_pago['especial'])==2)
				{
					if($v_ultimo_pago['total']>0)
					{
						$diferencia_aux	= $col_imp*($a_a_desc[1]+$desc_ad[$_mes_liq]);
						$descuentos	= $col_imp*$desc_ad_d[$_mes_liq];
						//print "$diferencia	= $col_imp*(".$a_a_desc[1]."+".$desc_ad[$_mes_liq].")<br />";
						$diferencia	= Formato_de_numeros($diferencia+$diferencia_aux);
						$descuentos	= Formato_de_numeros($descuentos);
					}
				}
				//$descuentos=$descuentos+$bonificacion;
			}else{
				$desc_ad=array("1"=> ".34","2"=> ".34","3"=> ".34","4"=> ".40","5"=> ".40","6"=> ".40","7"=> "0.50");
				$desc_ad_d=array("1"=> ".16","2"=> ".04","3"=> ".10","4"=> ".10","5"=> ".10","6"=> ".10","7"=> "0.0");
				$desc_g_vul=$desc_ad[$_mes_liq];

				$diferencia_aux	= $col_imp*$desc_ad[$_mes_liq];
				$descuentos	= $col_imp*$desc_ad_d[$_mes_liq];
				$diferencia	= Formato_de_numeros($diferencia+$diferencia_aux);
				$descuentos	= Formato_de_numeros($descuentos);
				//print "$diferencia	= $col_imp*".$desc_ad[$_mes_liq];
				//$descuentos=$descuentos+$bonificacion;
			}
		//$est_rec = ($recargos*0);
		//$est_mul = ($multas*0);
		$total		=$col_imp+$recargos+$multas;
		$total		= Formato_de_numeros($total);
		if($damnificado==1){
			$subsidios	= $col_imp+$recargos+$multas;
			$subsidios	= Formato_de_numeros($subsidios);
			$descuentos	= $est_rec+$est_mul;
		}else{
			$subsidios	= $descuentos+$diferencia+$est_rec+$est_mul;
		//$html.="$subsidios	= $descuentos+$diferencia+$est_rec+$est_mul<br />\n";
			$subsidios	= Formato_de_numeros($subsidios);
			$descuentos=$est_rec+$est_mul;
		}
		$pag_tots	= $total-$subsidios;
		$totales	= $total;
		$descuentos	= Formato_de_numeros($descuentos);
		$pag_tots	= Formato_de_numeros($pag_tots);
		$totales	= Formato_de_numeros($totales);
		$html.="<tr  >\n<td width=\"100px\" >\n";
                $html.="<input type='checkbox' id='ano' checked='checked' name='ano$h' value='$id_ciclo' onclick=\"ValorAno('ano$h','$h');
				CalcularTotalParcial('ano$h','total_par$h');\">";
                $html.="<input type='hidden' id='anos' name='anos$h' value='$ano_ciclo' >";
                $html.=$ano_ciclo."</td>\n";
		$html.="<td>".$tipo_c."</td>\n";
		$html.="<td>".$valor."</td>\n";
		$html.="<td>".$col_imp."</td>\n";
		$html.="<td>".$recargos."</td>\n";
		$html.="<td>".$multas."</td>\n";
		$html.="<td>$descuentos</td>\n";
		$html.="<td>".$totales."</td>\n";
                $html.="</tr><tr ><td></td>\n";
                $html.="<td></td>\n<td></td>\n";
                $html.="<td></td>\n<td class='TDtotal' colspan='3'>CONDONACIONES</td>\n";
       		$html.="<td class='TDTotal' >-".$subsidios."</td>\n";
       		$html.="</tr><tr >\n<td></td>\n";
       		$html.="<td colspan='2'>\n</td><td>\n";
		$html.="</td>\n";
      		$html.="<td class='TDtotal' style='text-decoration: underline;' colspan='3'>TOTAL A PAGAR<br /> POR A&Ntilde;O $ano_ciclo</td>\n";
       		$html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
       		$html.="<input type='hidden' id='total_par$h' name='total_par$h' value='$pag_tots' >";
		$aux_pag_t=$pag_tots+$aux_pag_t;
		$html.="<tr>\n";
       		$html.="</table><br />";
		$descuentos="";
		$total="";
		$aux='';
		$h++;
		if($total_tipo == 0)
		{
			$html.="<table border=\"0\" align='center' width=\"800px\"><tr class='THtitle'>
			<td style='font-zise:14px; color: red;'><b>Su clave no cuenta con una tipologia o los metros de construccion son 0
			</b></td></tr>";
			$html.="</table>";
		}
		$i++;
	}
}else
{
	$html.="<table border=\"0\" align='center' width=\"800px\">";
	$MyC = new CICLO;
	$collectiones = $MyC->ColeccionDamnificados('','6', '1','ano_ciclo', 'ASC',$id,'= '.date('Y'));
	$total_damnificados = $MyC->getTotal();
	if($total_damnificados>0)
	{
		$html.="<tr class='THtitle' style='color: #000000'><td colspan='8'><b>Damnificado<b/></td></tr>";
	}
	$html.="<tr class='THtitle'><td>Este predio tiene pagado a&ntilde;o corriente.</td></tr>";
	$html.="</table>";
}
if($total_ciclos > 0)
{
	$html.="<br /><table border=\"0\" align='center' width=\"800px\"><tr>
        	<td align='center' colspan='2'></td></tr><tr>
        	<td width='425px'></td><td><table border='0' width='425px' align='left' ><tr >
		<td class='TDtotal' >GATOS DE EJECUCI&Oacute;N:</td><td class='TDTotal' width='100px'>
		<div id='total_gastos'>";
		$aux_gas_ej=0;
		if($totales_ciclos_res > 0)
		{
			for($l=1;$l<$h;$l++)
			{
				$aux_gas_ej=$aux_gas_ej+$gastos[$l];
			}
					$total_gas_ej = $aux_gas_ej*.02;
			$salario=59*5;
			if($total_gas_ej<$salario)
			{
				$total_gas_ej=$salario;
			}
			$total_gas_ej = Formato_de_numeros($total_gas_ej);
                        $html.= $total_gas_ej;
 			$_SESSION['total_gas_ej']=$total_gas_ej;
		}
	$html.="<input type='hidden' name='gastos_ejec' id='gastos_ejec' value='$total_gas_ej'></div></td>
		</tr></table></td></tr><tr><td width='425px'></td><td><table border='0' width='425px' align='left' >
		<tr ><td class='TDtotal' >*SUBTOTAL A PAGAR:</td><td class='TDTotal' width='100px'><div id='total_pacial'>";
		$aux_pag_t = $aux_pag_t+$total_gas_ej;
		$aux_pag_t = $aux_pag_t-$total_gas_ej;
		$aux_pag_t=number_format($aux_pag_t, 2, '.', '');
                $html.=$aux_pag_t;
		$_SESSION['total_par']=$aux_pag_t;
	$html.="</div></td></tr></table></td></tr></table>";
}
$html.="<input type='hidden' name='total' id='total' value='$h'>";
	return $html;
}

function ParcialBimestre($id)
{
	$MyCiclo = new CICLO;
	global $MyCiclo;
	$collectiones = $MyCiclo->Coleccion('','6', '1','id_ciclo', 'ASC',$id,'= '.date('Y'));
	$total_ciclos = $MyCiclo->getTotal();
	$h=1;
	if($total_ciclos > 0){
		$i=0;
		foreach($collectiones as $collectiones)
		{

			$id_ciclo		= $MyCiclo->getID($collectiones);
			$ano_ciclo		= $MyCiclo->getAno($collectiones);
			$const 			= $MyCiclo->getConst($collectiones);
			$ter 			= $MyCiclo->getTer($collectiones);
			$ano_pag		= $MyCiclo->getAno_pag($collectiones);
			$mes_pag		= $MyCiclo->getMes_pag($collectiones);






			$clase_pag		= $MyCiclo->getClase_pag($collectiones);
			$valor		= $MyCiclo->getValor($collectiones);

			$MyTipologia = new TIPOLOGIA;
			global $MyTipologias;
			$coleccion = $MyTipologia->Coleccion('','20','1','id_tipo_c','ASC',$id_ciclo);
			$total_tipo =$MyTipologia->getTotal();
			$construccion=array("HA","HA1","HA2","HA3","HB","HB1","HB2","HB3",
				"HC","HC1","HC2","HC3","HD","HD1","HD2","HD3",
				"HE","HE1","HE2","HE3","HF","HF1","HF2","HF3");
			$aux=0;
			$sup_comer=0;

			if($total_tipo > 0)
			{
				foreach($coleccion as $coleccion)
				{

					$id_tipologia	= $MyTipologia->getID($coleccion);
					$sup		= $MyTipologia->getSup($coleccion);
					$tipo		= $MyTipologia->getTipo_const($coleccion);
					$aux = $sup+$aux;

					if(!in_array($tipo,$construccion))
					{
						$sup_comer=$sup+$sup_comer;

					}

				}
				if(!empty($aux)){
					$por_comercio =	(100*$sup_comer)/$aux;
				}
			}
				if($por_comercio==0)

	                {
	                    $tipo_c='HAB';
	                }elseif ($por_comercio>0) {
	                    $tipo_c='MIXTO';
	                }elseif ($por_comercio <= 25) {
	                    $tipo_c='MIXTO';
	                }elseif ($por_comercio > 25) {
	                    $tipo_c='COMERC';
	                }

			$BIMESTRES=array("2"=>"Enero-Febrero",
					"4"=>"Marzo-Abril",
					"6"=>"Mayo-Junio",
					"8"=>"Julio-Agosto",


					"10"=>"Septiembre-Octubre",
					"12"=>"Noviembre-Diciembre"
					);
			$col = Impuesto_pred($valor,$ano_ciclo);





			for($j=$mes_pag+1;$j <= 12;$j++)
			{

				$j++;

				$recargos = RecargosBimestral($j,$col['bimestral']);


				$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
				$thisClassC = $thisClass . "C";
				$iRow++;
        		                  $html.="<table border=\"0\" align='center' width=\"800px\">
        		                        <tr class='THtitle'>
        		                        <td>Periodo</td>
                                                <td>TIPO</td>
        		                        <td>VALOR CATASTRAL</td>
        		                        <td>IMPUESTO PREDIAL</td>
        	                        <td>RECARGOS</td><!--
        			                        <td>SUBSIDIO</td>-->
        		                        <td>TOTAL</td>
        		                        </tr>";
					$html.="<tr  >\n";

					$html.="<td width=\"200px\" >\n";
					$html.="<input type='hidden' id='ciclo' name='ciclo$h' value='$id_ciclo' >";
        		                $html.="<input type='checkbox' id='ano' checked='checked' name='ano$h' value='$j' onclick=\"ValorAno('ano$h','$h');\">";
        		                $html.=$BIMESTRES[$j]."</td>\n";
                                        $html.="<td>".$tipo_c."</td>\n";
					$html.="<td>".$valor."</td>\n";

					$col_imp=Formato_de_numeros($col['bimestral']);
					$html.="<td>".$col_imp."</td>\n";

					$recargos=Formato_de_numeros($recargos);
					$html.="<td>".$recargos."</td>\n";
					//if($por_comercio <= 25)
					//{
						//echo "<br />d2010.-".
						//echo "<br />d2010.-".$cor;
						//echo "<br />d2010.-".$col['impuesto'];
					//	$descuentos=($col['impuesto']*$cor)/100;
					//}
        		                $descuentos=Formato_de_numeros($descuentos);
					//$html.="<!--<td>$descuentos</td>-->\n";
					$total=$col['bimestral']+$recargos;
					$total=Formato_de_numeros($total);
        		                //$total=redondear_dos_decimal($total);
					$html.="<td>".$total."</td>\n";
        		                $html.="</tr>";
					//$html.="<tr >";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td class='TDtotal' colspan='2'>SUBSIDIO</td>\n";
        		                //$subsidios=$recargos+$descuentos;
        		                //$subsidios=redondear_dos_decimal($subsidios);
					//$subsidios=number_format($subsidios, 2, '.', '');
        		                //$html.="<td class='TDTotal' >-".$subsidios."</td>\n";
        		                //$html.="</tr>";
					$html.="<tr >\n";
                                        //$html.="<td></td>\n";
        		                $html.="<td colspan='2'>\n";
        		                $html.="</td><td>\n";
					$html.="</td>";
        		                $html.="\n";

        		                $html.="<td class='TDtotal' style='text-decoration: underline;' colspan='2'>TOTAL A PAGAR<br /> POR A&Ntilde;O $llave</td>\n";
        		                $pag_tots=$total-$subsidios;
					$pag_tots=Formato_de_numeros($pag_tots);
        		                $html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
					$html.="<tr>\n";
        		                $html.="</table><br />";
					$html.='';
					$descuentos="";
					$total="";
					$h++;
			}
				$i++;
			$html.="<input type='hidden' name='total' id='total' value='$h'>";
		}
	}else
	{
      		                  $html.="<table border=\"0\" align='center' width=\"700px\">
        		                        <tr class='THtitle'>
						<td>Usted esta al corriente con su pago predial</td>
        		                        </tr>";
				$html.="</table>";
	}

	return $html;
}
function RecargosBimestral($mes_i,$col)
{
	list($dia,$mes,$ano)=split("-",date('d-m-Y'));
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $meses = date("n",$fecha_exp_a);
	$rec_mes=($col*1.85)/100;
	$aux_rec=0;
	$rec_bi=0;
	$Mes_i=$mes_i-1;
	//echo "<br />mes".$meses;
	if($Mes_i < $meses)
	{
		for($i=$Mes_i;$i<=$meses;$i++)
		{
			$aux_rec=$rec_mes+$aux_rec;
		}
		$rec_bi=$aux_rec;
	}
	return $rec_bi;
}
function RecargosSemestral($mes_i,$col)
{
	list($dia,$mes,$ano)=split("-",date('d-m-Y'));
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $meses = date("n",$fecha_exp_a);
	$rec_mes=($col*1.85)/100;
	$aux_rec=0;
	$rec_bi=0;
	$Mes_i=$mes_i+1;
	//echo "<br />mes".$meses;
	if($Mes_i < $meses)
	{
		$Mes_i;
		$meses;
		for($i=$Mes_i;$i<=$meses;$i++)
		{
			$aux_rec=$rec_mes+$aux_rec;

		}
		$rec_bi=$aux_rec;
	}

	return $rec_bi;
}
function ParcialSemestre($id)
{
	$MyCiclo = new CICLO;
	global $MyCiclo;
	$collectiones = $MyCiclo->Coleccion('','6', '1','id_ciclo', 'ASC',$id,'= '.date('Y'));
	$total_ciclos = $MyCiclo->getTotal();$h=1;
	if($total_ciclos > 0){
		$i=0;
		foreach($collectiones as $collectiones)
		{

			$id_ciclo		= $MyCiclo->getID($collectiones);
			$ano_ciclo		= $MyCiclo->getAno($collectiones);
			$const 			= $MyCiclo->getConst($collectiones);
			$ter 			= $MyCiclo->getTer($collectiones);
			$ano_pag		= $MyCiclo->getAno_pag($collectiones);
			$mes_pag		= $MyCiclo->getMes_pag($collectiones);
			$clase_pag		= $MyCiclo->getClase_pag($collectiones);
			if(empty($const) || empty($ter))
			{
				$valor		= $MyCiclo->getValor($collectiones);
			}else
			{
				$valor = $const+$ter;
			}
			$MyTipologia = new TIPOLOGIA;
			global $MyTipologias;
			$coleccion = $MyTipologia->Coleccion('','20','1','id_tipo_c','ASC',$id_ciclo);
			$total_tipo =$MyTipologia->getTotal();
			$construccion=array("HA","HA1","HA2","HA3","HB","HB1","HB2","HB3",
				"HC","HC1","HC2","HC3","HD","HD1","HD2","HD3",
				"HE","HE1","HE2","HE3","HF","HF1","HF2","HF3");
			$aux=0;
			$sup_comer=0;

			if($total_tipo > 0)
			{
				foreach($coleccion as $coleccion)
				{

					$id_tipologia	= $MyTipologia->getID($coleccion);
					$sup		= $MyTipologia->getSup($coleccion);
					$tipo		= $MyTipologia->getTipo_const($coleccion);
					$aux = $sup+$aux;

					if(!in_array($tipo,$construccion))
					{
						$sup_comer=$sup+$sup_comer;

					}

				}
				if(!empty($aux)){
					$por_comercio =	(100*$sup_comer)/$aux;
				}
			}
				if($por_comercio==0)
	                {
	                    $tipo_c='HAB';
	                }elseif ($por_comercio>0) {
	                    $tipo_c='MIXTO';
	                }elseif ($por_comercio <= 25) {
	                    $tipo_c='MIXTO';

	                }elseif ($por_comercio > 25) {
	                    $tipo_c='COMERC';
	                }

			$BIMESTRES=array("6"=>"Enero-Junio",
					"12"=>"Julio-Diciembre"
					);
			$col = Impuesto_pred($valor,$ano_ciclo);

			if(empty($mes_pag))
			{
				$mes_pag=0;
			}
			foreach($BIMESTRES as $key => $value)
			{

				if($key!=$mes_pag)
				{
				if($key==12)
				{
					$mes_pag=6;
				}
				for($j=$mes_pag;$j < 12;$j=$j+6)
				{

					$recargos = RecargosSemestral($j,$col['semestral']);
					$aux_rec=$recargos+$aux_rec;

				}
				$recargos=$aux_rec;
				$aux_rec='';
				$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
				$thisClassC = $thisClass . "C";
				$iRow++;
        		                  $html.="<table border=\"0\" align='center' width=\"800px\">
        		                        <tr class='THtitle'>
        		                        <td>Periodo</td>
                                                <td>TIPO</td>
        		                        <td>VALOR CATASTRAL</td>
        		                        <td>IMPUESTO PREDIAL</td>
        	                        <td>RECARGOS</td><!--
        			                        <td>SUBSIDIO</td>-->
        		                        <td>TOTAL</td>
        		                        </tr>";
					$html.="<tr  >\n";

					$html.="<td width=\"200px\" >\n";
					$html.="<input type='hidden' id='ciclo' name='ciclo$h' value='$id_ciclo' >";
        		                $html.="<input type='checkbox' id='ano' name='ano$h' value='$key' onclick=\"ValorAno('ano$h','$h');\">";
        		                $html.=$value."</td>\n";
                                        $html.="<td>".$tipo_c."</td>\n";
					$html.="<td>".$valor."</td>\n";


					$col_imp=Formato_de_numeros($col['semestral']);
					$html.="<td>".$col_imp."</td>\n";

					$recargos=Formato_de_numeros($recargos);
					$html.="<td>".$recargos."</td>\n";
					//if($por_comercio <= 25)
					//{
						//echo "<br />d2010.-".
						//echo "<br />d2010.-".$cor;
						//echo "<br />d2010.-".$col['impuesto'];
					//	$descuentos=($col['impuesto']*$cor)/100;
					//}
        		                $descuentos=Formato_de_numeros($descuentos);

					//$html.="<!--<td>$descuentos</td>-->\n";
					$total=$col['semestral']+$recargos;
					$total=Formato_de_numeros($total);
        		                //$total=redondear_dos_decimal($total);
					$html.="<td>".$total."</td>\n";
        		                $html.="</tr>";
					//$html.="<tr >";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td></td>\n";
        		                //$html.="<td class='TDtotal' colspan='2'>SUBSIDIO</td>\n";
        		                //$subsidios=$recargos+$descuentos;
        		                //$subsidios=redondear_dos_decimal($subsidios);
					//$subsidios=number_format($subsidios, 2, '.', '');
        		                //$html.="<td class='TDTotal' >-".$subsidios."</td>\n";
        		                //$html.="</tr>";
					$html.="<tr >\n";
                                        //$html.="<td></td>\n";
        		                $html.="<td colspan='2'>\n";
        		                $html.="</td><td>\n";
					$html.="</td>";
        		                $html.="\n";

        		                $html.="<td class='TDtotal' style='text-decoration: underline;' colspan='2'>TOTAL A PAGAR<br /> POR A&Ntilde;O $llave</td>\n";
        		                $pag_tots=$total-$subsidios;
					$pag_tots=Formato_de_numeros($pag_tots);
        		                $html.="<td class='TDTotal' width='100' >$pag_tots</td>\n";
					$html.="<tr>\n";
        		                $html.="</table><br />";
					$html.='';
					$descuentos="";
					$total="";

					$h++;
					}
			}
			$i++;
		}
		$html.="<input type='hidden' name='total' id='total' value='$h'>";
	}else
	{
      		                  $html.="<table border=\"0\" align='center' width=\"700px\">
        		                        <tr class='THtitle'>
						<td>Usted esta al corriente con su pago predial</td>
        		                        </tr>";
				$html.="</table>";
	}
	return $html;
}
function BuscarCajera($id)
{
    $consultas = new CONSULTAS;
    $campos = array("cajeras.*");
    $condicion=array("ID_CAJ"=>$id);
    if($consultas->SeleccionarTablaFila("cajeras",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                	$id 		= $registro["ID_CAJ"];
			$nombre		= $registro["NOMBRE_CAJ"];
			$ape_pat	= $registro["APELLIDO_CAJ"];
			$ape_mat	= $registro["APELLIDO_MAT_CAJ"];
			$html="$nombre $ape_pat $ape_mat <input type='hidden' id='id_cajera' name='id_cajera' value='$id' >";
                }
        }
        return $html;
}
function selectTramite($id)
{
	$consultas = new CONSULTAS;
	$campos = array("tipo_tram.*","tip_t.*");
	$condiciones=array("id_tipo_tram"=>$id);
	$inner=array("tipo_tram:tip_t"=>"tipo_tram.tip_t=tip_t.id_tip");
	if($consultas->QueryINNERJOIN($inner, $campos, $condiciones,"", "id_tipo_tram ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$reg["id_tipo_tram"] = $registro["id_tipo_tram"];
			$reg["cuenta_contable"] = $registro["cuenta_contable"];
			$reg["nombre_tip"] = $registro["nombre_tip"];
		}
	}
	return $reg;
}
function selectCajera($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("cajeras.*");
	if($MySPANEL->Nivel() < NIVEL_OPERADOR)
	{
		$condiciones=array("AREA_CAJ"=>$MySPANEL->Area());
	}
		$condiciones["1"] = 1;
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("cajeras", $campos, $condiciones,"", "USERNAME_CAJ ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["ID_CAJ"];
			$nombre = $registro["NOMBRE_CAJ"];
			$apellido = $registro["APELLIDO_CAJ"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTFS($nombre)."".UTFS($apellido)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function selectColonia($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("colonias.*");
	$condiciones=array("status_col"=>1);
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("colonias", $campos, $condiciones,"", "col_nombre ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["col_clave"];
			$nombre = UTF($registro["col_nombre"]);
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";

			}

			$html .= ">".UTF($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function selectTipologia($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("tipologias_const.*");
	$condiciones=array("status_tip"=>1);

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila("tipologias_const", $campos, $condiciones,"", "clave_tipologia ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["clave_tipologia"];
			$nombre = $registro["clave_tipologia"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function bselectTipologia($id)
{
	$consultas = new CONSULTAS;
	$VUC = '';
	$html= "";
	global $MySPANEL;
	$campos = array("tipologias_const.*");
	$condiciones=array("status_tip"=>1,"clave_tipologia"=>$id);
	if($consultas->SeleccionarTablaFila("tipologias_const", $campos, $condiciones,"", "clave_tipologia ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$VUC = $registro["valor_unitario"];
			$html .= "$VUC";
			$html .= "<input type='hidden' id='v_unit' name='v_unit' value='$VUC' />";
		}
	}else{
		$html .= "$VUC";
		$html .= "<input type='hidden' id='v_unit' name='v_unit' value='$VUC' />";
	}
	return $html;
}
function selectTipologiaAnte($id,$funcion,$value,$caption,$ano)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("tipologias_const_$ano.*");
	$condiciones=array("status_tip"=>1);

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila("tipologias_const_$ano", $campos, $condiciones,"", "clave_tipologia ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["clave_tipologia"];
			$nombre = $registro["clave_tipologia"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function bselectTipologiaAnte($id,$ano)
{
	$consultas = new CONSULTAS;
	$VUC = '';
	$html= "";
	global $MySPANEL;
	$campos = array("tipologias_const_$ano.*");
	$condiciones=array("status_tip"=>1,"clave_tipologia"=>$id);
	if($consultas->SeleccionarTablaFila("tipologias_const_$ano", $campos, $condiciones,"", "clave_tipologia ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$VUC = $registro["valor_unitario"];
			$html .= "$VUC";
			$html .= "<input type='hidden' id='v_unit' name='v_unit' value='$VUC' />";
		}
	}else{
		$html .= "$VUC";
		$html .= "<input type='hidden' id='v_unit' name='v_unit' value='$VUC' />";
	}
	return $html;
}
function selectConservacion($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$conservacion =array(	"1.-Muy Bueno"	=>	"1",
				"2.-Bueno"	=>	"2",
				"3.-Medio"	=>	"3",
				"4.-Malo"	=>	"4",
				"5.-Muy malo"	=>	"5",
				"6.-Ruinoso"	=>	"6"
				);

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value='$i'>$caption</option>";
	foreach($conservacion as $Nombre => $cantidad)
	{
			$html .= "<option value='$cantidad' ";
			if($value == $cantidad)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$Nombre</option>";
	}
	$html .= "</select>";
	return $html;
}
function BuscarGpo($grupo)
{
	$consultas = new CONSULTAS;
	$campos = array("gpos_vul.*");
	$condiciones=array("status_gpos"=>1,"id_gpos"=>$grupo);
	if($consultas->SeleccionarTablaFila("gpos_vul", $campos, $condiciones,"", "nom_gpos ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_grupos	= $registro["id_gpos"];
			$grupo		= $registro["grupo"];
		}
	}
	return $grupo;
}
function selectAreas($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("id_area","nombre_area");

	if($MySPANEL->Area() != COI && $MySPANEL->Area() != CON)
	{
		if($MySPANEL->Nivel() <= NIVEL_OPERADOR)
		{
			$condiciones=array("id_area" => $MySPANEL->Area());
		}
	}
	$condiciones["status_area"] = 1;
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("area",$campos, $condiciones,"", "nombre_area ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_area"];
			$nombre = $registro["nombre_area"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTF($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoTramite($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("tipo_tram.*");
	$condicion=array("status_tram"=>1);
	$html = "<select name='$id' id='$id' $funcion style=\"width:400px\">";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('tipo_tram', $campos,$condicion, "", "id_tipo_tram ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["id_tipo_tram"];
			$nombre		= $registro["nombre_tram"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nom ".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function selectGiro($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("giros_alcoholes.*");
	$condiciones=array("status_giro"=>1);

	$html = "<select name='$id' id='$id' $funcion>\n";
	$html .= "<option value='$i'>$caption</option>\n";
	if($consultas->SeleccionarTablaFila("giros_alcoholes", $campos, $condiciones,"", "nombre_giro ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_Giro"];
			$nombre = $registro["nombre_giro"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre</option>\n";
		}
	}
	$html .= "</select>";
	return $html;
}
function selectArtGiro($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$campos = array("art_giro.*");
	$condiciones=array("status_art_giro"=>1);

	$html = "<select name='$id' id='$id' $funcion>\n";
	$html .= "<option value='$i'>$caption</option>\n";
	if($consultas->SeleccionarTablaFila("art_giro", $campos, $condiciones,"", "clave_art ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_art_giro"];
			$nombre = $registro["clave_art"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre</option>\n";
		}
	}
	$html .= "</select>";
	return $html;
}
function RecargosDiversos($ano_b,$per_fin)
{
	$aux_rec='';
	list($dia,$mes,$ano)=split("-",date('d-m-Y'));
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);

	$mes = date("n",$fecha_exp_a);
	list($dia_f,$mes_f,$ano_f)=split("-",$per_fin);

	$mes = (!empty($mes)) ? $mes :"$mes_f";

	$valor_ano	= ValorRec($ano_b);
	if($ano_b<date('Y')){
		$a=9;
		for($i=$ano_b;$i<date('Y');$i++){
			$array_a[$i]=$a;
			$a='12';
		}
		$array_a[date('Y')]=$mes;
	}else
	{
		$array_a[$ano_b]=$mes-3;
	}
	$var=$valor_ano['porcentaje'];
	foreach($array_a as $ano_a => $mes_a)
	{
		$result_rec=$mes_a*$var[$ano_a][1];
		$aux_rec=$result_rec+$aux_rec;
	}
	if($aux_rec>100)
	{
		$aux_rec=100;
	}
	return $aux_rec;
}
function ActualizacionDiversos($ano_b,$per_fin)
{
	$aux_act='';
	global $Mesnualidad;
	list($dia,$mes,$ano)=split("-",date('d-m-Y'));
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
	$mes = date("n",$fecha_exp_a);
	list($dia_f,$mes_f,$ano_f)=split("-",$per_fin);
	$mes = (!empty($mes)) ? $mes :"$mes_f";
	if($ano_b<date('Y')){
		$a=9;
		for($i=$ano_b;$i<date('Y');$i++){
			$array_a[$i]=$a;

			//echo "<br />----".$i."=$a\n";
			$a='12';
		}
		$array_a[date('Y')]=$mes;
		//echo "<br />-----".date('Y')."=$mes\n";
	}else
	{
		$array_a[$ano_b]=$mes-3;
	}
	foreach($array_a as $ano_a => $mes_a)
	{
		$result_act=$mes_a*$Mesnualidad[$ano_a];
		$aux_act=$result_act+$aux_act;
		//echo "<br />$ano_a * $mes_a*".$Mesnualidad[$ano_a]."\n";
	}
	return $aux_act;
}
function ActualizacionTraslados($fecha_tral,$per_fin)
{
	$aux_act='';
	global $Mesnualidad;
	list($dia,$mes,$ano)=split("-",date('d-m-Y'));

	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);

	$mes = date("n",$fecha_exp_a);
	list($dia_f,$mes_f,$ano_f)=split("-",$per_fin);

	$mes = (!empty($mes)) ? $mes :"$mes_f";

	$fecha_max_trasl = Saberdiashabiles(CambioFecha($fecha_tral),'17');
	//echo "$fecha_max_trasl=========".$fecha_tral;
	list($ano_t,$mes_t,$dia_t)=split("-",$fecha_max_trasl);
	if($ano_t<2005)
	{
		$ano_t=2005;
		$mes_t=1;
		$dia_t=1;
	}
	if($ano_t<date('Y')){
		$m=$mes_t-1;
		$a=12-$m;
		$sal_r=array();
		for($i=$ano_t;$i<date('Y');$i++){
			$array_a[$i]=$a;
			$array_r[$i]=$a;
			$a='12';
		}
		$array_a[date('Y')]=$mes;
		$array_r[date('Y')]=$mes;
	}else
	{
		$a=$mes-($mes_t-1);
		$array_a[$ano_t]=$a;
		$array_r[date('Y')]=$a;
	}
	$aux_act='';
	$aux_rec='';
	foreach($array_a as $mensu_a => $mes_a)
	{


		$result_act=$mes_a*$Mesnualidad[$mensu_a];
		$aux_act=$result_act+$aux_act;
		//$aux_a.="<br />$result_act=$mes_a*".$Mesnualidad[$mensu_a];
	}
	foreach($array_r as $mensu_r => $mes_r)
	{
		$sal_r=ValorRec($mensu_r);
		$ano_r=$sal_r['porcentaje'];
		$result_rec=($mes_r*$ano_r[$mensu_r][1])/100;
		$aux_rec=$result_rec+$aux_rec;
		//$aux_a.="<br />$result_rec=$mes_r*".$ano_r[$mensu_r][1];
	}
	$accesorios['act']	= $aux_act;
	$accesorios['rec']	= $aux_rec;
	$accesorios['s']	= $aux_a;
	return $accesorios;
}
function ValorRec($ano)
{
	$consultas = new CONSULTAS;
	$campos = array("salario_anual.*");
	$condiciones=array("ano >= $ano  and 1"=>1);

	if($consultas->SeleccionarTablaFila('salario_anual', $campos, $condiciones,'ano', "ano ASC")==CONSULTAS_SUCCESS)
	{
		$i=0;
		while($registro =$consultas->Fetch())
		{
			$ano			=$registro["ano"];
			$salario[$ano][0]	=$registro["salario"];
			$porcentaje[$ano][1]	=$registro["porcentaje"];
			$i++;
		}
	}

	$valor_anual['salario']		= $salario;
	$valor_anual['porcentaje']	= $porcentaje;
	return $valor_anual;
}
function ARTGIRO($id)
{
	$consultas = new CONSULTAS;
	$campos = array("art_giro.*");
	$condiciones=array("id_art_giro"=>$id);

	if($consultas->SeleccionarTablaFila('art_giro', $campos, $condiciones,'id_art_giro ', "id_art_giro  ASC")==CONSULTAS_SUCCESS)
	{
		$i=0;
		while($registro =$consultas->Fetch())
		{
			$fdo[2006]			=$registro["valor_2006"];
			$fdo[2007]			=$registro["valor_2007"];
			$fdo[2008]			=$registro["valor_2008"];
			$fdo[2009]			=$registro["valor_2009"];
			$fdo[2010]			=$registro["valor_2010"];
			$fdo[2011]			=$registro["valor_2011"];
			$fdo[2012]			=$registro["valor_2012"];
			$fdo[2013]			=$registro["valor_2013"];
			$i++;
		}
	}
	return $fdo;
}
function ARTGIROInfo($id)
{
	$consultas = new CONSULTAS;
	$campos = array("art_giro.*");
	$condiciones=array("id_art_giro" => $id);

	if($consultas->SeleccionarTablaFila('art_giro', $campos, $condiciones,'id_art_giro ', "id_art_giro  ASC")==CONSULTAS_SUCCESS)
	{
		$i=0;
		while($registro =$consultas->Fetch())
		{
			$nombre		=$registro["nombre"];
			$descripcion	=$registro["descripcion"];

		}
	}
	return "$nombre: $descripcion";
}
function SelectPosocion($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("factor_posicion.*");
	$condicion=array("status_fact_p"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('factor_posicion', $campos,$condicion, "", "orden ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["orden"];
			$id_area	= $registro["factor"];
			$nombre		= $registro["posicion"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$id ".$nombre."</option>";
		}

	}
	$html .= "</select>";
	return $html;
}
function ValorUnitarioTerreno($clave)
{
	global $MySPANEL;
	$clave = substr($clave, 0, -8);
	$consultas = new CONSULTAS;
	$campos = array("cal_areas_hom.*, cal_areas.*");
	$condicion=array("cal_areas.area_homo=cal_areas_hom.area_cal_h_id and cal_areas.val_busq"=>$clave);
	if($consultas->SeleccionarTablaFila('cal_areas,cal_areas_hom', $campos,$condicion, "", "val_busq ASC")==CONSULTAS_SUCCESS)
	{
		$registro =$consultas->Fetch();
		$val_m2_suelo		= $registro;
	}
	return $val_m2_suelo;
}
function SelectCalle($id,$funcion,$value,$caption,$zona,$tipo_calle)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("calle.id_calle","calle.calles");
	$zona=(int)$zona;
	$condicion=array("calle.tipo_calle=tipo_calle.id_tipo_calle and status_calle"=>1,"zona"=>$zona,"tipo_calle.id_tipo_calle"=>$tipo_calle);

	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila('tipo_calle,calle', $campos,$condicion, "", "calles ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_calle"];
			$nombre		= UTF($registro["calles"]);
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoAnuncio($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("tipo_anuncios.*");
	$zona=(int)$zona;
	$condicion=array("status_tipo_anuncion"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila('tipo_anuncios', $campos,$condicion, "", "tip_anuncio ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_tipo_anuncio"];
			$nombre		= $registro["tip_anuncio"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function Select_Tipo_Calle($id,$funcion,$value,$caption,$zona)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("tipo_calle.*");
	$zona=(int)$zona;
	$condicion=array("calle.tipo_calle=tipo_calle.id_tipo_calle and status_calle"=>1);
	if(!empty($zona))
	{
		$condicion["zona"] = $zona;
	}
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila('tipo_calle,calle', $campos,$condicion, "tipo_calle.nom_tipo_calle", "calles ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_tipo_calle"];
			$tipo 		= UTFS($registro["nom_tipo_calle"]);
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$tipo </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectDimenciones($id,$funcion,$value,$caption)
{
	global $Dimenciones;
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	foreach($Dimenciones as $llave => $valor){
		$html .= "<option value='$llave' ";
		//echo "$key == $id";
		if($llave == $value)
		{
			$html .= "selected='selected'";
		}
		$html .= ">$valor </option>";
	}
	$html .= "</select>";
	return $html;
}
function  SelectUsoComercio($id,$funcion,$value,$caption)
{
	global $Uso;
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	foreach($Uso as $llave => $valor){
		$html .= "<option value='$llave' ";
		if($llave == $value)
		{
			$html .= "selected='selected'";
		}
		$html .= ">$valor </option>";
	}
	$html .= "</select>";
	return $html;
}
function Buscar_Calle($id)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("id_calle, calles, zona, status_calle, id_tipo_calle, nom_tipo_calle");
	$condicion=array("calle.tipo_calle=tipo_calle.id_tipo_calle and id_calle"=>$id);
	if($consultas->SeleccionarTablaFila('calle, tipo_calle', $campos,$condicion, "", "calle.calles ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_tipo		=$registro["id_tipo_calle"];
		}
	}
	return $id_tipo;
}
function diashabiles($MaxDias)
{
	$fechaInicial = date("Y-m-d"); //obtenemos la fecha de hoy, solo para usar como referencia al usuario

	//$MaxDias = 3; //Cantidad de dias maximo para el prestamo, este sera util para crear el for


	//Creamos un for desde 0 hasta 3
	for ($i=0; $i<$MaxDias; $i++)
	{
                  //Acumulamos la cantidad de segundos que tiene un dia en cada vuelta del for
		$Segundos = $Segundos + 86400;

                  //Obtenemos el dia de la fecha, aumentando el tiempo en N cantidad de dias, segun la vuelta en la que estemos
		$caduca = date("D",time()+$Segundos);

                           //Comparamos si estamos en sabado o domingo, si es asi restamos una vuelta al for, para brincarnos el o los dias...
		if ($caduca == "Sat")
		{
                $i--;
		}else if ($caduca == "Sun")
		{
			$i--;
		}else
		{
                                    //Si no es sabado o domingo, y el for termina y nos muestra la nueva fecha
			$FechaFinal = date("Y-m-d",time()+$Segundos);
		}
	}
	return $FechaFinal;
}
function Saberdiashabiles($fechaInicial,$MaxDias)
{
	//$fechaInicial = date("Y-m-d"); //obtenemos la fecha de hoy, solo para usar como referencia al usuario

	list($ano,$mes,$dia)=split("-",$fechaInicial);

	// Imprime: July 1, 2000 is on a Saturday
	//echo date("l", mktime(0, 0, 0, $mes, $dia, $ano));
//	echo "<br />".$a=date("l",strtotime($ano,$mes,$dia));
	//$MaxDias = 3; //Cantidad de dias maximo para el prestamo, este sera util para crear el for
	//Creamos un for desde 0 hasta 3
	for ($i=0; $i<=$MaxDias; $i++)
	{
                  //Acumulamos la cantidad de segundos que tiene un dia en cada vuelta del for


                  //Obtenemos el dia de la fecha, aumentando el tiempo en N cantidad de dias, segun la vuelta en la que estemos
		$caduca = date("D",mktime(0, 0, 0, $mes, $dia, $ano)+$Segundos);
          	//echo "/".$caduca;
                //Comparamos si estamos en sabado o domingo, si es asi restamos una vuelta al for, para brincarnos el o los dias...
		if ($caduca == "Sat")
		{
			$i--;
		}else if ($caduca == "Sun")
		{
			$i--;
		}else
		{
                                    //Si no es sabado o domingo, y el for termina y nos muestra la nueva fecha
			$FechaFinal = date("Y-m-d",mktime(0, 0, 0, $mes, $dia, $ano)+$Segundos);
		}
		$Segundos = $Segundos + 86400;
	}
	return $FechaFinal;
}
function SelectRama($id,$funcion,$value,$caption)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$zona=(int)$zona;
	$condicion=array("status_rama"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_rama', $campos,$condicion, "", "nombre_rama ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_rama"];
			$nombre		= $registro["nombre_rama"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectSubRama($id,$funcion,$value,$caption,$condicones)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_subrama"=>1);
	if(!empty($condicones))
	{
		$condicion["rama_id"] = $condicones;
	}
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_subrama', $campos,$condicion, "", "nombre_subrama ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_subrama"];
			$nombre		= $registro["nombre_subrama"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectSector($id,$funcion,$value,$caption)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$zona=(int)$zona;
	$condicion=array("status_sector"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_sector', $campos,$condicion, "", "nombre_sector ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_sector"];
			$nombre		= $registro["nombre_sector"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectSubSector($id,$funcion,$value,$caption,$condicones)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_subsector"=>1);
	if(!empty($condicones))
	{
		$condicion["sector_id"] = $condicones;
	}
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_subsector', $campos,$condicion, "", "nombre_subsector ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())

		{
			$id		= $registro["id_subsector"];
			$nombre		= $registro["nombre_subsector"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectActividad($id,$funcion,$value,$caption)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_actividad"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_actividad', $campos,$condicion, "", "nombre_actividad ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_actividad"];
			$nombre		= $registro["nombre_actividad"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}

function SelectRazon($id,$funcion,$value,$caption)
{

	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_razon"=>1);
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_razon', $campos,$condicion, "", "nombre_razon ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_razon"];
			$nombre		= $registro["nombre_razon"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function Total_Comentario($comentario,$usuario)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("cae_comer_resp.*");
	if(!empty($comentario))
	{
		$condicion["comercio_id"] = $comentario;
	}
	if(!empty($usuario))
	{
		$condicion["usuario_comer_resp"] = $usuario;
	}
	if($consultas->SeleccionarTablaFila('cae_comer_resp', $campos,$condicion, "", "id_comer_resp ASC")==CONSULTAS_SUCCESS)
	{
		$reg['total']=$consultas->NumeroRegistros();
		while($registro =$consultas->Fetch())
		{
			$reg['respuesta']		=$registro["respuesta"];
		}
	}
	return $reg;
}
function time_stamp($session_time)
{
	$time_difference = time() - $session_time ;
	$seconds = $time_difference ;
	$minutes = round($time_difference / 60 );
	$hours = round($time_difference / 3600 );
	$days = round($time_difference / 86400 );
	$weeks = round($time_difference / 604800 );
	$months = round($time_difference / 2419200 );
	$years = round($time_difference / 29030400 );

	if($seconds <= 60)
	{
		$html="Hace $seconds segundos";
	}else if($minutes <=60)
	{
		if($minutes==1)
	 	{
			$html="Hace un minuto";
		}else
		{
			$html="Hace $minutes minutos";
		}
	}else if($hours <=24)
	{
		if($hours==1)
		{
			$html="Hace una hora";
		}else
		{
			$html="Hace $hours horas";
		}
	}else if($days <=7)
	{
		if($days==1)
		{
			$html="Hace un d&iacute;a";
		}else
		{
			$html="Hace $days d&iacute;as";

		}

	}else if($weeks <=4)
	{
		if($weeks==1)
		{
			$html="Hace una semana";
		}else
		{
			$html="Hace $weeks semana";
		}
	}else if($months <=12)
	{
		if($months==1)
		{
			$html="Hace un mes";
		}else
		{
			$html="Hace $months meses";
		}
	}else
	{
		if($years==1)
		{
			$html="Hace un a&ntilde;o";
		}else
		{
			$html="Hace $years a&ntilde;os";
		}
	}
	return $html;
}
function BusqClaveArea($id)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("id_area"=>$id);
	if($consultas->SeleccionarTablaFila('area', $campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_tipo		=$registro["clave_dep"];
		}
	}
	return $id_tipo;
}
function BusqArea($id)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("id_area"=>$id);
	if($consultas->SeleccionarTablaFila('area', $campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$nombre_area		=$registro["nombre_area"];
		}
	}
	return $nombre_area;
}
function SelectOpeCom($id,$funcion,$value,$caption,$area)
{

	$consultas = new CONSULTAS;
	$campos = array("cae_opc_commer.*");
	$condicion=array("status_opc"=>1);
	if(!empty($area))
	{
		$condicion["area_opc"] = $area;
	}
	$html = "<select name='$id' id='$id' $funcion >";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('cae_opc_commer', $campos,$condicion, "", "desc_opc ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		= $registro["id_opc_commer"];
			$nombre		= $registro["desc_opc"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre </option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function BuscarUs_ext($id)
{
    $consultas = new CONSULTAS;
    $campos = array("NOMBRE","APELLIDO","APELLIDO_MAT","id_area,nombre_area");
    $condicion=array("usuarios.AREA=area.id_area and ID"=>$id);
    if($consultas->SeleccionarTablaFila("usuarios,area",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{

			$nombre	= $registro["NOMBRE_CAJ"]." ".$registro["APELLIDO"]." ".$registro["APELLIDO_MAT"];
			$area	= $registro["nombre_area"];
			$html[1]=$nombre;
			$html[2]=$area;
                }
        }
        return $html;
}
function ValorUltimoPago($id)
{
	$valor=array();
   	$consultas = new CONSULTAS;
   	$campos = array("det_liq_2012.total,  pago_predial_2012.especial");
   	$condicion=array("pago_predial_liq_2012.id_solicitud=pago_predial_2012.id_pago and  pago_predial_2012.id_pago=det_liq_2012.id_liqp and det_liq_2012.id_c=ciclos.id_ciclo
	and MONTH(pago_predial_liq_2012.fecha) in (1,2,3) and pago_predial_2012.status_p_p='2' and  pago_predial_liq_2012.status_folio='1' and ciclos.ano_ciclo='2012' and  pago_predial_2012.id_predial"=>$id);
 	if($consultas->SeleccionarTablaFila("pago_predial_liq_2012,pago_predial_2012,det_liq_2012,ciclos",$campos,$condicion, "", "pago_predial_liq_2012.fecha desc")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$valor['total']		= $registro["total"];
			$valor['especial']	= $registro["especial"];
                }
        }
	return $valor;
}
function SelectNotif($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_noti"=>1);
	$html = "<select name='$id' id='$id' $funcion style=\"width:400px\">";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('notificadores_receptorias', $campos,$condicion, "", "id_noti ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["id_noti"];
			$nombre_1	= $registro["nombre_uno"];
			$nombre_2	= $registro["nombre_dos"];
			$nombre_3	= $registro["nombre_tres"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre_1,".$nombre_2.",$nombre_3</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function BuscarNotif($id)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_noti"=>1);
	$condicion=array("id_noti"=>$id);
	if($consultas->SeleccionarTablaFila('notificadores_receptorias', $campos,$condicion, "", "id_noti ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["id_noti"];
			$nombre_1	= $registro["nombre_uno"];
			$nombre_2	= $registro["nombre_dos"];
			$nombre_3	= $registro["nombre_tres"];
			$html .= "$nombre_1,".$nombre_2.",$nombre_3";
		}
	}
	return $html;
}
function selectDia_sem($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	global $MySPANEL;
	$conservacion =array(	"1"	=>	"Lunes",
				"2"	=>	"Martes",
				"3"	=>	"Miercoles",
				"4"	=>	"Jueves",
				"5"	=>	"Viernes",
				"6"	=>	"Sabado",
				"7"	=>	"Domingo",
				);

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value='$i'>$caption</option>";
	foreach($conservacion as  $cantidad=> $Nombre)
	{
			$html .= "<option value='$cantidad' ";
			if($value == $cantidad)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$Nombre</option>";
	}
	$html .= "</select>";
	return $html;
}
function SelectHorarios_Comercios($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_commer_h"=>1);
	$html = "<select name='$id' id='$id' $funcion style=\"width:400px\">";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('comercio_h', $campos,$condicion, "", "tipo ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["id_commer_h"];
			$nombre_1	= $registro["tipo"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".utf8_encode($nombre_1)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectMunicipioD($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("muni.id_edo"=>15);
	$html = "<select name='$id' id='$id' $funcion style=\"width:400px\">";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('muni', $campos,$condicion, "", "nom_mun asc")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["id_mun"];
			$nombre_1	= $registro["nom_mun"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre_1</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoPagoAgua($id,$funcion,$value,$caption)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_tablas_agua"=>1);
	$html = "<select name='$id' id='$id' $funcion style=\"width:400px\">";
	$html .= "<option value='$i'>$caption</option>";
	if($consultas->SeleccionarTablaFila('agua_tablas', $campos,$condicion, "", "nom_t_agua asc")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["tablas_agua_id"];
			$nombre_1	= $registro["nom_t_agua"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre_1</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function HtmlTablesAguas($tales,$medidas=array(),$clas='')
{
	global $_MESES;
	global $__BIMESTRES;

	$consultas = new CONSULTAS;
	$campos = array("agua_tablas.*");
	$condicion=array("tablas_agua_id"=>Sanitizacion($tales),"status_tablas_agua"=>"1");
	$consultas->SeleccionarTablaFila('agua_tablas', $campos, $condicion,"", "nom_tablas_agua asc");
	while($registro =$consultas->Fetch())
	{
		$tabla 		= $registro["nom_tablas_agua"];
		$grupo		= $registro["grupo"];
		$periodo	= $registro["periodo_pago"];
		$MyTCicloAgua	= new TCICLO_AGUA;
		#$html.="$grupo $tabla ";
		$coleccion=$MyTCicloAgua->ColeccionTablesCalculo(Sanitizacion($tabla));
		$total=$MyTCicloAgua->getTotal();
		$html.="<input type='hidden' name='grupo_t' id='grupo_t' velue='$grupo'>
			<table width='100%' border='0' cellpadding='0' cellspacing='0' >";
		if($grupo==1){
			$html.="<tr>
				<td class='THtitle'>Consumo Inicial</td>
				<td class='THtitle'>Consumo Final</td>
				<td class='THtitle'>Cuota Minima</td>
				<td class='THtitle'>Cuota Adicional</td>
				</tr>";
		}else{
			$html.="<tr>
				<td class='THtitle'>Seleccionar</td>
				<td class='THtitle'>Rango</td>
				<td class='THtitle'>Tipo</td>
				</tr>";
		}
		if($total>0)
		{
			foreach($coleccion as $coleccion)
			{
				$id		= $MyTCicloAgua->getID($coleccion);
				$c_fin		= $MyTCicloAgua->getConsumo_fin($coleccion);
				$c_ini		= $MyTCicloAgua->getConsumo_ini($coleccion);
				$cuota_min	= $MyTCicloAgua->getCuota_min($coleccion);
				$cuota_adic	= $MyTCicloAgua->getCuota_adic($coleccion);
				$rango		= $MyTCicloAgua->getRango($coleccion);
				$tipo		= $MyTCicloAgua->getTipo($coleccion);
				$region		= $MyTCicloAgua->getRegion($coleccion);
				$status		= $MyTCicloAgua->getStatus($coleccion);
				$tablas		= $MyTCicloAgua->getSu_table($coleccion);
				if($grupo==1){
					$html.="<tr>
					<td>$c_ini</td>
					<td>$c_fin</td>
					<td>$cuota_min</td>
					<td>$cuota_adic</td>
					</tr>";
				}else{
					$html.="<tr>
					<td><input type='radio' name='rangos' id='rangos' value='$id' onClick='Realizar_Calculo2(\"$id\",\"$tabla\")'";
					if($clas==$id){
						$html.=" checked='checked'";
					}
					$html.="/>
						<input type='hidden' name='v_rangos$id' id='v_rangos$id' value='$rango' />
					</td>
					<td>$rango</td>
					<td>$tipo</td>
					</tr>";
				}
			}
		}
		$html.="</table>";
		if($grupo==1){
			if($periodo == 'M'){
				$html2="<table  width='100%' border='0' cellpadding='0' cellspacing='0'  >";
					$html2.="<tr><td>Inicial</td><td><input type='text' name='inicial' id='inicial' value=''></td></tr>";
				foreach($_MESES as $key => $value){
					$html2.="<tr><td>$value</td><td><input type='text' name='lect_$key' id='lect_$key' value='".$medidas['Medidas'][$key]."'></td></tr>";
				}
				$html2.="</table>";
			}elseif($periodo == 'B'){
				$html2="<table  width='100%' border='0' cellpadding='0' cellspacing='0'  >";
					$html2.="<tr><td>Inicial</td><td><input type='text' name='inicial' id='inicial' value=''></td></tr>";
				foreach($__BIMESTRES as $key => $value){
					$html2.="<tr><td>$value</td><td><input type='text' name='lect_$key' id='lect_$key' value=''></td></tr>";
				}
				$html2.="</table>";
			}
			$html3="<input type=\"button\" value=\"Calcular\" onclick='realizar_calculo(\"$tabla\")' >";
		}
	}
	$html_array= array(0=>$html,1=>$html2,2=>$periodo,3=>$html3);
	return $html_array;
}
function Drenaje($importe)
{
	$drenaje = (($importe*8)/100);
	return $drenaje;
}
function BuscarTablaCalculoAgua($id)
{
	global $MySPANEL;
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_tablas_agua"=>1,"tablas_agua_id"=>$id);

	if($consultas->SeleccionarTablaFila('agua_tablas', $campos,$condicion, "", "nom_t_agua asc")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area	= $registro["tablas_agua_id"];
			$nombre		= $registro["nom_tablas_agua"];
			$nombre_t	= $registro["nom_t_agua"];
			$grupo		= $registro["grupo"];
			$html['grupo']	= $grupo;
			$html['nombre']	= $nombre;
			$html['nombre_t']	= $nombre_t;
			$html['id']	= $id_area;
		}
	}
	return $html;
}
function HtmlCalculosPago($valores=array(),$tipo_pago)
{

	global $_MESES;
	global $__BIMESTRES;

	print_r ($valores);
	$html="<table  width='100%' border='0' cellpadding='0' cellspacing='0'  >";
	foreach($valores as $key => $value)
	{
		$html.="<tr><td>";
		if($tipo_pago == 'M'){
			$html.=$_MESES[$key];
		}elseif($tipo_pago == 'B'){
			$html.=$__BIMESTRES[$key];
		}
		$html.="</td><td>$value<input type='hidden' name='pagar_".$key."' id='pagar_".$key."' value='$value' >
			</td></tr>";

	}
	$html.='</table>';
	return $html;
}

function HtmlCalculos_Pago2($valores,$tipo_pago)
{

	global $_MESES;
	global $__BIMESTRES;
	$html="<table  width='100%' border='0' cellpadding='0' cellspacing='0'  >";
	if($tipo_pago == 'M'){
		foreach($valores as $key => $value)
		{
			$html.="<tr><td>".$_MESES[$key]."</td><td>$value
			<input type='hidden' name='pagar_$key' id='pagar_$key' value='$value'></td></tr>";
		}
	}elseif($tipo_pago == 'B'){
		foreach($valores as $key => $value)
		{

			$html.="<tr><td>".$__BIMESTRES[$key]."</td><td>$value
			<input type='hidden' name='pagar_$key' id='pagar_$key' value='$value'></td></tr>";
		}
	}
	$html.='</table>';

	return $html;
}
?>
