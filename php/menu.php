	<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="img/avatars/sunny.png" alt="me" class="online" />
						<span><?php echo $MySPANEL->Usuario(); ?></span>
						<i class="fa fa-angle-down"></i>
					</a>

				</span>
			</div>
			<!-- end user info -->
			<nav>
	<ul>
		<li <?php
			$breadcrumb='';
			if($MyLogic-> MyCommand()==HOME){
		?>class="active"<?php
			}
		?> >
			<a href="<?php echo $MyLogic->base_url();?>index.php" title="Dashboard">
				<i class="fa fa-lg fa-fw fa-home"></i>
				<span class="menu-item-parent">Home</span>
			</a>
		</li>
<?php
$i = 0;

foreach($MyLogic->MyMenu() as $key => $value)
{
	list($caption, $img)	= explode(":",$key);
	?>
		<li <?php if(multi_in_array($MyLogic->MyCommand(),$value) || multi_in_array(multi_in_array_especial($MyLogic->MyCommand(),$MyLogic->MyDMenu()),$value)){ ?>class="active" <?php } ?> >
			<a href="#"><i class="fa <?php echo $img;?>"></i> <span class="menu-item-parent"><?php echo $caption; ?></span></a>
			<ul>
	<?php
	foreach($value as $index => $valor)
	{
		if ($MySPANEL->AccesoPermitido($uiCommand[$valor][0],$uiCommand[$valor][1], $MySPANEL->USUARIO()) != ACCESS_DENIED)
		{
			list($caption, $imgs)	= explode(":",$index);
	?>
			<li <?php if($MyLogic->MyCommand()==$valor || (multi_in_array_especial($MyLogic->MyCommand(),$MyLogic->MyDMenu())== $valor)){ $breadcrumb="<li>$caption</li>"; ?>	class="active" <?php } ?> >
				<?php
				print ("<a href=\"");
				if(substr($valor,0,7) == "http://")
				{
					print $valor;
				}
				else
				{
					print ($MyLogic->base_url()."index.php?command=".$valor);
				}
				print ("\" title=\"$caption\">");
				echo $caption;?></a>
			</li>
	<?php
			if((multi_in_array_especial($MyLogic->MyCommand(),$MyLogic->MyDMenu())== $valor)){
				$breadcrumb="<li><a href=\"";
				if(substr($valor,0,7) == "http://")
				{
					$breadcrumb.="index.php?command=".$valor;
				}
				else
				{
					$breadcrumb.=$MyLogic->base_url()."index.php?command=".$valor;
				}
				$breadcrumb.="\" title=\"$caption\">$caption</a></li><li>".$MyLogic->Title($MyLogic->MyCommand())."</li>";
			}
		}
	}
	?>		</ul>
		</li>
	<?php
}
?>
		</ul>

			</nav>
			<span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

		</aside>
		<!-- END NAVIGATION -->
