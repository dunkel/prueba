<?php
session_start();
include_once('class/class.debug.php');
$MyDebug->SetDebug(0);
$MyDebug->DebugError();
include_once('util.php');
include_once('class/class.consultas.php');
include_once('class/class.login.panel.php');
include_once('class/class.logic.panel.php');

$username	= isset($CONTEXT['username'])			? 	strip_tags($CONTEXT['username']) 		: "";
$password	= isset($CONTEXT['password'])			? 	strip_tags($CONTEXT['password']) 		: "";
$remember	= isset($CONTEXT['remember'])			? 	strip_tags($CONTEXT['remember']) 		: "";

$MyLogin = new LOGINPANEL;
if($_SESSION['intentos']<3)
{
	if($MyLogin->LOGINE($username, $password)==LOGIN_SUCCESS)
	{
		$MySPANEL->SetVar('my_oda_user',	$MyLogin->USUARIO());
		$MySPANEL->SetVar('my_oda_email',	$MyLogin->EMAIL());
		$MySPANEL->SetVar('my_oda_id',		$MyLogin->ID());
		$MySPANEL->SetVar('my_oda_nivel',	$MyLogin->NIVEL());

		$location = $MyLogic->base_url()."index.php?command=".HOME;
		if($_SESSION['URL_REDIREC_COMMAND']!=HOME){
			$location = $_SESSION['URL_REDIREC'];
		}
		$_SESSION['intentos']='';
	}
	else
	{
		$http_vars["MsgErr"] = "El nombre de usuario y/o contrase&ntilde;a no son correctos";
		$_SESSION['intentos']++;
		$location = $MyLogic->base_url()."index.php?command=".LOGIN;

	}
}else
{
	if (!empty($_REQUEST['captcha']))
	{
		if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha'])
		{
        		$http_vars["MsgErr"] = "El captcha es invalido";
			$_SESSION['intentos']++;
			$location = $MyLogic->base_url()."index.php?command=".LOGIN;
    		}else
		{
			if($MyLogin->LOGINE($username,$password)==LOGIN_SUCCESS)
			{
				$MySPANEL->SetVar('my_oda_user',	$MyLogin->USUARIO());
				$MySPANEL->SetVar('my_oda_email',	$MyLogin->EMAIL());
				$MySPANEL->SetVar('my_oda_id',		$MyLogin->ID());
				$MySPANEL->SetVar('my_oda_nivel',	$MyLogin->NIVEL());
				$location = "index.php?command=".HOME;
				if($_SESSION['URL_REDIREC_COMMAND']!=HOME){
					$location = $MyLogic->base_url()."index.php?".$_SESSION['URL_REDIREC'];
				}
				$_SESSION['intentos']='';
			}
			else
			{
				$http_vars["MsgErr"] = "El nombre de usuario y/o contrase&ntilde;a no son correctos";
				$_SESSION['intentos']++;
				$location = $MyLogic->base_url()."index.php?command=".LOGIN;

			}
		}
		unset($_SESSION['captcha']);

	}else{
		$http_vars["MsgErr"] = "El captcha esta vacio";
		$_SESSION['intentos']++;
		$location = $MyLogic->base_url()."index.php?command=".LOGIN;
	}
}

$_SESSION["cookie_http_vars"] = $http_vars;
$MyDebug->Dump();
header("HTTP/1.1 302 Moved Temporarily");
header("Location: $location");
?>
