<?php

$MyDebug = new MYDEBUG();

class MYDEBUG
{
	var $m_msg;
	var $m_debugOn;
	var $m_time_inicio ;

	function MYDEBUG()
	{
		$this->I_Init();
		$this->m_time_inicio = $this->DebugMicrotime();
	}

	function I_Init()
	{
		$this->m_msg = "";
		$this->m_debugOn = 0;
	}

	function DebugMessage( $msg )
	{
		if ( $this->m_debugOn )
		{
			$this->m_msg .= $msg . "<br />\n";
		}
	}

	function DebugEnabled()
	{
		return ($this->m_debugOn ? 1 : 0);
	}

	function SetDebug( $debugOn )
	{
		$this->m_debugOn = $debugOn;
	}

	function Dump()
	{
		if ( $this->m_debugOn )
		{
			print("<div class=\"dbgText\">" . $this->m_msg . "</div>\n");
			print("<div class=\"dbgText\">" . ($this->DebugMicrotime()-$this->m_time_inicio) . "</div>\n");
		}
	}

	function DumpArray( $arrayName, $array=array() )
	{
		if(!empty($array) && count($array))
		{
			foreach( $array as $k => $v )
			{
				$this->DebugMessage( "$arrayName.[$k] = [$v]" );
			}
		}
	}


	function DebugInclude( $file )
	{
		if ( $this->m_debugOn )
		{
			print("<div class=\"dbgInclude\">" . $file . "</div>");
		}
	}

	function DebugError()
	{
		if ( $this->m_debugOn )
		{
			ini_set('display_errors', 1);
			error_reporting(E_ALL);
		}
	}

	function DebugMicrotime()
	{
		list($useg, $seg) = explode(" ", microtime());
		return ((float)$useg + (float)$seg);
	}

}
?>
