<?php
abstract class IBD extends PDO
{
	var $m_dbResultados;
	var $m_link;
	var $m_tipo_fetch;
	var $m_conectarbd;
	var $m_validate;
	
	function Limpiar()
	{
		$m_link=0;
		$m_conectarbd='';
		$m_tipo_fetch='';
		$m_validate='0';
		$m_dbResultados=array();
	}
		
	function ConectarBD()
	{
		global $MyDebug;

		try {
			$port='';
			if(!empty(_BD_PORT_)){
				$port="port="._BD_PORT_.";";
			}
			$link = new PDO("mysql:dbname="._BD_NAME_.";".$port."host="._BD_SERVER_, _BD_USER_, _BD_PASSWD_);
		} catch (PDOException $e) {
			if($e->getCode()=='1049'){
				$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede Seleccionar la base de datos.");		
				return IBD_ERR_CANTSELECT;	
			}elseif($e->getCode()=='2005'){
				$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede hacer la conexion.");
				return IBD_ERR_CANTCONNECT;
			}elseif($e->getCode()=='1045'){
				$MyDebug->DebugMessage("IBD::ConectarBD(): Acceso denegado a la base de datos.");
				return IBD_ERR_CANTACCESSDENIED;
			}			
		}
		$this->m_link=$link;
		return IBD_SUCCESS;
	}
	
	function Query($origen, $consulta)
	{
		global $MyDebug;
		
		if ($this->m_conectarbd!=IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("IBD::Query(): Fallo en la conexion.");
			return $this->m_conectarbd;			
		}
		$result = $this->m_link->prepare($consulta);
		$this->m_validate = $result->execute();
		if(!$this->m_validate)
		{
			$MyDebug->DebugMessage("IBD::Query(): Error en la consulta");
			return IBD_ERR_DBUNAVAILABLE;
		}
		$this->m_dbResultados[$origen]=$result;
		return IBD_SUCCESS;
	}
		
	function FetchIbd($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($this->m_validate)
		{
			return $dbResultados->fetch($this->m_tipo_fetch);
		}
		
		return 0;
	}		
	
	function Liberar($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($this->m_validate)
		{
			$dbResultados->closeCursor();
			$this->m_dbResultados[$origen]=0;
		}
		
		return IBD_SUCCESS;
	}
	
	function NumeroRows($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($this->m_validate)
		{
			return $dbResultados->rowCount();
		}
		
		return 0;
	}
	
	function UltimoID()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->lastInsertId();
	}
	public function Autocommit($valor='TRUE')
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->beginTransaction();
	}
	public function Commit()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->commit();
	}
	public function Rollback()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->rollback();
	}		
}
?>
