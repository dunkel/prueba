<?php
$iCONSULTASNextError=200;

define ("CONSULTAS_SUCCESS", 		$iCONSULTASNextError++);
define ("CONSULTAS_ERR_NOROWS", 	$iCONSULTASNextError++);

$iIBDNextError=200;

define ("IBD_SUCCESS",			$iIBDNextError++);
define ("IBD_ERR_CANTCONNECT",		$iIBDNextError++);
define ("IBD_ERR_CANTSELECT",		$iIBDNextError++);
define ("IBD_ERR_DBUNAVAILABLE",	$iIBDNextError++);

include_once('parameters.php');

define ("_IBD_SELECT_", 		$parameters['TIPO']);
define ("_BD_SERVER_", 			$parameters['BDSERVER']);
define ("_BD_NAME_", 			$parameters['BDNAME']);
define ("_BD_USER_", 			$parameters['BDUSER']);
define ("_BD_PASSWD_", 			$parameters['BDPASSWD']);
define ("_BD_PORT_", 			$parameters['BDPORT']);
define ("FETCH_OBJ",			PDO::FETCH_OBJ);


include_once('class.ibd.'._IBD_SELECT_.'.php');

class CONSULTAS extends IBD
{
	var $m_ibd;
	var $m_key;
	var $m_num;
	var $m_query_total;

	function CONSULTAS()
	{
		$this->Iniciar();
	}

	function Iniciar()
	{
		$this->Limpiar();
		$this->m_conectarbd=$this->ConectarBD();
		$this->m_num = 0;
		$this->m_query_total = '';
	}

	function AgregarRegistro($tabla, $nvoregistro)
	{
		global $MyDebug;
		$llaveDB = '';
		$valorDB = '';

		foreach ($nvoregistro as $llave => $valor )
		{
			$llaveDB .= "$llave, ";
			$valorDB .= "'$valor', ";
		}

		$llaveDB = substr($llaveDB, 0, -2);

		$valorDB = substr($valorDB, 0, -2);

		$sql = "INSERT INTO $tabla ($llaveDB) VALUES ($valorDB)";

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro:Query[$sql]");

		if (($result = $this->Query("AGREGAR$tabla", $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla], [".implode(",",$nvoregistro)."]): Fallo [$result]");
			return $result;
		}

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla], [".implode(",",$nvoregistro)."]): Proceso Satisfactorio");
		return CONSULTAS_SUCCESS;
	}
	function AgregarRegistroSinComillas($tabla, $nvoregistro)
	{
		global $MyDebug;
		$llaveDB = '';
		$valorDB = '';

		foreach ($nvoregistro as $llave => $valor )
		{
			$llaveDB .= "$llave, ";
			$valorDB .= "$valor, ";
		}

		$llaveDB = substr($llaveDB, 0, -2);

		$valorDB = substr($valorDB, 0, -2);

		$sql = "INSERT INTO $tabla ($llaveDB) VALUES ($valorDB) ";

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistroSpecial:Query[$sql]");

		if (($result = $this->Query("AGREGAR$tabla", $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla], [".implode(",",$nvoregistro)."]): Fallo [$result]");
			return $result;
		}

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla], [".implode(",",$nvoregistro)."]): Proceso Satisfactorio");
		return CONSULTAS_SUCCESS;
	}
	function AgregarRegistroSpecial($tabla, $nvoregistro)
	{
		global $MyDebug;
		$llaveDB = '';
		$valorDB = '';

		foreach ($nvoregistro as $llave => $valor )
		{
			$llaveDB .= "$llave, ";
			$valorDB .= "$valor, ";
		}

		$llaveDB = substr($llaveDB, 0, -2);

		$valorDB = substr($valorDB, 0, -2);

		$sql = "INSERT INTO $tabla ($llaveDB) SELECT $valorDB ";

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistroSpecial:Query[$sql]");

		if (($result = $this->Query("AGREGAR$tabla", $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla],[".implode(",",$nvoregistro)."]): Fallo [$result]");
			return $result;
		}

		$MyDebug->DebugMessage("CONSULTAS::AgregarRegistro([$tabla],[".implode(",",$nvoregistro)."]): Proceso Satisfactorio");
		return CONSULTAS_SUCCESS;
	}
	function ActualizarRegistro($tabla, $nvoregistro, $condiciones)
	{
		global $MyDebug;
		$nvoregistroDB = '';
		$condicionesDB = '';

		$parametros = "[$tabla], [".implode(",",$nvoregistro)."], [".implode(",",$condiciones)."]";

		foreach ($nvoregistro as $llave => $valor )
		{
			$nvoregistroDB .= "$llave=$valor, ";
		}

		$nvoregistroDB = substr($nvoregistroDB, 0, -2);

		foreach ($condiciones as $llave => $valor)
		{
			$condicionesDB .= "$llave='$valor' and ";
		}

		$condicionesDB = substr($condicionesDB, 0, -5);

		$sql = "UPDATE $tabla SET $nvoregistroDB WHERE $condicionesDB";

		$MyDebug->DebugMessage("CONSULTAS::ActualizarRegistro:Query[$sql]");

		if (($result = $this->Query("UPDATE$tabla", $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::ActualizarRegistro($parametros): Fallo [$result]");
			return $result;
		}

		$MyDebug->DebugMessage("CONSULTAS::ActualizarRegistro($parametros): Proceso Satisfactorio");

		return CONSULTAS_SUCCESS;
	}
	function StoredProcedura($tabla, $nvoregistro)
	{
		global $MyDebug;
		$nvoregistroDB = '';
		$condicionesDB = '';

		$parametros = "[$tabla], [".implode(",",$nvoregistro)."]";

		foreach ($nvoregistro as $llave)
		{
			$nvoregistroDB .= "$llave, ";
		}

		$nvoregistroDB = substr($nvoregistroDB, 0, -2);

		$sql = "CALL $tabla ($nvoregistroDB)";
		$key = "callsp$tabla";

		$MyDebug->DebugMessage("CONSULTAS::StoredProcedura:Query[$sql]");

		if(($result=$this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::StoredProcedura($parametros): Fallo [$result]");
			return $result;
		}

		if(($result=$this->NumeroRows($key)) <1 )
		{
			$MyDebug->DebugMessage("CONSULTAS::StoredProcedura($parametros): Fallo [$result] registros");
			$this->Liberar($key);
			return CONSULTAS_ERR_NOROWS;
		}
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::StoredProcedura($parametros): Proceso Satisfactorio");
		return CONSULTAS_SUCCESS;
	}
	function EliminarRegistro($tabla, $condiciones)
	{
		global $MyDebug;
		$condicionesDB = '';

		foreach ($condiciones as $llave => $valor)
		{
			$condicionesDB .= "$llave='$valor' and ";
		}

		$condicionesDB = substr($condicionesDB, 0, -5);

		$sql = "DELETE FROM $tabla WHERE $condicionesDB";

		$MyDebug->DebugMessage("CONSULTAS::EliminarRegistro:Query[$sql]");

		if (($result=$this->Query("DELETE$tabla", $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::EliminarRegistro([$tabla], [".implode(",",$condiciones)."]): Fallo [$result]");
			return $result;
		}

		$MyDebug->DebugMessage("CONSULTAS::EliminarRegistro([$tabla], [".implode(",",$condiciones)."]): Proceso Satisfactorio");

		return CONSULTAS_SUCCESS;
	}

	function SeleccionarTodo($tabla, $campos, $group, $order)
	{
		global $MyDebug;
		$camposDB = '';

		$parametros = "[$tabla], [".implode(",",$campos)."], [$group], [$order]";

		foreach ($campos as $valor)
		{
			$camposDB .= "$valor, ";
		}

		$camposDB = substr($camposDB, 0, -2);

		$sql = "SELECT  $camposDB FROM $tabla ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}
		$count_sql = $sql;
		if(!empty($order))
		{
			$sql .= " ORDER BY $order ";
		}

		$key = "select$tabla";

		$MyDebug->DebugMessage("CONSULTAS::SeleccionarTodo:Query[$sql]");

		if(($result=$this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarTodo($parametros): Fallo [$result]");

			return $result;
		}

		if(($result=$this->NumeroRows($key)) < 1)
		{
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarTodo($parametros): Fallo [$result] registros");
			$this->Liberar($key);
			return CONSULTAS_ERR_NOROWS;
		}
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::SeleccionarTodo($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace($camposDB," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}

	function SeleccionarTablaFila($tabla, $campos, $condiciones, $group, $order)
	{
		global $MyDebug;
		$camposDB = '';
		$condicionesDB = '';

		$parametros="[$tabla],[".implode(",",$campos)."],[".implode(",",$condiciones)."],[$group],[$order]";

		foreach ($campos as $valor)
		{
			$camposDB .= "$valor, ";
		}

		$camposDB = substr($camposDB, 0, -2);

		foreach ($condiciones as $llave => $valor)
		{
			$condicionesDB .= "$llave='$valor' and ";
		}

		$condicionesDB = substr($condicionesDB, 0, -5);

		$sql = "SELECT $camposDB FROM $tabla WHERE $condicionesDB ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}

		$count_sql = $sql;

		if(!empty($order))
		{
			$sql .= "ORDER BY $order";
		}

		$key = "selecttf$tabla";

		$MyDebug->DebugMessage("CONSULTAS::SeleccionarTablaFila:Query[$sql]");

		if(($result=$this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarTablaFila($parametros): Fallo [$result]");
			return $result;
		}

		if(($result=$this->NumeroRows($key)) <1 )
		{
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarTablaFila($parametros): Fallo [$result] registros");
			$this->Liberar($key);
			return CONSULTAS_ERR_NOROWS;
		}
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::SeleccionarTablaFila($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace($camposDB," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}



	function BuscarTexto($tabla, $campos, $condiciones, $group, $order)
	{
		global $MyDebug;
		$camposDB = '';
		$condicionesDB = '';

		$parametros = "[$tabla], [".implode(",",$campos)."], [".implode(",",$condiciones)."], [$group], [$order]";

		foreach ($campos as $campo)
		{
			$camposDB .= "$campo, ";
		}
		$camposDB = substr($camposDB, 0, -2);

		foreach ($condiciones as $llave => $valor)
		{
			$condicionDB .= "$llave like '%$valor%' or ";
		}

		$condicionDB = substr($condicionDB, 0, -4);

		$sql = "SELECT $camposDB FROM $tabla WHERE $condicionDB ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}
		$count_sql = $sql;
		if(!empty($order))
		{
			$sql .= "ORDER BY $order";
		}

		$key = "BuscarTexto$tabla";

		$MyDebug->DebugMessage("CONSULTAS::BuscarTexto:Query[$sql]");

		if(($result = $this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::BuscarTexto($parametros): Fallo [$result]");
			return $result;
		}

		if(($result = $this->NumeroRows($key)) <1 )
		{
			$this->Liberar($key);
			$MyDebug->DebugMessage("CONSULTAS::BuscarTexto($parametros): Fallo [$result] registros");
			return CONSULTAS_ERR_NOROWS;
		}

		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::BuscarTexto($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace($camposDB," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}

	function QueryINNERJOIN($inner, $campos, $condicion, $group, $order )
	{
		global $MyDebug;
		$camposDB	= '';
		$condicionDB	= '';
		$innerDB	= '';
		$tabla		= '';
		$parametros = "[$inner], [".implode(",",$campos)."], [".implode(",",$condiciones)."], [$group], [$order]";

		foreach ($campos as $campo)
		{
			$camposDB .= "$campo, ";
		}
		$camposDB = substr($camposDB, 0, -2);

		foreach ($condicion as $llave => $valor)
		{
			$condicionDB .= "$llave='$valor' and ";
		}

		$condicionDB = substr($condicionDB, 0, -5);

		foreach ($inner as $llave => $valor)
		{
			list($table1, $table2) = explode(":", $llave);
			$innerDB .= "$table1 INNER JOIN $table2 ON $valor ";
			$tabla	.= "$table1$table2";
		}

		$innerDB = substr($innerDB, 0, -1);

		$sql = "SELECT $camposDB FROM $innerDB WHERE $condicionDB ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}

		$count_sql = $sql;

		if(!empty($order))
		{
			$sql .= "ORDER BY $order";
		}

		$key = "Inner$tabla";

		$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN:Query[$sql]");

		if(($result = $this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Fallo [$result]");
			return $result;
		}

		if(($result=$this->NumeroRows($key)) < 1 )
		{
			$this->Liberar($key);
			$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Fallo [$result] registros");
			return CONSULTAS_ERR_NOROWS;
		}
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace($camposDB," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}

	function QueryTIPOJOIN($inner, $campos, $condicion, $group, $order )
	{
		global $MyDebug;
		$camposDB	= "";
		$condicionDB	= "";
		$innerDB	= "";
		$parametros = "[".implode(",",$inner)."], [".implode(",",$campos)."], [".implode(",",$condicion)."], [$group], [$order]";
		$parametros = "";
		foreach ($campos as $campo)
		{
			$camposDB .= "$campo, ";
		}
		$camposDB = substr($camposDB, 0, -2);

		foreach ($condicion as $llave => $valor)
		{
			$condicionDB .= "$llave='$valor' and ";
		}

		$condicionDB = substr($condicionDB, 0, -5);
		$tabla="-";
		foreach ($inner as $llave => $valor)
		{
			list($table1, $table2,$tinner) = explode(":", $llave);
			$tipo_inner=(!empty($tinner))?$tinner:'INNER JOIN';
			$innerDB .= "$table1 $tipo_inner $table2 ON $valor ";
			$tabla.="$llave-";
		}

		$innerDB = substr($innerDB, 0, -1);

		$sql = "SELECT $camposDB FROM $innerDB WHERE $condicionDB ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}

		$count_sql = $sql;

		if(!empty($order))
		{
			$sql .= "ORDER BY $order";
		}

		$key = "TipoInner$tabla";

		$MyDebug->DebugMessage("CONSULTAS::QueryTIPOJOIN:Query[$sql]");

		if(($result = $this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Fallo [$result]");
			return $result;
		}

		if(($result=$this->NumeroRows($key)) < 1 )
		{
			$this->Liberar($key);
			$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Fallo [$result] registros");
			return CONSULTAS_ERR_NOROWS;
		}
		
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::QueryINNERJOIN($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace($camposDB," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}
	function SeleccionarUnionTablaFila($select=array(), $group, $order)
	{
		global $MyDebug;
		$parametros 	= "";
		$tabla		= "";
		$sql 		= "SELECT * FROM (";
		foreach($select as $tablas => $registros)
		{
			$camposDB = "";
			$condicionesDB = "";
			foreach ($registros['campos'] as $valor)
			{
				$camposDB .= "$valor, ";
			}

			$camposDB = substr($camposDB, 0, -2);

			foreach ($registros['condiciones'] as $llave => $valor)
			{
				$condicionesDB .= "$llave='$valor' and ";
			}

			$condicionesDB = substr($condicionesDB, 0, -5);
			$parametros .= "[$tablas], [".implode(",",$registros['campos'])."], [".implode(",",$registros['condiciones'])."],[UNION], [$group], [$order],";
			$sql .= "SELECT $camposDB FROM ".$tablas." WHERE $condicionesDB UNION ";
			$tabla .= "$tablas";
		}
		$sql = substr($sql, 0, -6);

		$sql .= ") tablaunion ";

		if(!empty($group))
		{
			$sql .= "GROUP BY $group ";
		}

		$count_sql = $sql;

		if(!empty($order))
		{
			$sql .= "ORDER BY $order";
		}
		$parametros.= "[$group], [$order]";
		//echo "<br />".$sql;
		$key = "UNIO$tabla";

		$MyDebug->DebugMessage("CONSULTAS::SeleccionarUnionTablaFila:Query[$sql]");

		if(($result = $this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarUnionTablaFila($parametros): Fallo [$result]");
			return $result;
		}

		if(($result=$this->NumeroRows($key)) < 1 )
		{
			$this->Liberar($key);
			$MyDebug->DebugMessage("CONSULTAS::SeleccionarUnionTablaFila($parametros): Fallo [$result] registros");
			return CONSULTAS_ERR_NOROWS;
		}
		$this->m_key=$key;
		$MyDebug->DebugMessage("CONSULTAS::SeleccionarUnionTablaFila($parametros): Proceso Satisfactorio");

		$this->m_query_total = str_replace("*"," COUNT(*) total ",$count_sql);

		return CONSULTAS_SUCCESS;
	}
	function Fetch()
	{
		global $MyDebug;

		if ( ! $this->m_key )
		{
			$MyDebug->DebugMessage("CONSULTAS::Fetch(): Null key.");
			return 0;
		}

		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::Fetch(): Null IBD");
			return 0;
		}

		return $this->FetchIbd( $this->m_key );

	}

	function NumeroRegistros()
	{
		global $MyDebug;
		
		if ( ! $this->m_key )
		{
			$MyDebug->DebugMessage("CONSULTAS::NumeroRegistros(): Null key.");
			return 0;
		}

		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::NumeroRegistros(): Null IBD");
			return 0;
		}
		$this->CountNumeroRegistros();
		return $this->m_num;
	}

	function CountNumeroRegistros()
	{
		global $MyDebug;
		$sql = $this->m_query_total;
		if ( ! $sql )
		{
			$MyDebug->DebugMessage("CONSULTAS::CountNumeroRegistros(): Null sql");
			return 0;
		}
		$pos = strpos($sql, 'GROUP BY');
		if ($pos === true) {
			$sql = "select count(*) total from  ($sql) tmp";
		}
		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::CountNumeroRegistros(): Null IBD");
			return 0;
		}
		$key = str_replace(array(' ',','),'',$this->m_key);
		$key ='Count'.$key;

		$MyDebug->DebugMessage("CONSULTAS::CountNumeroRegistros:Query[$sql]");

		if(($result = $this->Query($key, $sql)) != IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("CONSULTAS::CountNumeroRegistros($key,$sql): Fallo [$result]");
			return $result;
		}

		while($registro = $this->FetchIbd( $key ))
		{
			if($this->m_tipo_fetch==FETCH_OBJ){
				$this->m_num	= $registro->total;
			}else{
				$this->m_num	= $registro["total"];
			}
		}
		$this->Liberar( $key );
	}

	function NumeroRegistrosConsulta()
	{
		global $MyDebug;

		if ( ! $this->m_key )
		{
			$MyDebug->DebugMessage("CONSULTAS::NumeroRegistrosConsulta(): Null key.");
			return 0;
		}

		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::NumeroRegistrosConsulta(): Null IBD");
			return 0;
		}

		return $this->NumeroRegistros($this->m_key);
	}

	function Free()
	{
		global $MyDebug;

		if ( ! $this->m_key )
		{
			$MyDebug->DebugMessage("CONSULTAS::Free(): Null key.");
			return 0;
		}

		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::Free(): Null IBD");
			return 0;
		}

		$this->Liberar( $this->m_key );
		$this->Iniciar();
		return CONSULTAS_SUCCESS;
	}

	function UltimoID()
	{
		if ( ! $this )
		{
			return 0;
		}
		return $this->UltimoInsertID();
	}


	function NuevaConsulta()
	{
		global $MyDebug;

		if ( ! $this->m_key )
		{
			$MyDebug->DebugMessage("CONSULTAS::NuevaConsulta(): Null key.");
			return 0;
		}

		if ( ! $this )
		{
			$MyDebug->DebugMessage("CONSULTAS::NuevaConsulta(): Null IBD");
			return 0;
		}
		$this->Liberar( $this->m_key );
		$this->m_key = 0;
		$this->m_num = 0;
		$this->m_query_total = '';
		return CONSULTAS_SUCCESS;
	}
}

$consultas	= new CONSULTAS;
?>
