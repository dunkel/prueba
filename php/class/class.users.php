<?php
include_once('class.plantilla.php');

$ErrUser = 200;
define("US_SUCCESS", 		$ErrUser++);
define("US_ERROR", 		$ErrUser++);
define("US_VACIO", 		$ErrUser++);

		
class USUARIOS
{
	var $m_total;
	var $m_consultas;
	var $m_id;
	var $m_plantilla;
	var $m_username;
	var $m_email;
	var $m_nivel;
	var $m_nombre;
	var $m_ape_mat;
	var $m_ape_pat;
	var $m_rfc;
	var $m_curp;
	var $m_status;
	var $m_ultimoacceso;
	var $m_id_mun;
	var $m_mun;
	var $m_id_edo;
	var $m_edo;
	var $m_colonia;
	var $m_cp;
	var $m_domicilio;
	var $m_nom_con;
	var $m_tel_con;
	var $m_email_con;
	
	function USUARIOS()
	{
		$this->I_Init();	
	}
	
	function I_Init()
	{
		$this->m_id		= array();
		$this->m_username	= array();	
		$this->m_email		= array();	
		$this->m_nivel		= array();
		$this->m_nombre		= array();
		$this->m_ape_mat	= array();
		$this->m_ape_pat	= array();
		$this->m_rfc		= array();
		$this->m_curp		= array();
		$this->m_status		= array();
		$this->m_fecha		= array();
		$this->m_ultimoacceso	= array();
		$this->m_id_mun		= array();
		$this->m_mun		= array();
		$this->m_id_edo		= array();
		$this->m_edo		= array();
		$this->m_cp		= array();
		$this->m_dimicilio	= array();
		$this->m_nom_con	= array();
		$this->m_tel_con	= array();
		$this->m_email_con	= array();
		$this->m_total		= "0";
		$this->m_consultas	= new CONSULTAS;
		$this->m_plantilla	= new Plantilla();
	}
	
	function getID($id)
	{
		return $this->m_id[$id];	
	}
	function getUsername($id)
	{
		return $this->m_username[$id];	
	}
	function getEmail($id)
	{
		return $this->m_email[$id];	
	}
	function getNombre($id)
	{
		return $this->m_nombre[$id];	
	}
	function getApe_pat($id)
	{
		return $this->m_ape_pat[$id];	
	}
	function getApe_mat($id)
	{
		return $this->m_ape_mat[$id];	
	}	
	function getDomicilio($id)
	{
		return $this->m_domicilio[$id];	
	}
	function getRFC($id)
	{
		return $this->m_rfc[$id];	
	}
	function getCURP($id,$type='sql')
	{
		return $this->m_curp[$id];
	}
	function getCP($id,$type='sql')
	{
		return $this->m_cp[$id];
	}
	function getNom_con($id)
	{
		return $this->m_nom_con[$id];	
	}
	function getEmail_con($id)
	{
		return $this->m_email_con[$id];	
	}
	function getTel_con($id)
	{
		return $this->m_tel_con[$id];	
	}
	function getMunicipio($id,$type='sql')
	{
		if($type == 'sql')
		{
			return $this->m_id_mun[$id];
		}
		elseif($type == 'interface')
		{
			return $this->m_mun[$id];
		}
	}
	function getEstado($id,$type='sql')
	{
		if($type == 'sql')
		{
			return $this->m_id_edo[$id];
		}
		elseif($type == 'interface')
		{
			return $this->m_edo[$id];
		}
	}
	function getNivel($id)
	{
		return $this->m_nivel[$id];	
	}
	function getFecha_reg($id,$type)
	{
		global $_Months;
	
		if($type == 'sql')
		{
			$fecha = $this->m_fecha[$id];
		}
		elseif($type == 'interface')
		{
			list($anio,$mes,$dia) = split("-",$this->m_fecha[$id]);
			$fecha = $dia." ".$_Months[$mes]." ".$anio;
		}
		return $fecha;
	}

	function getUltimoAcsesso($id,$type)
	{
		global $_Months;
	
		if($type == 'sql')
		{
			$fecha = $this->m_ultimoacceso[$id];
		}
		elseif($type == 'interface')
		{
			list($anio,$mes,$dia) = split("-",$this->m_ultimoacceso[$id]);
			$fecha = $dia." ".$_Months[$mes]." ".$anio;
		}
		return $fecha;
	}
	function getStatus($id)
	{
		return $this->m_status[$id];
	}	
	function getTotal()
	{
		return $this->m_total;
	}
	
	
	function Coleccion($id, $tampag = '1', $page = '1' , $by = 'id_user', $order = 'ASC', $nombre = '', $usuario = '', $nivel = '', $email = '')
	{
		$this->I_Init();
		
		$reg1 = ($page-1) * $tampag;
		
		$this->m_consultas->NuevaConsulta();
		
		
		$campos = array("users.*","muni.*","edos.*");
			
		$condiciones = array("status_users" => "1");
		
		$inner = array("users:muni"=>"users.id_municipio=muni.id_mun",
				":edos"=>"muni.id_edo=edos.id_edos"				
				);
		
		if(!empty($id))
		{
			if(is_numeric($id))
			{			
				$condiciones['users.id_user'] = $id;
			}
			else
			{
				$condiciones['users.user_name'] = $id;
			}
		}
		
		if(!empty($usuario))
		{
			$condiciones["user_name like '%$usuario%' and 1"] = "1";
		}
		if(!empty($email))
		{
			$condiciones["email like '%$email%' and 1"] = "1";
		}
		if(!empty($nivel))
		{
			$condiciones["nivel_users"] = $nivel;
		}
	
		if($tampag > 1)
		{
			$this->m_consultas->QueryINNERJOIN($inner,$campos, $condiciones, "","");
		
			$this->m_total = $this->m_consultas->NumeroRegistros();
		}
		
		$result = $this->m_consultas->QueryINNERJOIN($inner,$campos, $condiciones, "","$by $order LIMIT $reg1, $tampag");
		
		$i=0;
		
		if($result == CONSULTAS_SUCCESS)
		{
			while($registro = $this->m_consultas->Fetch())
			{
				$collection[$i] = $registro["id_user"];
		
				$this->m_id[$collection[$i]]		= $registro["id_user"];
				$this->m_username[$collection[$i]]	= $registro["user_name"];
				$this->m_email[$collection[$i]]		= $registro["email"]; 
				$this->m_nivel[$collection[$i]]		= $registro["nivel_users"];
				$this->m_nombre[$collection[$i]]	= $registro["nombre"];
				$this->m_ape_pat[$collection[$i]]	= $registro["ape_pat"];
				$this->m_ape_mat[$collection[$i]]	= $registro["ape_mat"];
				$this->m_rfc[$collection[$i]]		= $registro["rfc"];
				$this->m_curp[$collection[$i]]		= $registro["curp"];
				$this->m_termino[$collection[$i]]	= $registro["termino"];
				$this->m_status[$collection[$i]]	= $registro["status_users"];
				$this->m_fecha[$collection[$i]]		= $registro["fecha"];
				$this->m_cp[$collection[$i]]		= $registro["cp"];
				$this->m_domicilio[$collection[$i]]	= $registro["domicilio"];
				$this->m_mun[$collection[$i]]		= $registro["nom_mun"];
				$this->m_id_mun[$collection[$i]]	= $registro["id_mun"];
				$this->m_id_edo[$collection[$i]]	= $registro["id_edos"];
				$this->m_edo[$collection[$i]]		= $registro["nom_edo"];
				$this->m_nom_con[$collection[$i]]	= $registro["nombre_contacto"];
				$this->m_email_con[$collection[$i]]	= $registro["email_contacto"];
				$this->m_tel_con[$collection[$i]]	= $registro["tel_contacto"];
				$i++;
			}
		}
		else
		{
			$this->m_total = 0;			
			return US_ERROR;
		}
		return $collection;
	}
	
	function EliminarUsuario($id)
	{	
		$condiciones = array('id_user'	=>	$id);
		$nvoregistro = array('status_users' => "0");
	
		if($this->m_consultas->ActualizarRegistro("users", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
   			return US_ERROR;
		}
		return US_SUCCESS;
	}
	function GuardaUsuario($email,$nombre,$ape_pat,$ape_mat,$curp,$rfc,$domicilio,$cp,$mun,$termino,$nom_con,$email_con,$tel_con)
	{
		global $MyDebug;
		global $MyUploader;

		if(empty($email) || empty($nombre) || empty($ape_pat)  || empty($ape_mat) || empty($curp) || empty($rfc) || empty($domicilio) || empty($cp)  || empty($mun) || empty($termino) || empty($nom_con) || empty($email_con) || empty($tel_con))
		{
			return US_VACIO;
		}		
		
		if($this->ExistEmail($email) == US_SUCCESS)
		{
			$MyDebug->DebugMessage("USERS::GuardarUsuario: el correo [$email] ya existe.");
			return US_ERROR;
		}

		$nvoregistro = array( 	'email'			=>	$email,
					'nombre'		=>	UTFS($nombre),
					'ape_pat'		=>	UTFS($ape_pat),
					'ape_mat'		=>	UTFS($ape_mat),
					'curp'			=>	$curp,
					'rfc'			=>	$rfc,
					'domicilio'		=>	UTFS($domicilio),
					'cp'			=>	$cp,
					'id_municipio'		=>	$mun,
					'termino'		=>	$termino,
					'nombre_contacto'	=>	UTFS($nom_con), 
					'email_contacto'	=>	$email_con,
					'tel_contacto'		=>	$tel_con,
					'fecha'			=>	date('Y-m-d')
				);

		
		if($this->m_consultas->AgregarRegistro("users", $nvoregistro) != CONSULTAS_SUCCESS)
		{	
			return US_ERROR;
		}
		$id=$this->m_consultas->UltimoID(); 
		$_SESSION["id_notario"] = $id;

		return US_SUCCESS;
	}
	function HabilitarUsuario($id,$email)
	{	
		$condiciones = array('id_user'		=>	$id);
		$nvoregistro = array('status_users'	=>	'2');
	

		$alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdfghijklmnopqrstuvwxyz0123456789";
		$password = substr(md5(str_shuffle($alphanum)), 0, 10);
		$contrasena = md5($password);	

		$key	= substr(md5(str_shuffle($alphanum)), 0, 10);
		$llave	= md5($key);

		$nvoregistro["contrasena"]	= $contrasena;
		$nvoregistro["validador"]	= $llave;

		$nvoregistros["contrasena"]	= $password;
		$nvoregistros["validador"]	= $key;
		
		$this->m_plantilla->asigna_variables($nvoregistros);

		$ContenidoString = $this->m_plantilla->muestra("templates/altanotario.tpl");
		
		$headers = "From: TESORERIA MUNICIPAL NEZAHUALCOYOTL <info@tesoreriamunicipal.gob.mx>\r\n";
		$headers .= "Reply-To: \r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			
		Correo::Enviar('Habilitar Cuenta', $email,$ContenidoString, $headers);

		if($this->m_consultas->ActualizarRegistro("users", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
   			return US_ERROR;
		}
		return US_SUCCESS;
	}
	function ConfirmarUsuario($id,$validador)
	{	
		$condiciones = array('id_user'		=>	$id);
		$nvoregistro = array('status_users'	=>	'3','validador'	=>	'');
	
		if($this->m_consultas->ActualizarRegistro("users", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
   			return US_ERROR;
		}
		return US_SUCCESS;
	}	
	function ActualizaUsuario($id,$nombre,$ape_pat,$ape_mat,$cp,$col,$calle,$num,$mun,$email,$contrasena,$fecha_nac,$sexo,$civil,$ocupacion,$termino,$curp)
	{
		global $MyUploader;

		if(empty($nombre) || empty($ape_pat)  || empty($email) || empty($mun) || empty($sexo) || empty($ocupacion) || empty($col) || empty($curp))
		{
			return US_VACIO;
		}		

		$nvoregistro = array();

		$nvoregistro['email']		=	"'".$email."'"; 
		$nvoregistro['nombre']		=	"'".UTFS($nombre)."'";
		$nvoregistro['ape_pat']		=	"'".UTFS($ape_pat)."'";
		$nvoregistro['ape_mat']		=	"'".UTFS($ape_mat)."'";
		$nvoregistro['cp']		=	"'".$cp."'";
		$nvoregistro['colonia']		=	"'".UTFS($col)."'";
		$nvoregistro['calle']		=	"'".UTFS($calle)."'";
		$nvoregistro['num']		=	"'".$num."'";
		$nvoregistro['id_municipio']	=	"'".$mun."'";
		$nvoregistro['fecha_nac']	=	"'".$fecha_nac."'";
		$nvoregistro['sexo']		=	"'".$sexo."'";
		$nvoregistro['edo_civil']	=	"'".$civil."'";
		$nvoregistro['ocupacion']	=	"'".$ocupacion."'";
		$nvoregistro['curp']		=	"'".$curp."'";

		if(!empty($contrasena))
		{
			$nvoregistro['contrasena']	=	"'".md5($contrasena)."'";
		}
	
		$condiciones = array('id_user'	=>	$id);

		if($this->m_consultas->ActualizarRegistro("users", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
			return US_ERROR;
		}		
		return US_SUCCESS;
	}
	function ExistEmail($mail)
	{
		$campos = array("email, id_user");

		$condiciones = array('email' => $mail, 'status_users' => "1");
		
		if($this->m_consultas->SeleccionarTablaFila("users", $campos, $condiciones, "", "id_user DESC") == CONSULTAS_SUCCESS)
		{
			return US_SUCCESS;	
		}
		return US_ERROR;
	}
}

$MyUSERS = new USUARIOS;
?>
