<?php
include_once('class.plantilla.php');
include_once('class.abstracto.php');

$ErrUser = 200;
define("USERS_SUCCESS", 	$ErrUser++);
define("USERS_ERROR", 		$ErrUser++);
define("USERS_VACIO", 		$ErrUser++);


class USERS extends Abstracto
{
	protected $total;
	protected $id;
	protected $plantilla;
	protected $username;
	protected $nombre;
	protected $ape_pat;
	protected $ape_mat;
	protected $email;
	protected $nivel;
	protected $fecha;
	protected $cp;
	protected $estado;
	protected $municipio;
	protected $asentamiento;
	protected $calle;
	protected $numero;
	protected $ultimoacceso;


	function USERS()
	{
		$this->I_Init();
		$this->total		= "0";
		$this->plantilla	= new Plantilla();
	}

	function getNivel($id, $type)
	{
		if($type == 'sql')
		{
			return $this->nivel[$id];
		}
		elseif($type == 'interface')
		{
			switch($this->nivel[$id])
			{
			case NIVEL_USERPREDIO:
				$nivel = "LIQUIDADOR PREDIO";
				break;
			case NIVEL_USERDIVERSOS:
				$nivel = "LIQUIDADOR DIVERSOS";
				break;
			case NIVEL_USEROPERADOR:
				$nivel = "OPERADOR";
				break;
			case NIVEL_OPERADOR:
				$nivel = "JEFES";
				break;
			case NIVEL_USERADMIN:
				$nivel = "ADMINISTRADOR";
				break;
			case NIVEL_USERSUPERADMIN:
				$nivel = "S ADMINISTRADOR";
				break;
			default:
				break;
			}
			return $nivel;
		}
	}

	function getFecha($id,$type)
	{
		global $_Months;

		if($type == 'sql')
		{
			$fecha = $this->m_fecha[$id];
		}
		elseif($type == 'interface')
		{
			list($anio,$mes,$dia) = explode("-",$this->fecha[$id]);
			$fecha = $dia." ".$_Months[$mes]." ".$anio;
		}
		return $fecha;
	}

	function getUltimoAcsesso($id,$type)
	{
		global $_Months;

		if($type == 'sql')
		{
			$fecha = $this->m_ultimoacceso[$id];
		}
		elseif($type == 'interface')
		{
			list($anio,$mes,$dia) = split("-",$this->ultimoacceso[$id]);
			$fecha = $dia." ".$_Months[$mes]." ".$anio;
		}
		return $fecha;
	}

	function Coleccion($id, $tampag = '1', $page = '1' , $by = 'usuario', $order = 'ASC', $nombre = '', $usuario = '', $nivel = '', $email = '')
	{
		global $MySPANEL;
		$this->I_Init();

		$reg1 = ($page-1) * $tampag;

		$this->Consultas()->NuevaConsulta();


		$campos = array("usuarios.*");
		$condiciones = array("status" => "1");

		if(!empty($id))
		{
			if(is_numeric($id))
			{
				$condiciones['usuarios.id'] = $id;
			}
			else
			{
				$condiciones['usuarios.usuario'] = $id;
			}
		}

		if(!empty($usuario))
		{
			$condiciones["usuario like '%$usuario%' and 1"] = "1";
		}
		if(!empty($email))
		{
			$condiciones["mail like '%$email%' and 1"] = "1";
		}
		if(!empty($nivel))
		{
			$condiciones["nivel"] = $nivel;
		}

		$result = $this->Consultas()->SeleccionarTablaFila("usuarios",$campos, $condiciones, "","$by $order LIMIT $reg1, $tampag");

		if($tampag > 1)
		{
			$this->total = $this->Consultas()->NumeroRegistros();
		}

		$i=0;

		if($result == CONSULTAS_SUCCESS)
		{
			while($registro = $this->Consultas()->Fetch())
			{
				$collection[$i] = $registro["id"];

				$this->id[$collection[$i]]		= $registro["id"];
				$this->username[$collection[$i]]	= $registro["usuario"];
				$this->nombre[$collection[$i]]		= $registro["nombre"];
				$this->ape_pat[$collection[$i]]		= $registro["apellido_pat"];
				$this->ape_mat[$collection[$i]]		= $registro["apellido_mat"];
				$this->email[$collection[$i]]		= $registro["mail"];
				$this->nivel[$collection[$i]]		= $registro["nivel"];
				$this->cp[$collection[$i]]		= $registro["cp"];
				$this->estado[$collection[$i]]		= $registro["estado"];
				$this->municipio[$collection[$i]]	= $registro["municipio"];
				$this->asentamiento[$collection[$i]]	= $registro["asentamiento"];
				$this->calle[$collection[$i]]		= $registro["calle"];
				$this->numero[$collection[$i]]		= $registro["numero"];
				$i++;
			}
		}
		else
		{
			$this->total = 0;
			return USERS_ERROR;
		}
		return $collection;
	}

	function EliminarUsuario()
	{
		$this->Consultas()->Autocommit(FALSE);
		$condiciones = array('id'	=> $this->id);
		$nvoregistro = array('status'	=> "0");

		if($this->Consultas()->ActualizarRegistro("usuarios", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
			$this->Consultas()->Rollback();
   			return USERS_ERROR;
		}
		$MyLogDB = new LOG_DB;
		$MyLogDB->AddEntrada("usuarios",$this->id, "Eliminar",$nvoregistros);
		$this->Consultas()->Commit();
		return USERS_SUCCESS;
	}

	function GuardaUsuario()
	{
		global $MyDebug;
		global $MyUploader;

		if(empty($this->username) || empty($this->email)  || empty($this->nivel) || empty($this->nombre) || empty($this->ape_pat) || empty($this->ape_mat) || empty($this->cp) || empty($this->estado) || empty($this->municipio) || empty($this->asentamiento) || empty($this->calle) || empty($this->numero))
		{
			return USERS_VACIO;
		}
		$this->Consultas()->Autocommit(FALSE);
		if($this->ExistUser() == USERS_SUCCESS)
		{
			$MyDebug->DebugMessage("USERS::GuardarUsuario: el usuario [{$this->username}] ya existe.");
			return USERS_ERROR;
		}

		$alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdfghijklmnopqrstuvwxyz0123456789";
		$password = substr(md5(str_shuffle($alphanum)), 0, 10);
		$contrasena = sha1($password);

		$nvoregistro = array( 	'usuario'	=>	$this->username,
					'passwd'	=>	'',
					'mail'		=>	$this->email,
					'nivel'		=>	$this->nivel,
					'nombre'	=>	$this->nombre,
					'apellido_pat'	=>	$this->ape_pat,
					'apellido_mat'	=>	$this->ape_mat,
					'cp'		=>	$this->cp,
					'estado'	=>	$this->estado,
					'municipio'	=>	$this->municipio,
					'asentamiento'	=>	$this->asentamiento,
					'calle'		=>	$this->calle,
					'numero'	=>	$this->numero,
				);


		if($this->Consultas()->AgregarRegistro("usuarios", $nvoregistro) != CONSULTAS_SUCCESS)
		{
			$this->Consultas()->Rollback();
			return USERS_ERROR;
		}
//		$nvoregistros["contrasena"] = $password;
//		$nvoregistros["usuario"] = $usuario;
//
//
//		$this->m_plantilla->asigna_variables($nvoregistros);
//
//		$ContenidoString = $this->m_plantilla->muestra("../templates/mailnuevousuario.tpl");
//
//		$headers = "From: Sistema <info@localhost>\r\n";
//		$headers .= "Reply-To: \r\n";
//		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
//
//		Correo::Enviar('Alta de usuario', $this->email,$ContenidoString, $headers);

		$this->id=$this->Consultas()->UltimoID();

		$MyLogDB = new LOG_DB;
		$MyLogDB->AddEntrada("usuarios",$this->id, "Nuevo",$nvoregistro);

		$nvoregistros['passwd']		=	"'".sha1($id.$contrasena)."'";
		$condiciones = array('id'	=>	$this->id);
		if($this->Consultas()->ActualizarRegistro("usuarios", $nvoregistros, $condiciones) != CONSULTAS_SUCCESS)
		{
			$this->Consultas()->Rollback();
			return USERS_ERROR;
		}

		$MyLogDB = new LOG_DB;
		$MyLogDB->AddEntrada("usuarios",$this->id, "Editar",$nvoregistros);
		$this->Consultas()->Commit();
		return USERS_SUCCESS;
	}

	function ActualizaUsuario($contrasena)
	{
		global $MyUploader;

		if(empty($this->id) || empty($this->email)  || empty($this->nivel) || empty($this->nombre) || empty($this->ape_pat) || empty($this->ape_mat) || empty($this->cp) || empty($this->estado) || empty($this->municipio) || empty($this->asentamiento) || empty($this->calle) || empty($this->numero))
		{
			return USERS_VACIO;
		}

		$nvoregistro = array( 	'mail'		=>	"'".$this->email."'",
					'nivel'		=>	"'".$this->nivel."'",
					'nombre'	=>	"'".$this->nombre."'",
					'apellido_pat'	=>	"'".$this->ape_pat."'",
					'apellido_mat'	=>	"'".$this->ape_mat."'",
					'cp'		=>	"'".$this->cp."'",
					'estado'	=>	"'".$this->estado."'",
					'municipio'	=>	"'".$this->municipio."'",
					'asentamiento'	=>	"'".$this->asentamiento."'",
					'calle'		=>	"'".$this->calle."'",
					'numero'	=>	"'".$this->numero."'",
				);

		if(!empty($contrasena))
		{
			$nvoregistro['passwd']	=	"'".sha1($this->id.$contrasena)."'";

			$nvoregistros["contrasena"] = $contrasena;
//			$this->m_plantilla->asigna_variables($nvoregistros);
//
//			$ContenidoString = $this->m_plantilla->muestra("../templates/maileditarusuario.tpl");
//
//			$headers = "From: TERORERIA ODAPASL <sistemas@tesoreriamunicipal.gob.mx>\r\n";
//			$headers .= "Reply-To: \r\n";
//			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
//
//			Correo::Enviar('Alta de usuario', $email,$ContenidoString, $headers);
		}

		$condiciones = array('id'	=>	$this->id);

		if($this->Consultas()->ActualizarRegistro("usuarios", $nvoregistro, $condiciones) != CONSULTAS_SUCCESS)
		{
			return USERS_ERROR;
		}
		$MyLogDB = new LOG_DB;
		$MyLogDB->AddEntrada("usuarios",$this->id, "Editar",$nvoregistro);

		return USERS_SUCCESS;
	}

	function ExistUser()
	{
		$campos = array("usuario");

		$condiciones = array('usuario' => $this->username, 'status' => "1");

		if($this->Consultas()->SeleccionarTablaFila("usuarios", $campos, $condiciones, "", "id DESC") == CONSULTAS_SUCCESS)
		{
			return USERS_SUCCESS;
		}
		return USERS_ERROR;

	}
}

$MyUser = new USERS;
?>
