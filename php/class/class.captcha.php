<?PHP
session_start();

class Captcha {

    var $m_image_info;

	function Captcha()
	{
		$this->I_Init();	
	}   
	
	function I_Init()
	{
	
		$this->m_image_info = null;
	}
   
    function random_key($length)
	{
    
	   $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    
	   $key = substr(str_shuffle($chars), 0, $length);	
    
	   return $key;
	}

    function random_background($images) 
	{
        if ($images) 
		{
            $select_image = $images[rand(0, count($images) - 1)];
        }
        return $select_image;
    }

    function random_color($colors) 
	{
        if ($colors) 
		{
            $select_color = $colors[rand(0, count($colors) - 1)];
        }
        return $select_color;
    }

    function random_font($fonts) 
	{
        if ($fonts) 
		{
            $select_font = $fonts[rand(0, count($fonts) - 1)];
        }
        return $select_font;
    }

    function random_fontsizes($fontsizes)
	 {
        if ($fontsizes) 
		{
            $select_fontsize = $fontsizes[rand(0, count($fontsizes) - 1)];
        }
        return $select_fontsize;
    }

    function captcha_set($images, $length, $colors, $fonts, $fontsizes, $width, $height, $space) {
        
        $key = $this -> random_key($length);
        
		//unset($_SESSION['captcha']);
        
		$_SESSION['captcha'] = md5($key);

        $select_image = $this -> random_background($images);
		
        $this -> image_info = getImageSize($select_image); //array = ["width", "heigth", "mime"]

        if ($this -> image_info['mime'] == 'image/jpeg' || $this -> image_info['mime'] == 'image/jpg') 
		{
            $old_image = imageCreateFromJPEG($select_image);
        }
		elseif ($this -> image_info['mime'] == 'image/gif') 
		{
            $old_image = imageCreateFromGIF($select_image);
        }
		elseif ($this -> image_info['mime'] == 'image/png') 
		{
            $old_image = imageCreateFromPNG($select_image);
        }

        if (empty($width)) 
		{ 
			$width = $this -> image_info[0]; 
		}
        if (empty($height)) 
		{ 
			$height = $this -> image_info[0]; 
		}

        $new_image = imageCreateTrueColor($width, $height); //devuelve un identificador de imagen representando una imagen en blanco de tama�o anchura por altura . 

        $bg = imagecolorallocate($new_image, 255, 255, 255); //Color de fondo de la imagen
        
		imagefill($new_image, 0, 0, $bg);//realiza un relleno empezando en la coordenada x ,y (arriba izquierda es 0,0) con el color col en la imagen

        imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $width, $height, $this -> image_info[0], $this -> image_info[1]);

        $bg = imagecolorallocate($new_image, 255, 255, 255);
		
        for ($i = 0; $i < strlen($key); $i++) 
		{
            $color_cols = explode(',', $this -> random_color($colors));
            $fg = imagecolorallocate($new_image, trim($color_cols[0]), trim($color_cols[1]), trim($color_cols[2]));
            imagettftext($new_image, $this -> random_fontsizes($fontsizes), rand(-10, 15), 10 + ($i * $space), rand($height - 10, $height - 5), $fg, $this -> random_font($fonts), $key[$i]);
        }

        if ($this -> image_info['mime'] == 'image/jpeg') 
		{
            header("Content-type: image/jpeg");
            header('Content-Disposition: inline; filename=captcha.jpg');
            imageJPEG($new_image);
        }
		elseif ($this -> image_info['mime'] == 'image/gif') 
		{
            header("Content-type: image/gif");
            header('Content-Disposition: inline; filename=captcha.gif');
            imageGIF($new_image);
        }
		elseif ($this -> image_info['mime'] == 'image/png') 
		{
            header("Content-type: image/png");
            header('Content-Disposition: inline; filename=captcha.png');
            imagePNG($new_image);
        }

        imageDestroy($old_image);
        imageDestroy($new_image);
    }

}


$MyCaptcha = new Captcha();

$images = array('../images/captchas/background1.jpg');
$fonts = array('../fonts/ariali_0.ttf','../fonts/casual.ttf');
$fontsizes = array(22, 24, 26);
$colors = array('128,23,23', '128,23,22', '33,67,87', '67,70,83', '31,56,163', '48,109,22', '165,42,166', '18,95,98', '213,99,8'); // RGB colors

$MyCaptcha->captcha_set($images, 5, $colors, $fonts, $fontsizes, 143, 47, 23);
/*
captcha_set function arguments
1 - background images
2 - rasgele charachter lengh
3 - font colors
4 - fonts
5 - font sizes
6 - width
7 - height
8 - charachters space
*/
?>
