<?php

	define("NIVEL_USERPUBLICO",		0);
	define("NIVEL_USERADMIN",		1);
	define("NIVEL_USERSUPERADMIN",		2);
	define("ACCESS_DENIED",			400);


	$_NIVELUSER = array
	(	'ADMINISTRADOR'		=>	NIVEL_USERADMIN,
		'SUPER ADMINISTRADOR'	=>	NIVEL_USERSUPERADMIN
	);
	$_NIVELUSERS = array
	(	NIVEL_USERADMIN		=>	'ADMINISTRADOR',
		NIVEL_USERSUPERADMIN	=>	'SUPER ADMINISTRADOR'
	);
	class MYSESSIONPANEL
	{
		var $m_username;
		var $m_hash;
		var $m_nivel;
		var $m_email;
		var $m_id;

		function MYSESSIONPANEL()
		{
			$this->I_Init();
		}

		function I_Init()
		{
			global $MyDebug;

			$this->m_username = "";
			$this->m_email = "";
			$this->m_nivel = 0;
			$this->m_id = "";



			if ( 	isset($_SESSION['my_oda_user'])   && ($_SESSION['my_oda_user'] != "") &&
			     	isset($_SESSION['my_oda_nivel']) && ($_SESSION['my_oda_nivel'] != "") &&
				isset($_SESSION['my_oda_email']) && ($_SESSION['my_oda_email'] != "") &&
				isset($_SESSION['my_oda_id']) && ($_SESSION['my_oda_id'] != ""))
			{
				$this->m_username = $_SESSION['my_oda_user'];
				$this->m_nivel = $_SESSION['my_oda_nivel'];
				$this->m_email = $_SESSION['my_oda_email'];
				$this->m_id = $_SESSION['my_oda_id'];
				$MyDebug->DebugMessage("SESSION::Login:[".$this->m_username."][".$this->m_nivel."][".$this->m_email."][".$this->m_id."]");
			}
		}

		function LoggedIn()
		{
			return ($this->m_username != "");
		}

		function USUARIO()
		{
			return $this->m_username;
		}

		function NIVEL()
		{
			return $this->m_nivel;
		}

		function EMAIL()
		{
			return $this->m_email;
		}

		function ID()
		{
			return $this->m_id;
		}

		function SetVar( $varname, $value )
		{
			$_SESSION[$varname] = $value;
		}

		function GetVar( $varname )
		{
			return ( isset($_SESSION[$varname]) ? $_SESSION[$varname] : "" );
		}

		function AccesoPermitido($operacion,$operacion_a, $duenio)
		{
			global $MyDebug;

			if ($this->m_username == "" || $this->m_email == "" || $this->m_id == "" || $this->m_nivel <= 0)
			{
				return ACCESS_DENIED;
			}

			if(is_numeric($duenio))
			{
				if ( ($this->m_id != $duenio) && $this->m_nivel != NIVEL_USERADMIN )
				{
					return ACCESS_DENIED;
				}
			}
			else
			{
				if ( ($this->m_username != $duenio) && $this->m_nivel != NIVEL_USERADMIN )
				{
					return ACCESS_DENIED;
				}
			}
			if(!in_array($this->m_nivel,$operacion))
			{
				$MyDebug->DebugMessage("SESSION::AccesoPermitido:[".$this->m_nivel."][".implode(",",$operacion)."][No Tiene permisos Nivel]");
				return ACCESS_DENIED;
			}
			if($this->m_nivel < NIVEL_USERSUPERADMIN)
			{
				if(!in_array($this->m_area,$operacion_a))
				{
					$MyDebug->DebugMessage("SESSION::AccesoPermitido:[".$this->m_area."][".implode(",",$operacion_a)."][No Tiene permisos area]");
					return ACCESS_DENIED;
				}
			}
			return 1;
		}

		function EndSessionP()
		{
			$_SESSION['my_oda_user'] = "";
			$_SESSION['my_oda_email'] = "";
			$_SESSION['my_oda_id'] = "";
			$_SESSION['my_oda_nivel'] = 0;
			session_destroy();
			session_start();
			$this->I_Init();
		}

	}
session_name('session_panel');
if(!isset($_SESSION)) session_start();
$MySPANEL = new MYSESSIONPANEL;
?>
