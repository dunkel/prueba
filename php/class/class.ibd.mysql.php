<?php
abstract class IBD
{
	var $m_dbResultados;
	var $m_link;
	var $m_tipo_fetch;
	var $m_conectarbd;
	
	function Limpiar()
	{
		$m_link=0;
		$m_tipo_fetch='';
		$m_conectarbd='';
		$m_dbResultados=array();
	}
		
	function ConectarBD()
	{
		global $MyDebug;
		$port='';
		if(!empty(_BD_PORT_)){
			$port=":"._BD_PORT_."";
		}
		$link= mysql_connect(_BD_SERVER_."$port", _BD_USER_, _BD_PASSWD_);
		
		if (!$link)
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede hacer la conexion.");
			return IBD_ERR_CANTCONNECT;
		}		
		
		if(!mysql_select_db(_BD_NAME_, $link))
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede Seleccionar la base de datos.");		
			return IBD_ERR_CANTSELECT;
			mysql_close($link);
		}
		$this->m_link=$link;
		return IBD_SUCCESS;
	}
	
	function Query($origen, $consulta)
	{
		global $MyDebug;
		
		if(($result=$this->ConectarBD())!=IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("IBD::Query(): Fallo en la conexion.");
			return $result;			
		}	
		
		$result= mysql_query($consulta, $this->m_link);
		
		if(!$result)
		{
			$MyDebug->DebugMessage("IBD::Query(): Error en la consulta");
			return IBD_ERR_DBUNAVAILABLE;
		}
		
		$this->m_dbResultados[$origen]=$result;
		return IBD_SUCCESS;
	}
		
	function FetchIbd($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			if($this->m_tipo_fetch==FETCH_OBJ){
				return mysql_fetch_object($dbResultados);
			}else{
				return mysql_fetch_array($dbResultados);
			}
		}
		
		return 0;
	}		
	
	function  Liberar($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			mysql_free_result($dbResultados);
			$this->m_dbResultados[$origen]=0;
		}
		
		return IBD_SUCCESS;
	}
	
	function NumeroRows($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			return mysql_num_rows($dbResultados);
		}
		
		return 0;
	}
	
	function UltimoID()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysql_insert_id($this->m_link);	
	}
	public function Autocommit($valor='TRUE')
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysql_query("BEGIN");
	}
	public function Commit()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysql_query("COMMIT");
	}
	public function Rollback()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysql_query("ROLLBACK");
	}		
}
?>
