<?php
	include_once("class.plantilla.php");
	$_iLOGINError = 200;
	
	define("LOGIN_SUCCESS", 	$_iLOGINError++);
	define("LOGIN_BADLOGIN",  	$_iLOGINError++);
	define("LOGIN_DBFAILURE", 	$_iLOGINError++);
	
	class LOGINPANEL
	{
		var $m_ibd;
		var $m_user;
		var $m_nivel;
		var $m_email;
		var $m_id;
		var $m_area;
		var $m_plantilla;

		function LOGINPANEL()
		{
			$this->Iniciar();
		}
		
		function Iniciar()
		{
			$this->m_ibd = new CONSULTAS;
			$this->m_plantilla = new Plantilla;
		}
		
		function USUARIO()
		{
			return $this->m_user;
		}

		function NIVEL()
		{
			return $this->m_nivel;
		}
	
		function EMAIL()
		{
			return $this->m_email;
		}
		
		function ID()
		{
			return $this->m_id;
		}

		function LOGINE($usuario, $hash)
		{
			$consulta  = "SELECT id, usuario, passwd, nivel, mail ";
			$consulta .= "FROM usuarios WHERE usuario='$usuario' AND passwd=SHA1(CONCAT(id,'$hash')) AND status > 0";
			//echo $consulta;
			if (($result = $this->m_ibd->Query("Login", $consulta))!= IBD_SUCCESS)
			{
				return $result;
			}
			
			if (($result = $this->m_ibd->NumeroRows("Login")) < 1 )
			{
				$this->m_ibd->Liberar("Login");
				return LOGIN_BADLOGIN; 
			}
			
			$registro = $this->m_ibd->FetchIbd("Login");
			
			if ( ! $registro )
			{
				$result = LOGIN_DBFAILURE;
			}
			else
			{
				$this->m_user 	= $registro['usuario'];
				$this->m_nivel 	= $registro['nivel'];
				$this->m_email 	= $registro['mail'];
				$this->m_id 	= $registro['id'];	
				
				$sql = "UPDATE usuarios set ultimoacceso='".date('Y-m-d')."' where ID='".$this->m_id."' and status > 0";
				$this->m_ibd->Query("usuarios", $sql);
							
				$result = LOGIN_SUCCESS;
			}
			
			$this->m_ibd->Liberar("Login");
			return $result;
		}	
		
		function FORGOT($usuario, $email)
		{
			$consulta  = "SELECT id ";
			$consulta .= "FROM usuarios WHERE usuario='$usuario' AND mail='$email' AND status > 0 ";
			
			if (($result = $this->m_ibd->Query("forgot", $consulta))!= IBD_SUCCESS)
			{
				return $result;
			}
			
			if (($result = $this->m_ibd->NumeroRows("forgot")) < 1 )
			{
				$this->m_ibd->Liberar("forgot");
				return LOGIN_BADLOGIN; 
			}
			
			$registro = $this->m_ibd->FetchIbd("forgot");
			
			if ( ! $registro )
			{
				$result = LOGIN_DBFAILURE;
			}
			else
			{
				$password	=	substr(md5(uniqid()),0,10);
				
				$sql = "UPDATE usuarios set passwd='".md5($password)."' where id='".$registro['id']."'";
				$this->m_ibd->Query("updatepass", $sql);
						
				$this->m_plantilla->asigna_variables(array("usuario" => $usuario, "password" => $password));
				$ContenidoString = $this->m_plantilla->muestra("/templates/mailolvidopass.tpl");
				
				$headers = "From: Prueba <>\r\n";
				$headers .= "Reply-To: \r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";			

				Correo::Enviar("Recordatorio de contraseņa", $email, $ContenidoString,$headers);
											
				$result = LOGIN_SUCCESS;
			}
			
			$this->m_ibd->Liberar("forgot");
			return $result;
		}	
		
	}
?>
