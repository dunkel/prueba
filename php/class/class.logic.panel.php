<?php
include_once('class.session.panel.php');

define("MSG_ALERT",	"HTML");

/**/
$_command=1001;
define("HOME",			"");
define("LOGIN",			$_command++);

define("LISTA_MIEMBROS",	$_command++);//lista miembreso
define("FORM_OPERADOR",		$_command++);
define("FORM_OPERADOR_EDIT",	$_command++);
define("DELETE_OPERADOR",	$_command++);
define("NIVEL_OPER",		$_command++);

define("PATH_AJAX",			"ajax/");
define("PATH_JS",			"js/");
define("PATH_CSS",			"css/");
$uiCommand=array();

$uiCommand[HOME]	=	array(
array(NIVEL_USERADMIN, NIVEL_USERSUPERADMIN),				//PERMISO ACCESO
array(0,''),					//PERMISO ACCESO AREAS
"HOME",						//My Title
"ajax.common.php",				//My AJAX File
"php/home.php",					//My PHP File
""	,					//My JS File
""						//My css File
);

$uiCommand[LOGIN]	=	array(
array(NIVEL_USERPUBLICO),			//PERMISO ACCESO
array(NIVEL_USERPUBLICO),			//PERMISO ACCESO AREAS
"",						//My Title
"ajax.common.php",				//My AJAX File
"php/frm.login.php",				//My PHP File
""	,					//My JS File
"login.css" 					//My css File
);

$uiCommand[LISTA_MIEMBROS]	=	array(
array(NIVEL_USERADMIN, NIVEL_USERSUPERADMIN),			//PERMISO ACCESO
array(NIVEL_USERPUBLICO),				//PERMISO ACCESO AREAS
"Lista de Usuarios",				//My Title
"ajax.usuarios.php",				//My AJAX File
"php/users/lista.php",				//My PHP File
"usuarios.js",					//My JS File
"" //My css File
);
$uiCommand[FORM_OPERADOR]	=	array(
array(NIVEL_USERSUPERADMIN),			//PERMISO ACCESO
array(NIVEL_USERPUBLICO),				//PERMISO ACCESO AREAS
"Nuevo Usuario",				//My Title
"ajax.usuarios.php",				//My AJAX File
"php/users/form.users.php",				//My PHP File
"usuarios.js",					//My JS File
"" 						//My css File
);

$uiCommand[FORM_OPERADOR_EDIT]	=	array(
array(NIVEL_USERADMIN, NIVEL_USERSUPERADMIN),			//PERMISO ACCESO
array(NIVEL_USERPUBLICO),				//PERMISO ACCESO AREAS
"Editar Usuario",				//My Title
"ajax.usuarios.php",				//My AJAX File
"php/users/form.users.php",				//My PHP File
"edit.usuarios.js",				//My JS File
"" //My css File
);
$uiCommand[DELETE_OPERADOR]	=	array(array(NIVEL_USERADMIN, NIVEL_USERSUPERADMIN),array(NIVEL_USERPUBLICO));
$uiCommand[NIVEL_OPER]		=	array(array(NIVEL_USERADMIN, NIVEL_USERSUPERADMIN),array(NIVEL_USERPUBLICO));

class INDEX
{
	var $m_ajax;
	var $m_js;
	var $m_content;
	var $m_title;
	var $m_css;
	var $m_command;


	function INDEX()
	{
		$this->m_ajax		= "";
		$this->m_js		= "";
		$this->m_css		= "";
		$this->m_content	= "";
		$this->m_title		= "";
		$this->m_command	= "";
	}


	function Title($command)
	{
		return $this->m_title;
	}

	function MyAjaxFile()
	{
		return $this->m_ajax;
	}


	function MyPHPFile()
	{
		return $this->m_content;
	}

	function MyJSFile()
	{
		return $this->m_js;
	}

	function MyCSSFile()
	{
		return $this->m_css;
	}

	function MyCommand()
	{
		return $this->m_command;
	}

	function base_url()
	{
		// first get http protocol if http or https
		$base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off') ? 'https://' : 'http://';
		// get default website root directory
		$tmpURL = dirname(__FILE__);
		// when use dirname(__FILE__) will return value like this "C:\xampp\htdocs\my_website",
		//convert value to http url use string replace,
		// replace any backslashes to slash in this case use chr value "92"
		$tmpURL = str_replace(chr(92),'/',$tmpURL);
		// now replace any same string in $tmpURL value to null or ''
		// and will return value like /localhost/my_website/ or just /my_website/
		$tmpURL = str_replace($_SERVER['DOCUMENT_ROOT'],'',$tmpURL);
		// delete any slash character in first and last of value
		$tmpURL = ltrim($tmpURL,'/');
		$tmpURL = rtrim($tmpURL, '/');
		// check again if we find any slash string in value then we can assume its local machine
		if (strpos($tmpURL,'/'))
		{
			// explode that value and take only first value
			$tmpURL = explode('/',$tmpURL);
			$tmpURL = $tmpURL[0];
		}
		// now last steps
		// assign protocol in first value
		if ($tmpURL !== $_SERVER['HTTP_HOST'])
		{
			// if protocol its http then like this
			$base_url .= $_SERVER['HTTP_HOST'].'/'.$tmpURL.'/';
		}else
		{
			// else if protocol is https
			$base_url .= $tmpURL.'/';
		}
		// give return value
		return $base_url;
	}
	function MyDMenu()
	{
		$MyMenu=array(
			LISTA_MIEMBROS			=> array(LISTA_MIEMBROS,FORM_OPERADOR,FORM_OPERADOR_EDIT),
			);
		return $MyMenu;
	}
	function MyMenu()
	{
		$MyMenu = array("Sistemas:fa-laptop "=>array(
				"Usuarios:fa-user"						=> LISTA_MIEMBROS,
				
			)
		);

		ksort($MyMenu);
		return (object)$MyMenu;
	}
	function Logic($command)
	{
		global $uiCommand;
		global $MySPANEL;
		global $MyLog;


		if(!isset($uiCommand[$command]))
		{

			$command=HOME;

			if(isset($_SESSION["cookie_http_vars"]) && !empty($_SESSION["cookie_http_vars"]))
			{
				$_SESSION["cookie_http_vars"]=array();

			}
		}

		if(!in_array($MySPANEL->NIVEL(),$uiCommand[$command][0]))
		{

			$command=LOGIN;

			if(isset($_SESSION["cookie_http_vars"]) && !empty($_SESSION["cookie_http_vars"]))
			{
				$_SESSION["cookie_http_vars"]=array();
			}
		}



		$this->m_title		= $uiCommand[$command]['2'];
		$this->m_ajax 		= PATH_AJAX.$uiCommand[$command]['3'];
		$this->m_content	= $uiCommand[$command]['4'];
		$this->m_js 		= $uiCommand[$command]['5'];
		$this->m_css 		= $uiCommand[$command]['6'];
		$this->m_command 	= $command;
	}
}

$MyLogic	= new INDEX;

?>
