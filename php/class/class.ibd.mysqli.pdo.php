<?php
abstract class IBD 
{
	var $m_dbResultados;
	var $m_link;
	var $m_tipo_fetch;
	var $m_conectarbd;
		
	function Limpiar()
	{
		$m_link=0;
		$m_tipo_fetch='';
		$m_conectarbd='';
		$m_dbResultados=array();
	}
		
	function ConectarBD()
	{
		global $MyDebug;
		$link = new mysqli(_BD_SERVER_, _BD_USER_, _BD_PASSWD_, _BD_NAME_,_BD_PORT_); 
		
		if ($link->connect_error)
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede hacer la conexion.".$link->connect_error);
			return IBD_ERR_CANTCONNECT;
		}		
		
		if(!$link->select_db(_BD_NAME_))
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede Seleccionar la base de datos.");		
			return IBD_ERR_CANTSELECT;
			$link->close();
		}
		$this->m_link=$link;
		return IBD_SUCCESS;
	}
	
	function Query($origen, $consulta)
	{
		global $MyDebug;

		if ($this->m_conectarbd!=IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("IBD::Query(): Fallo en la conexion.");
			return $this->m_conectarbd;			
		}	
		
		$result = $this->m_link->query($consulta);
		
		if(!$result)
		{
			$MyDebug->DebugMessage("IBD::Query(): Error en la consulta");
			return IBD_ERR_DBUNAVAILABLE;
		}
		
		$this->m_dbResultados[$origen]=$result;
		return IBD_SUCCESS;
	}
		
	function FetchIbd($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			if($this->m_tipo_fetch==FETCH_OBJ){
				return $dbResultados->fetch_object();
			}else{
				return $dbResultados->fetch_array();
			}
		}
		
		return 0;
	}		
	
	function  Liberar($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			$dbResultados->close();
			$this->m_dbResultados[$origen]=0;
		}
		
		return IBD_SUCCESS;
	}
	
	function NumeroRows($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];
		
		if($dbResultados)
		{
			return $dbResultados->num_rows;
		}
		
		return 0;
	}
	
	function UltimoID()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->insert_id;	
	}
	public function Autocommit($valor='FALSE')
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return $this->m_link->autocommit($valor);
	}
	public function Commit()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return  $this->m_link->commit();
	}
	public function Rollback()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return  $this->m_link->rollback();
	}	
}
?>
