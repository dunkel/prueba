<?php
abstract class IBD
{
	var $m_dbResultados;
	var $m_link;
	var $m_tipo_fetch;
	var $m_conectarbd;

	function Limpiar()
	{
		$m_link=0;
		$m_tipo_fetch='';
		$m_conectarbd='';
		$m_dbResultados=array();
	}

	function ConectarBD()
	{
		global $MyDebug;

		$link= mysqli_connect(_BD_SERVER_, _BD_USER_, _BD_PASSWD_, _BD_NAME_, _BD_PORT_);

		if (!$link)
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede hacer la conexion.");
			return IBD_ERR_CANTCONNECT;
		}

		if(!mysqli_select_db($link, _BD_NAME_))
		{
			$MyDebug->DebugMessage("IBD::ConectarBD(): No se puede Seleccionar la base de datos.");
			return IBD_ERR_CANTSELECT;
			mysql_close($link);
		}
		$this->m_link=$link;
		return IBD_SUCCESS;
	}

	function Query($origen, $consulta)
	{
		global $MyDebug;

		if ($this->m_conectarbd!=IBD_SUCCESS)
		{
			$MyDebug->DebugMessage("IBD::Query(): Fallo en la conexion.");
			return $this->m_conectarbd;
		}

		$result= mysqli_query($this->m_link, $consulta);

		if(!$result)
		{
			$MyDebug->DebugMessage("IBD::Query(): Error en la consulta");
			return IBD_ERR_DBUNAVAILABLE;
		}

		$this->m_dbResultados[$origen]=$result;
		return IBD_SUCCESS;
	}

	function FetchIbd($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];

		if($dbResultados)
		{
			if($this->m_tipo_fetch==FETCH_OBJ){
				return mysqli_fetch_object($dbResultados);
			}else{
				return mysqli_fetch_array($dbResultados);
			}
		}

		return 0;
	}

	function  Liberar($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];

		if($dbResultados)
		{
			mysqli_free_result($dbResultados);
			$this->m_dbResultados[$origen]=0;
		}

		return IBD_SUCCESS;
	}

	function NumeroRows($origen)
	{
		$dbResultados=$this->m_dbResultados[$origen];

		if($dbResultados)
		{
			return mysqli_num_rows($dbResultados);
		}

		return 0;
	}

	function UltimoInsertID()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysqli_insert_id($this->m_link);
	}
	public function Autocommit($valor='TRUE')
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysqli_autocommit($this->m_link, $valor);
	}
	public function Commit()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysqli_commit($this->m_link);
	}
	public function Rollback()
	{
		if ( ! $this->m_link )
		{
			return 0;
		}
		return mysqli_rollback($this->m_link);
	}
}
?>
