<?php

define("MAX_FILE_SIZE",1000);
define("IMAGE_TYPE","^.*\.(jpg|gif|png|jpeg)$");
define("MIME_TYPE","^image\/(jpeg|gif|png|pjpeg)$");

define("ERR0","Error: La extencion del archivo no es valida!");
define("ERR1","Error: El tipo de archivo de imagen no es valido!");
define("ERR2","Error: El limite de tamaño de la imagen es de 1 MB!");
define("ERR3","Error: El directorio no tiene los permisos suficientes cambiar a 777!");
define("ERR4","Error: No se subio el archivo!");
define("UPLOAD_SUCCESS",200);
 
class UPLOADER
{
	var $m_filename;
	var $m_name;
	
	function UPLOADER()
	{
		$this->I_Init(); 
	}
	
	function I_Init()
	{
		$this->m_filename = "";
		$this->m_name = "";
	}
	
	function ValidaUpload($file)
	{
		if(isset($_FILES[$file]) && strlen($_FILES[$file]['name']) > 1)
		{
			return UPLOAD_SUCCESS;
		}
		return ERR4;
	}
	
	function ValidaImagenType($file)
	{
		if((eregi(IMAGE_TYPE,$_FILES[$file]['name'])))
		{
			return UPLOAD_SUCCESS;
		}
		else
		{
			unlink($_FILES[$file]['tmp_name']);
			return ERR0;
		}
	}
	
	function ValidaImagenMIMEType($file)
	{
		$img=getimagesize($_FILES[$file]['tmp_name']);
		
		if((eregi(MIME_TYPE,$_FILES[$file]['type'])) && (eregi(MIME_TYPE,$img['mime'])))
		{
			return UPLOAD_SUCCESS;
		}
		else
		{
			unlink($_FILES[$file]['tmp_name']);
			return ERR1;
		}
	}
	
	function ValidaImagenSize($file)
	{
		if($_FILES[$file]['size'] < MAX_FILE_SIZE)
		{
			return UPLOAD_SUCCESS;
		}
		else
		{
			unlink($_FILES[$file]['tmp_name']);
			return ERR2;
		}
	}
	
	
	function Upload($file, $path)
	{
		
		
		if(is_uploaded_file($_FILES[$file]['tmp_name']))
		{
			
			if(copy($_FILES[$file]['tmp_name'],$path."/".$_FILES[$file]['name']))
			{
				return UPLOAD_SUCCESS;
			}
			else
			{
				unlink($_FILES[$file]['tmp_name']);				
				return ERR3;
				
			}
		}
	}
	
	function SetNombreImagen($file)
	{
		$this->m_name = $_FILES[$file]['name'];
	}
	
	function GetNombreImagen()
	{
		return $this->m_name;
	}
	
	function ValidarImagen($file, $path)
	{
		$this->m_filename = $file;
		
		if($result = $this->ValidaUpload($this->m_filename) != UPLOAD_SUCCESS)
		{
				
			return ERR4;
		}
		
		if($result = $this->ValidaImagenType($this->m_filename) != UPLOAD_SUCCESS)
		{
							
			return ERR0;
		}
		
		if($result = $this->ValidaImagenMIMEType($this->m_filename) != UPLOAD_SUCCESS)
		{
							
			return ERR1;
		}
		
	/*	if($result = $this->ValidaImagenSize($this->m_filename) != UPLOAD_SUCCESS)
		{				
			return ERR2;
		}
	*/	
		if($result = $this->Upload($this->m_filename, $path) != UPLOAD_SUCCESS)
		{			
			return ERR3;
		}
		
		$this->SetNombreImagen($this->m_filename);
		
		return UPLOAD_SUCCESS;

	}
}
 
$MyUploader = new UPLOADER;

?>
