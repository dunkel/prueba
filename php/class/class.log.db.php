<?php

$ErrTxt = 200;
define("LOGDB_SUCCESS", 		$ErrTxt++);
define("LOGDB_ERROR", 		$ErrTxt++);


class LOG_DB
{
	var $m_consultas;
	var $m_total;
	var $m_id;
	var $m_tabla;
	var $m_id_rel;
	var $m_type;
	var $m_detalle;
	var $m_fecha;
	var $m_hora;
	var $m_id_usuarios;
	var $m_nom_usuarios;
	var $m_ap_usuarios;
	var $m_ip;
	var $m_mac;

	function LOG_DB()
	{
		$this->I_init();
	}

	function I_init()
	{
		$this->m_consultas	= new CONSULTAS;
		$this->m_total		= "";
		$this->m_id			= array();
		$this->m_tabla		= array();
		$this->m_id_rel		= array();
		$this->m_type		= array();
		$this->m_detalle	= array();
		$this->m_fecha		= array();
		$this->m_hora		= array();
		$this->m_id_usuarios	= array();
		$this->m_nom_usuarios	= array();
		$this->m_ap_usuarios	= array();
		$this->m_ip		= array();
		$this->m_mac		= array();
	}

	function getTotal()
	{
		return $this->m_total;
	}

	function getID($id)
	{
		return $this->m_id[$id];
	}

	function getTabla($id)
	{
		return $this->m_tabla[$id];
	}

	function getIdRel($id)
	{
		return $this->m_id_rel[$id];
	}

	function getType($id)
	{
		return $this->m_type[$id];
	}

	function getDetalle($id)
	{
		return $this->m_detalle[$id];
	}

	function getFecha($id,$type)
	{
		global $_Months;

		if($type == 'sql')
		{
			$fecha = $this->m_fecha[$id];
		}
		elseif($type == 'interface')
		{
			list($anio,$mes,$dia) = split("-",$this->m_fecha[$id]);
			$fecha = $dia." ".$_Months[$mes]." ".$anio;
		}
		return $fecha;
	}

	function getHora($id)
	{
		return $this->m_hora[$id];
	}
	function getUsuario($id, $type='sql')
	{
		if($type == 'sql')
		{
			$usuario = $this->m_id_area[$id];
		}
		elseif($type == 'interface')
		{
			$usuario = $this->m_nom_usuarios[$id]." ".$this->m_ap_usuarios[$id];
		}
		return $usuario;
	}
	function getIP($id)
	{
		return $this->m_ip[$id];
	}
	function getMac($id)
	{
		return $this->m_mac[$id];
	}


	function Coleccion($tampag = '1', $page = '1' , $by = 'logdatabase.id', $order = 'DESC', $tabla = '', $type = '', $id_rel = '', $usuario = '', $fecha = '')
	{
		$this->I_Init();

		$reg1 = ($page-1) * $tampag;

		$this->m_consultas->NuevaConsulta();


		$campos = array("logdatabase.*", "usuarios.*");
		$condiciones = array("logdatabase.usuario=usuarios.ID and 1" => "1");

		if(!empty($id_rel))
		{
			$condiciones['logdatabase.id_rel'] = $id_rel;
		}

		if(!empty($tabla))
		{

			$condiciones["logdatabase.tabla"] = $tabla;

		}
		if(!empty($type))
		{
			$condiciones["logdatabase.type='$type' and 1"] = 1;
		}
		if(!empty($usuario))
		{
			$condiciones["usuarios.usuario like '%$usuario%' and 1"] = "1";
		}
		if(!empty($fecha))
		{
			$condiciones["logdatabase.fecha"] = $fecha;
		}

		$result = $this->m_consultas->SeleccionarTablaFila("logdatabase, usuarios",$campos, $condiciones, "","$by $order LIMIT $reg1, $tampag");

		if($tampag > 1)
		{
			$this->m_total = $this->m_consultas->NumeroRegistros();
		}

		$i=0;
		$collection = array();

		if($result == CONSULTAS_SUCCESS)
		{
			while($registro = $this->m_consultas->Fetch())
			{
				$collection[$i] = $registro["id"];

				$this->m_id[$collection[$i]]			=	$registro['id'];
				$this->m_id_rel[$collection[$i]]		=	$registro['id_rel'];
				$this->m_tabla[$collection[$i]]			=	$registro['tabla'];
				$this->m_type[$collection[$i]]			=	$registro['type'];
				$this->m_fecha[$collection[$i]]			=	$registro['fecha'];
				$this->m_id_usuarios[$collection[$i]]		= 	$registro["ID"];
				$this->m_nom_usuarios[$collection[$i]]		= 	$registro["NOMBRE"];
				$this->m_ap_usuarios[$collection[$i]]		= 	$registro["APELLIDO"];
				$this->m_hora[$collection[$i]]			=	$registro['hora'];
				$this->m_ip[$collection[$i]]			=	$registro['ip'];
				$this->m_mac[$collection[$i]]			=	$registro['mac'];

				$lista = explode("\n",$registro['detalle']);
				if(count($lista) > 0)
				{
					$detalle = array();
					foreach($lista as $value)
					{
						list($campo, $valor) = explode("=>" , $value);
						$detalle[$campo] = $valor;
					}
				}

				$this->m_detalle[$collection[$i]]	=	$registro['detalle'];
				$i++;
			}
		}
		else
		{
			$this->m_total = 0;
			return LOGDB_ERROR;
		}
		return $collection;
	}

	function AddEntrada($table,$id_rel, $type, $campos = array(),$condiciones = array())
	{
		global $MySPANEL;
		global $MyDebug;

		if(empty($table) || empty($type))
		{
			return LOGDB_ERROR;
		}
		$detalle ='';
		$condicion ='';
		if(count($campos) > 0)
		{
			foreach ($campos as $key => $value)
			{
				$detalle .= "$key => ".str_replace("'","",$value)."\n";
			}

		}

		$nvoregistro = array(
							'tabla'		=> $table,
							'id_rel'	=> $id_rel,
							'type'		=> $type,
							'detalle'	=> $detalle,
							'fecha'		=> date('Y-m-d'),
							'hora'		=> date('H:i:s'),
							'usuario'	=> $MySPANEL->ID(),
							'ip'		=> IP_REAL(),
							'mac'		=> ''
		);
		if(count($condiciones) > 0)
		{
			foreach ($condiciones as $key => $value)
			{
				$condicion .= "$key => ".str_replace("'","",$value)."\n";
			}
			$nvoregistro['condiciones']=$condicion;
		}
		if($this->m_consultas->AgregarRegistro("logdatabase_$table", $nvoregistro) != CONSULTAS_SUCCESS)
		{
			$MyDebug->DebugMessage("LOG_DB::AddEntrada: Fallo [$table] [$id_rel] [$type] [$detalle] [$condicion]");
			return LOGDB_ERROR;

		}

		return LOGDB_SUCCESS;

	}
	function AddEntradas($table,$id_rel, $type, $campos = array(),$condiciones = array())
	{
		global $MyDebug;
		$detalle ='';
		$condicion ='';
		if(empty($table) || empty($type))
		{
			return LOGDB_ERROR;
		}

		if(count($campos) > 0)
		{
			foreach ($campos as $key => $value)
			{
				$detalle .= "$key => ".str_replace("'","",$value)."\n";
			}

		}

		$nvoregistro = array(
							'tabla'		=> $table,
							'id_rel'	=> $id_rel,
							'type'		=> $type,
							'detalle'	=> $detalle,
							'fecha'		=> date('Y-m-d'),
							'hora'		=> date('H:i:s'),
							'ip'		=> IP_REAL(),
							'mac'		=> ''
		);
		if(count($condiciones) > 0)
		{
			foreach ($condiciones as $key => $value)
			{
				$condicion .= "$key => ".str_replace("'","",$value)."\n";
			}
			$nvoregistro['condiciones']=$condicion;
		}
		if($this->m_consultas->AgregarRegistro("logdatabase", $nvoregistro) != CONSULTAS_SUCCESS)
		{
			$MyDebug->DebugMessage("LOG_DB::AddEntrada: Fallo [$table] [$id_rel] [$type] [$detalle] [$condicion]");
			return LOGDB_ERROR;

		}

		return LOGDB_SUCCESS;

	}
}
$MyLogDB = new LOG_DB;
?>
