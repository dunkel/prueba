<?php
$method = strtolower($_SERVER['REQUEST_METHOD']);
$http_vars = array();
	switch($method)
	{
		case 'get':
			$CONTEXT =& $_GET;
			$http_vars = isset($HTTP_GET_VARS)?$HTTP_GET_VARS:'';
			break;
		case 'post':
			$CONTEXT =& $_POST;
			$http_vars = isset($HTTP_GET_VARS)?$HTTP_GET_VARS:'';
			break;
		default:
			$CONTEXT = array();
			break;
	}




$_Months = array ('01'=>"Ene",
                '02'=>"Feb",
                '03'=>"Mar",
                '04'=>"Abr",
                '05'=>"May",
                '06'=>"Jun",
                '07'=>"Jul",
                '08'=>"Ago",
                '09'=>"Sep",
                '10'=>"Oct",
                '11'=>"Nov",
                '12'=>"Dic");

$_MES = array(
		"01"	=> "Enero",
		"02"	=> "Febrero",
		"03"	=> "Marzo",
		"04"	=> "Abril",
		"05"	=> "Mayo",
		"06"	=> "Junio",
		"07"	=> "Julio",
		"08"	=> "Agosto",
		"09"	=> "Septiembre",
		"10"	=> "Octubre",
		"11"	=> "Noviembre",
		"12"	=> "Diciembre"
		);
$_BIM = array(
		"1"	=> "PRIMERO",
		"2"	=> "PRIMERO",
		"3"	=> "SEGUNDO",
		"4"	=> "SEGUNDO",
		"5"	=> "TERCERO",
		"6"	=> "TERCERO",
		"7"	=> "CUARTO",
		"8"	=> "CUARTO",
		"9"	=> "QUINTO",
		"10"	=> "QUINTO",
		"11"	=> "SEXTO",
		"12"	=> "SEXTO"
		);
$Conservacion =array(	"1.-Muy Bueno"	=>	"1.00000",
			"2.-Bueno"	=>	"0.87500",
			"3.-Medio"	=>	"0.75000",
			"4.-Malo"	=>	"0.40000",
			"5.-Muy malo"	=>	"0.25000",
			"6.-Ruinoso"	=>	"0.08000"
			);
$_BIMESTRES=array("2"=>"Enero-Febrero",
		"4"=>"Marzo-Abril",
		"6"=>"Mayo-Junio",
		"8"=>"Julio-Agosto",
		"10"=>"Septiembre-Octubre",
		"12"=>"Noviembre-Diciembre"
				);
$__BIMESTRES=array("1"=>"Enero-Febrero",
		"2"=>"Marzo-Abril",
		"3"=>"Mayo-Junio",
		"4"=>"Julio-Agosto",
		"5"=>"Septiembre-Octubre",
		"6"=>"Noviembre-Diciembre"
				);
$_r_BIMESTRES=array("1"=>"2",
		"2"=>"4",
		"3"=>"6",
		"4"=>"8",
		"5"=>"10",
		"6"=>"12"
				);
$_DIAS = array("1"  => "LUNES",
		"2" => "MARTES",
		"3" => "MIERCOLES",
		"4" => "JUEVES",
		"5" => "VIERNES",
		"6" => "SABADO",
		"7" => "DOMINGO",
		);
$_MESES = array(
		"1"	=> "Enero",
		"2"	=> "Febrero",
		"3"	=> "Marzo",
		"4"	=> "Abril",
		"5"	=> "Mayo",
		"6"	=> "Junio",
		"7"	=> "Julio",
		"8"	=> "Agosto",
		"9"	=> "Septiembre",
		"10"	=> "Octubre",
		"11"	=> "Noviembre",
		"12"	=> "Diciembre"
		);
define("NIVEL_CAJERA",		1);
define("NIVEL_BANCO",		2);
$_NIVELCAJERA = array('CAJERA'		=>	NIVEL_CAJERA,
                    'BANCO'		=>	NIVEL_BANCO
                 );

function Sanitizacion($var)
{
	$var = htmlentities($var, ENT_QUOTES, "UTF-8");
	$var = strip_tags($var);
	if(!get_magic_quotes_gpc())
	{
		$var  = addslashes($var);
	}
	return $var;
}
function htmlclean($input)
{
     $sb_convert = $input;
     $sb_input = array("<",">","(",")");
     $sb_output = array("&lt;","&gt;","&#40;","&#41;");
     $output = str_replace($sb_input, $sb_output, $sb_convert);
     return $output;
}
function ValidarIESeis()
{
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$posicion = strrpos($user_agent, "MSIE 6.0");
	if ($posicion != false)
	{
		$ie = true;
	}
	return $ie;
}
function Mayusculas($txt)
{
	if(!empty($txt) && !is_array($txt))
	{
		$txt = mb_strtoupper($txt, 'UTF-8');
	}
	return $txt;
}
function check_email_address($email)
{
	// Primero, checamos que solo haya un s�mbolo @, y que los largos sean correctos
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
	{
		// correo inv�lido por n�mero incorrecto de caracteres en una parte, o n�mero incorrecto de s�mbolos @
    return false;
  }
  // se divide en partes para hacerlo m�s sencillo
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++)
	{
    if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
		{
      return false;
    }
  }
  // se revisa si el dominio es una IP. Si no, debe ser un nombre de dominio v�lido
	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1]))
	{
     $domain_array = explode(".", $email_array[1]);
     if (sizeof($domain_array) < 2)
		 {
        return false; // No son suficientes partes o secciones para se un dominio
     }
     for ($i = 0; $i < sizeof($domain_array); $i++)
		 {
        if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
				{
           return false;
        }
     }
  }
  return true;
}
function makeHTMLPaginar1($numrows1, $maxPage, $terminamosconel,$paginanum, $by, $order, $tampag,$titulo)
{
	if ($terminamosconel >= $numrows1)
	{
		$terminamoscon = $numrows1;
	}
	else
	{
		$terminamoscon = $terminamosconel;
	}
	$empezamoscon = $terminamosconel - ($tampag-1);

	if ($paginanum > 1)
	{
		$page = $paginanum - 1;
		$prev = " <a  href=\"javascript: PaginacionSubmit('$page','$by','$order');\">";
		$prev .= "Anterior</a> ";
	}
	else
	{
		$prev  = '';
	}

	if ($paginanum < $maxPage)
	{
		$page = $paginanum + 1;
		$next = " <a  href=\"javascript: PaginacionSubmit('$page','$by','$order');\"> ";
		$next .= "Siguiente </a> ";
	}
	else
	{
		$next = '';
	}

	$html .= "<span >".$first.$prev."  $empezamoscon - $terminamoscon (de $numrows1 $titulo) ".$next.$last."</span>";

	if($tampag <= $numrows1)
	{

		$html .= "<br />";

		$anterior		= $paginanum - 1;
		$posterior		= $paginanuml + 1;

		if ($paginanum > 1)
    		$html .= "<a  href=\"javascript: PaginacionSubmit('1','$by','$order');\" >&laquo;</a> ";
  		else
    		$html.= "<span><b>&laquo;</b></span>";


		if ($paginanum< 6)
		{
			for ($i=1; $i<$paginanum; $i++)
				$html .= "<a  href=\"javascript: PaginacionSubmit('$i','$by','$order');\" > $i</a> ";
  		}
		else
		{
			for ($i=$paginanum-5; $i<$paginanum; $i++)
				$html .= "<a  href=\"javascript: PaginacionSubmit('$i','$by','$order');\" > $i</a> ";
		}

		$html .= "<span>[<b>$paginanum</b>]</span>";

		if($paginanum +6 > $maxPage)
		{
			for ($i=$paginanum+1; $i<=$maxPage; $i++)
			    	$html .= "<a  href=\"javascript: PaginacionSubmit('$i','$by','$order');\" > $i</a> ";
  		}
		else
		{
			for ($i=$paginanum+1; $i<=$paginanum+6; $i++)
			    	$html .= "<a  href=\"javascript: PaginacionSubmit('$i','$by','$order');\" > $i</a> ";
		}
		if ($paginanum<$maxPage)
		    	$html .= "<a  href=\"javascript: PaginacionSubmit('$maxPage','$by','$order');\" > &raquo;</a>";
	  	else
		    	$html .= "<span><b>&raquo;</b></span>";

		$html .= "<br />";
	}

	return $html;
}

function makeHTMLPaginar($numrows1, $maxPage, $terminamosconel,$paginanum, $by, $order, $tampag)
{
	if ($terminamosconel >= $numrows1)
	{
		$terminamoscon = $numrows1;
	}
	else
	{
		$terminamoscon = $terminamosconel;
	}
	$empezamoscon = $terminamosconel - ($tampag-1);

	if ($paginanum > 1)
	{
		$page = $paginanum - 1;
		$prev = " <a  href=\"javascript: PaginacionSubmit('$page','$by','$order');\">";
		$prev .= "Anterior</a> ";
	}
	else
	{
		$prev  = '';
	}

	if ($paginanum < $maxPage)
	{
		$page = $paginanum + 1;
		$next = " <a  href=\"javascript: PaginacionSubmit('$page','$by','$order');\"> ";
		$next .= "Siguiente </a> ";
	}
	else
	{
		$next = '';
	}

	$html .= "<span >".$first.$prev."  $empezamoscon - $terminamoscon (de $numrows1 Productos) ".$next.$last."</span>";

	if($tampag <= $numrows1)
	{
		$html .= "<br />";
		$anterior		= $paginanum - 1;
		$posterior		= $paginanuml + 1;
		$html .= "<br />";
	}
	return $html;
}

function makeHTMLMsgJquery($id='',$title,$msg,$color)
{
	$icons='fa-check';
	if($color=='#a90329'){
		$icons='fa-times';
	}
	$html="$.smallBox({
				title : '$title',
				content : '$msg',
				color : '$color',
				iconSmall : 'fa $icons fa-2x fadeInRight animated',
				timeout : 5000
			});
			";
	if(!empty($id))
	{
		$html.="$('#row-$id').fadeOut('slow');";
	}
	return $html;
}
function IP_REAL()
{
	if (isset($_SERVER))
	{
		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
			return $_SERVER["HTTP_X_FORWARDED_FOR"];
		if (isset($_SERVER["HTTP_CLIENT_IP"]))
			return $_SERVER["HTTP_CLIENT_IP"];

		return Sanitizacion($_SERVER["REMOTE_ADDR"]);
	}

	if (getenv('HTTP_X_FORWARDED_FOR'))
		return getenv('HTTP_X_FORWARDED_FOR');
	if (getenv('HTTP_CLIENT_IP'))
		return getenv('HTTP_CLIENT_IP');
	return Sanitizacion(getenv('REMOTE_ADDR'));
}
function MAC_REAL()
{
	$ip = IP_REAL();
	$comando=`/usr/sbin/ping $ip 5`;
    	if (preg_match ("no", $comando)) return Sanitizacion($ip);
    	else
    	{
        	$comando=`/usr/sbin/arp $ip`;
        	ereg(".{1,2}-.{1,2}-.{1,2}-.{1,2}-.{1,2}-.{1,2}|.{1,2}:.{1,2}:.{1,2}:.{1,2}:.{1,2}:.{1,2}", $comando, $mac);
            	return Sanitizacion($mac[0]);
    	}
}
function PermisosArchivo($file)
{
	$perms = fileperms($file);

	if (($perms & 0xC000) == 0xC000)
	{
		// Socket
		 $info = 's';
	}
	elseif (($perms & 0xA000) == 0xA000)
	{
		// Enlace Simb�lico
		$info = 'l';
	}
	elseif (($perms & 0x8000) == 0x8000)
	{
		// Regular
		$info = '-';
	}
	elseif (($perms & 0x6000) == 0x6000)
	{
		// Bloque especial
		$info = 'b';
	}
	elseif (($perms & 0x4000) == 0x4000)
	{
    	// Directorio
    	$info = 'd';
	}
	elseif (($perms & 0x2000) == 0x2000)
	{
		// Caracter especial
		$info = 'c';
	}
	elseif (($perms & 0x1000) == 0x1000)
	{
    	// Pipe FIFO
    	$info = 'p';
	}
	else
	{
    	// Desconocido
    	$info = 'u';
	}
	// Due�o
	$info .= (($perms & 0x0100) ? 'r' : '-');
	$info .= (($perms & 0x0080) ? 'w' : '-');
	$info .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));
	// Grupo
	$info .= (($perms & 0x0020) ? 'r' : '-');
	$info .= (($perms & 0x0010) ? 'w' : '-');
	$info .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));
	// Mundo
	$info .= (($perms & 0x0004) ? 'r' : '-');
	$info .= (($perms & 0x0002) ? 'w' : '-');
	$info .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));
	return $info;

}


function makeHTMLOrder($order, $ordercampo, $campo, $caption, $page, $width)
{
	if($campo == $ordercampo && (!empty($campo) && !empty($ordercampo)))
	{
		$order = $order == "ASC" ? "DESC" : "ASC";
		$thisClass = $order == "ASC" ? "class='MyASC'" : "class='MyDESC'";
	}
	else
	{
		$thisClass = "class='THtitle'";
	}
	if(!empty($width))
	{
		$thisWidth = "width='$width'";
	}
	if(!empty($caption))
	{
		$caption = "<a href=\"javascript:PaginacionSubmit('$page','$campo','$order');\" >$caption</a>";
	}

	$html = "<td $thisWidth  $thisClass>$caption</td>";
	return $html;
}

function makeHTMLImg($src, $width, $height, $alt)
{
	if(!empty($height))
	{
		$height = "height='$height'";
	}

	if(!empty($width))
	{
		$width = "width='$width'";
	}

	$html = "<img src='$src' $width $height border='0' alt='$alt' title='$alt' />";
	return ($html);
}
function NIVELUSERS($id,$funcion,$value,$caption)
{
	global $_NIVELUSER;
	$html = "<SELECT name='$id' id='$id' $funcion  >
		<OPTION value=''>$caption</OPTION>";
	foreach($_NIVELUSER as $llave => $valor)
	{
		$html .= "<OPTION value='".$valor."' ";
		if($valor == $value)
		{
			$html .= "selected='selected'";
		}
		$html .= ">$llave</OPTION>";
	}
	$html .= "</SELECT>";
	return $html;
}
function SelectMeses($id,$funcion,$value,$caption)
{
	global $_MESES;
	$html = "<SELECT name='$id' id='$id' $funcion  >
		<OPTION value=''>$caption</OPTION>";
	foreach($_MESES as $llave => $valor)
	{
		$html .= "<OPTION value='".$llave."' ";
		if($llave == $value)
		{
			$html .= "selected='selected'";
		}
		$html .= ">$llave</OPTION>";
	}
	$html .= "</SELECT>";
	return $html;
}
function SelectAno($id,$funcion,$value,$caption)
{
	$html = "<SELECT name='$id' id='$id' $funcion >
		<OPTION value=''>$caption</OPTION>";
	$x=(date('Y')-6);
	while($x <= date(Y))
	{
		$html .= "<OPTION value='".$x."' ";
		if($x == $value)
		{
			$html .= "selected='selected'";
		}
		$html .= ">$x</OPTION>";
		$x++;
	}
	$html .= "</SELECT>";
	return $html;

}
function nivelUser($id,$funcion,$value,$caption)
{
	$html = "<SELECT name='$id' id='$id' $funtion>
		<OPTION value=''></OPTION>
		<OPTION value='".NIVEL_USERADMIN."' ";
	if(NIVEL_USERADMIN == $value)
		$html .= "selected='selected'";
	$html .= ">Administrador</OPTION>
		<OPTION value='".NIVEL_USERMIEMBRO."'";
	if(NIVEL_USERMIEMBRO == $value)
		$html .= "selected='selected'";
	$html .= ">Miembro</OPTION>
		</SELECT>";
	return $html;

}
function getMetaKeywords($text)
{
	$preposiciones=array(	"durante","mediante","quizas","tambien",
				"tampoco","despues","cierto","aprisa","deprisa",
				"bastante", "arriba", "ma�ana", "despacio",
				"abajo", "entonces", "casi", "alrededor",
				"pronto", "dentro", "tarde", "fuera", "siempre",
				"muchos","muchas","mucho","mucha","algunas",
				"esfuerzo", "ademas","aunque","cuenta","acerca",
				"grandes","resultado","porque","nombre","titulo","tienes","embargo"
				);

	$text = mb_convert_encoding($text, 'UTF-8', 'HTML-ENTITIES');
	$tofind = "�����������������������������������������������������";
	$replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
	$text = strtr(utf8_decode($text),$tofind,$replac);
	// Limpiamos el texto
	$text = strip_tags($text);
        $text = strtolower($text);
	$text = trim($text);
	$text = preg_replace('/[^a-zA-Z0-9 -]/', ' ', $text);
	// extraemos las palabras
	$match = explode(" ", $text);
	// contamos las palabras
	$count = array();
        if (is_array($match))
	{
		foreach ($match as $key => $val)
		{
			if (strlen($val)> 5 && !in_array(strtolower($val),$preposiciones))
				{
					if (isset($count[$val]))
					{
						$count[$val]++;
					} else {
					$count[$val] = 1;
					}
				}
		}
	}
	// Ordenamos los totales
	arsort($count);
	$count = array_slice($count, 0, 15);
	return implode(", ", array_keys($count));
}
function FechaInterface($fecha_in)
{
	global $_Months;
	$fecha_aux =explode("-",$fecha_in);
	$fechas_out = $fecha_aux[2]." de ".$_Months[$fecha_aux[1]]." de ".$fecha_aux[0];
	return $fechas_out;
}
function CambioFecha($fecha_in)
{
	$fecha_aux  =explode("-",$fecha_in);
	$fechas_out = $fecha_aux[2]."-".$fecha_aux[1]."-".$fecha_aux[0];
	return $fechas_out;
}
function RegresarFecha($fecha_inf)
{
	$fecha_aux  =explode("-",$fecha_in);
	$fechas_out = $fecha_aux[2]."-".$fecha_aux[1]."-".$fecha_aux[0];
	return $fechas_out;
}
function getFecha($id, $dia='', $mes='', $ano='')
{
	$_MES = array(
		"1"	=> "Enero",
		"2"	=> "Febrero",
		"3"	=> "Marzo",
		"4"	=> "Abril",
		"5"	=> "Mayo",
		"6"	=> "Junio",
		"7"	=> "Julio",
		"8"	=> "Agosto",
		"9"	=> "Septiembre",
		"10"	=> "Octubre",
		"11"	=> "Noviembre",
		"12"	=> "Diciembre"
		);
	$html="<select name='dia$id' type='text' />";
	$html.="<OPTION value=''>D&iacute;a...</OPTION>";
	for($x=1;$x<32;$x++)
	{
		$html.="<OPTION values='$x' ";
		if($dia == $x)
		{
			$html .= "selected='selected' ";
		}
		$html .= ">$x</OPTION>";
	}
	$html.="</SELECT>-";
	$html.="<select name='mes$id' type='text' />";
	$html.="<OPTION value=''>Mes...</OPTION>";
	for($x=1;$x<13;$x++)
	{
		$html.="<OPTION value='$x'";
		if($mes==$x)
		{
			$html.="selected='selected' ";
		}
		$html.="> $_MES[$x]</OPTION>";
	}
 	$html.="</SELECT>-";
	$html.="<select name='ano$id' type='text'/>";
	$html.="<OPTION value=''>A&ntilde;o...</OPTION>";
	$x=date(Y)+"5";
	while($x!=1900)
	{
		$html.="<OPTION values='$x' ";
		if($ano == $x)
		{
			$html .= "selected='selected' ";
		}
		$html .= ">$x</OPTION>";
		$x=$x-1;
	}
	$html.="</SELECT>";
	return $html;
}

function getHora($id, $hrs='', $min='', $seg='')
{

	$html="<select name='hrs_$id' type='text' >";
	$html.="<OPTION value=''>Hrs.</OPTION>";
	for($x=0;$x<24;$x++)
	{
		if(strlen($x)==1){
			$x="0".$x;
		}
		$html.="<OPTION values='$x' ";
		if($hrs == $x)
		{
			$html .= "selected='selected' ";
		}
		$html .= ">$x</OPTION>";
	}
	$html.="</SELECT>-";
	$html.="<select name='min_$id' type='text' >";
	$html.="<OPTION value=''>Min.</OPTION>";
	for($x=0;$x<59;$x++)
	{
		if(strlen($x)==1){
			$x="0".$x;
		}
		$html.="<OPTION value='$x'";
		if($min==$x)
		{
			$html.="selected='selected' ";
		}
		$html.="> $x</OPTION>";
	}
 	$html.="</SELECT>-";
	$html.="<select name='seg_$id' type='text' >";
	$html.="<OPTION value=''>Seg.</OPTION>";
	for($x=0;$x<59;$x++)
	{
		if(strlen($x)==1){
			$x="0".$x;
		}
		$html.="<OPTION value='$x'";
		if($seg==$x)
		{
			$html.="selected='selected' ";
		}
		$html.="> $x</OPTION>";
	}
	$html.="</SELECT>";
	return $html;
}
function UTF($txt)
{
	$encoding = mb_detect_encoding($txt, 'ASCII,UTF-8,ISO-8859-1');
 	if ($encoding == "ISO-8859-1")
	{
	     	$txt = utf8_encode($txt);
	}
	if ($encoding == "UTF-8")
	{
	     	$txt = utf8_encode($txt);
	}
 	return $txt;
}
function UTFS($text)
{
	$text = mb_convert_encoding($text, 'UTF-8', 'HTML-ENTITIES');
	$text= utf8_decode($text);
	return $text;
}
function SelectTramites($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("tipo_tram.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("tipo_tram", $campos, "", "id_tipo_tram ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_tipo_tram"];
			$nombre = $registro["nombre_tram"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectRequisitos($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("requisitos.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("requisitos", $campos, "", "nombre_req ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_req"];
			$nombre = $registro["nombre_req"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)




			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function selectArea($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("id_area","nombre_area");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("area", $campos, "", "nombre_area ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_area"];
			$nombre = $registro["nombre_area"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTF($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectEstados($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("edos.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("edos", $campos, "", "nom_edo ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_edo = $registro["id_edos"];
			$nom_edo = $registro["nom_edo"];
			$html .= "<option value='$id_edo' ";
			if($value == $id_edo)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nom_edo."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectMunicipios($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("muni.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("muni", $campos, "", "nom_mun ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_edo = $registro["id_mun"];
			$nom_edo = $registro["nom_mun"];
			$html .= "<option value='$id_edo' ";
			if($value == $id_edo)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nom_edo."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectEdoCivil($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("edo_civil.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("edo_civil", $campos, "", "id_civil ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_edo = $registro["id_civil"];
			$nom_edo = $registro["nom_civil"];
			$html .= "<option value='$id_edo' ";
			if($value == $id_edo)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nom_edo."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}


function SelectCp($id,$funcion,$value,$caption)
{

	$consultas = new CONSULTAS;
	$campos = array("cp.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("cp", $campos, "", "colonia ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_colonia = $registro["id_cp"];
			$nombre = $registro["colonia"];
			$html .= "<option value='$id_colonia' ";
			if($value == $id_colonia)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectOcupacion($id,$value)
{
	$consultas = new CONSULTAS;
	$campos = array("ocupacion.*");

	if($consultas->SeleccionarTodo("ocupacion", $campos, "", "id_ocupacion ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_colonia = $registro["id_ocupacion"];
			$nombre = $registro["nom_ocupacion"];
			$html .= "<input type='radio' name='$id' id='$id' ";
			$html .= "value='$id_colonia' ";
			if($value == $id_colonia)
			{
				$html .= "checked='checked'";
			}

			$html .= ">".$nombre."<br />";
		}
	}
	$html .= "</select>";
	return $html;
}
function BuscarOcuapcion($id)
{
    $consultas = new CONSULTAS;
    $campos = array("ocupacion.*");
    $condicion=array("id_ocupacion"=>$id);
    if($consultas->SeleccionarTablaFila("ocupacion",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                    $ocupacion = $registro["nom_ocupacion"];
                }
        }
        return $ocupacion;
}
function SelectColonias($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("colonias.*");
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("colonias", $campos, "", "col_nombre ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_colonia = $registro["id_colonia"];
			$clave = $registro["col_clave"];
			$nombre = $registro["col_nombre"];
			$html .= "<option value='$clave' ";
			if($value == $clave)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function Listar_bimestres($ano_b)
{
	$dia=date('d');
	$mes=date('m');
	$ano=date('Y');
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $mes = date("n",$fecha_exp_a);
	if($mes%2==1)
	{
		$bi_act = ($mes/2)+".5";
		$cob=0;
	}else
	{
		$bi_act =$mes/2;
		$cob=1;
	}
	if(date('Y')== $ano_b)
	{
		for($c=$bi_act;$c>=1;$c--)
		{
			$val_bi[$c]=$cob;
			$cob=$cob+2;
		}
		$valores[$ano_b]=$val_bi;

	}else
	{
		$cob=1;
		$bi_atr = 6;
		for($c=$bi_atr;$c>=1;$c--)
		{
			$val_bi_b[$c]=$cob;
			$cob=$cob+2;
		}
		$valores[$ano_b]=$val_bi_b;

		$ano_sig=$ano_b+1;

		while($ano_sig < date('Y'))
		{
			for($c=$bi_atr;$c>=1;$c--)
			{
				$val_bi_s[$c]=12;
			}
			$valores[$ano_sig]=$val_bi_s;
			$ano_sig++;
		}
		$bi_act=6;
		for($c=$bi_act;$c>=1;$c--)
		{
			$val_bi_a[$c]=$mes;
		}
		$valores[date('Y')]=$val_bi_a;
	}
	return $valores;
}
function Listar_semestral($ano_b)
{
	$dia=date('d');
	$mes=date('m');
	$ano=date('Y');
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $mes = date("n",$fecha_exp_a);
	if($mes%2==1)
	{
		$bi_act = ($mes/2)+".5";
		$cob=0;
	}else
	{
		$bi_act =$mes/2;
		$cob=1;
	}
	if(date('Y')== $ano_b)
	{
		for($c=$bi_act;$c>=1;$c--)
		{
			$val_bi[$c]=$cob+2;
		}
		$valores[$ano_b]=$val_bi;

	}else
	{
		$cob=11;
		$bi_atr = 2;
		for($c=$bi_atr;$c>=1;$c--)
		{
			$val_bi_b[$c]=$cob;
			$cob=$cob-6;
		}
		$valores[$ano_b]=$val_bi_b;
		$ano_sig=$ano_b+1;
		while($ano_sig < date('Y'))
		{
			for($c=$bi_atr;$c>=1;$c--)
			{
				$val_bi_s[$c]=12;
			}
			$valores[$ano_sig]=$val_bi_s;
			$ano_sig++;
		}

		for($c=$bi_atr;$c>=1;$c--)
		{
			$val_bi_a[$c]=$mes;
		}
		$valores[date('Y')]=$val_bi_a;
	}
 	return $valores;
}
function Listar_anual($ano_b)
{
	$dia=date('d');
	$mes=date('m');
	$ano=date('Y');
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $mes = date("n",$fecha_exp_a);
	if($mes%2==1)
	{
		$bi_act = ($mes/2)+".5";
		$cob=0;
	}else
	{
		$bi_act =$mes/2;
		$cob=1;
	}
	if(date('Y')== $ano_b)
	{
		$mese_sin_rec = Meses_sin_rec($ano_b);
		$val_bi[$c]=$mes-$mese_sin_rec;
		$valores[$ano_b]=$val_bi;
	}else
	{
		$mese_sin_rec = Meses_sin_rec($ano_b);
		$cob = 12-$mese_sin_rec;
		$bi_atr = 1;
		for($c=$bi_atr;$c>0;$c--)
		{
			$val_bi_b[$c]=$cob;
		}
		$valores[$ano_b]=$val_bi_b;
		$ano_sig=$ano_b+1;
		while($ano_sig < date('Y'))
		{
			for($c=$bi_atr;$c>0;$c--)
			{
				$val_bi_s[$c]=12;
			}
			$valores[$ano_sig]=$val_bi_s;
			$ano_sig++;
		}

		for($c=$bi_atr;$c>0;$c--)
		{
			$val_bi_a[$c]=$mes;
		}
		$valores[date('Y')]=$val_bi_a;
	}
	return $valores;
}
function Listar_bimestresAgua($ano_b)
{
	$consultas = new CONSULTAS;
	$sql = "SELECT bimestres('$ano_b') bimestres";
	$consultas->Query('bimestres', $sql);
	$bi = array();
	while ($result = $consultas->Fetch('bimestres'))
	{
		$bi = explode(',',$result['bimestres']);
	}
	return array_filter($bi);
}
function Recargos($bus_ano,$valor_cas,$actualizacion)
{
	$val_rec = Valor_rec($valor_cas,$bus_ano);
	$tipo_perido = 6;
	$aux_porc=array();
	if($tipo_perido==6)#bimestral
	{
		$col=Listar_bimestresAgua($bus_ano);
#		echo "<br />";
#		var_dump($col);
		foreach($col as $key => $value)
		{
			$bimestres = array();
#			echo "<br />$key => $value";
			$bimestres = explode(':',$value);
			$porcentaje = $bimestres[1]*$val_rec['porcentaje'];
#			echo "<br />s->$porcentaje = ".$bimestres[1]."*".$val_rec['porcentaje_a'];
			$aux_porc[$bimestres[0]]   = redondear_dos_decimal((($valor_cas+$actualizacion[$bimestres[0]])*$porcentaje)/100);
#			echo "<br />".$aux_porc[$bimestres[0]]."=(($valor_cas+".$actualizacion[$bimestres[0]].")*$porcentaje)/100";
		}

	}elseif($tipo_perido==2)#semestral
	{
		$col=Listar_semestral($bus_ano);
		foreach($col as $key => $value)
		{
			//echo "<br />$key => $value";
			$val_rec = Valor_rec($valor_cas,$key);
			foreach($value as $llave => $valor)
			{
				$porcentaje = $valor*$val_rec['porcentaje'];
				//echo "<br />s->$porcentaje = $valor*".$val_rec['porcentaje'];
				$aux_porc=$aux_porc+$porcentaje;
				//echo "<br />$aux_porc=$porcentaje";
			}
		}
	}elseif($tipo_perido==1)#anul
	{
		$col=Listar_anual($bus_ano);
		foreach($col as $key => $value)
		{
			//echo "<br />$key => $value";
			$val_rec = Valor_rec($valor_cas,$key);
			foreach($value as $llave => $valor)
			{
				$porcentaje = $valor*$val_rec['porcentaje'];
				//echo "<br />a->$porcentaje = $valor*".$val_rec['porcentaje'];
				$aux_porc=$aux_porc+$porcentaje;
				//echo  "<br />$aux_porc=$porcentaje";

			}
		}
	}
#	echo "<br />(($valor_cas/$tipo_perido)*$aux_porc)/100";

#	$rec[$bus_ano] = (($valor_cas/$tipo_perido)*$aux_porc)/100;

#	if($valor_cas < $rec[$bus_ano])
#	{
#		$rec[$bus_ano]=$valor_cas;
#	}
/*
	if(($bus_ano <= 2010) && ($bus_ano >= 2008))
	{
		$porsentaje=1.85;
	}elseif(($bus_ano >= 2005) && ($bus_ano <= 2007))
	{
		$porsentaje=1.68;
	}
	$i=0;

        foreach($col as $value)
        {
        	$rec[$i]=($val_cat_bi*($value*$porsentaje))/100;
		$i++;
        }
	$recargo=$rec[0]+$rec[1]+$rec[2]+$rec[3]+$rec[4]+$rec[5];*/
	return $aux_porc;
}
function Actualizacion($bus_ano,$valor_cas)
{
	$val_rec = Valor_rec($valor_cas,$bus_ano);
	$tipo_perido = 6;
	$aux_porc=array();
#	echo "<br />$bus_ano";
	if($tipo_perido==6)#bimestral
	{
		$col=Listar_bimestresAgua($bus_ano);
#		echo "<br />";
#		var_dump($col);
		foreach($col as $key => $value)
		{
			$bimestres = array();
#			echo "<br />$key => $value";
			$bimestres = explode(':',$value);
			$porcentaje = $bimestres[1]*$val_rec['porcentaje_a'];
#			echo "<br />s->$porcentaje = ".$bimestres[1]."*".$val_rec['porcentaje_a'];
			$aux_porc[$bimestres[0]]   = redondear_dos_decimal(($valor_cas*$porcentaje)/100);
#			echo "<br />".$aux_porc[$bimestres[0]]."=($valor_cas*$porcentaje)/100";
		}

	}elseif($tipo_perido==2)#semestral
	{
		$col=Listar_semestral($bus_ano);
		foreach($col as $key => $value)
		{
			$val_rec = Valor_rec($valor_cas,$key);
			foreach($value as $llave => $valor)
			{
				$porcentaje = $valor*$val_rec['porcentaje_a'];
				$aux_porc=$aux_porc+$porcentaje;
			}
		}
	}elseif($tipo_perido==1)#anul
	{
		$col=Listar_anual($bus_ano);
		foreach($col as $key => $value)
		{
			$val_rec = Valor_rec($valor_cas,$key);
			foreach($value as $llave => $valor)
			{
				$porcentaje = $valor*$val_rec['porcentaje_a'];
				$aux_porc=$aux_porc+$porcentaje;

			}
		}
	}

#	$rec[$bus_ano] = (($valor_cas/$tipo_perido)*$aux_porc)/100;

#	if($valor_cas < $rec[$bus_ano])
#	{
#		$rec[$bus_ano]=$valor_cas;
#	}
#	$actualizacion=0;
#	foreach ($aux_porc as $key => $value)
#	{
#		$actualizacion+=$value;
#	}
#	$rec[$bus_ano]=$actualizacion;
	return $aux_porc;
}
function Valor_rec($valor_cas,$ano)
{
	$consultas = new CONSULTAS;
	$campos = array("salario_anual.*");
	$condiciones=array("ano"=>$ano,"rango_ini <= $valor_cas and rango_final >= $valor_cas and  1"=>"1");

	if($consultas->SeleccionarTablaFila('salario_anual', $campos, $condiciones,'', "id_sal_ano ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id		=$registro["id_sal_ano"];
			$perido		=$registro["per_pago"];
			$porcentaje	=$registro["porcentaje"];
			$porcentaje_a	=$registro["porcentaje_a"];
		}
	}

	$valor_anual['periodo']		= $perido;
	$valor_anual['porcentaje']	= $porcentaje;
	$valor_anual['porcentaje_a']	= $porcentaje_a;
	return $valor_anual;
}
function Meses_sin_rec($ano)
{
	$consultas = new CONSULTAS;
	$campos = array("mes_sin_rec.*");
	$condiciones=array("ano_rec"=>$ano,"tipo"=>"1","status_rec"=>"1");

	if($consultas->SeleccionarTablaFila('mes_sin_rec', $campos, $condiciones,'', "id_mes_rec ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$mes		=$registro["mes_sin_rec"];
		}
	}
	return $mes;
}
function Impuesto_pred($valor_cas,$ano)
{
	$consultas = new CONSULTAS;
	$campos = array("limites.*","cal_imp.*");
	$condiciones=array("ano_curso"=>$ano);
	$inner=array("limites:cal_imp"=>"limites.id_limt=cal_imp.id_lim");
	if($consultas->QueryINNERJOIN($inner, $campos, $condiciones,'', "id_calculo ASC")==CONSULTAS_SUCCESS)
	{

		while($registro =$consultas->Fetch())
		{
			$id		=$registro["id_calculo"];
			$lim_inf	=$registro["lim_inf"];
			$lim_sup	=$registro["lim_sup"];
			$cuota_foja	=$registro["cuota_fija"];
			$factor		=$registro["factor"];
			if(($valor_cas >= $lim_inf) && ($valor_cas <= $lim_sup))
			{
				$imp_predial=(($valor_cas-$lim_inf)*$factor)+$cuota_foja;
			}
		}
	}

	$imp['impuesto']= redondear_dos_decimal($imp_predial);
	$imp['bimestral']= redondear_dos_decimal($imp_predial/6);
	$imp['semestral']= redondear_dos_decimal($imp_predial/2);
	return $imp;
}
function Impuesto_trasl($valor_cas,$ano)
{
	$consultas = new CONSULTAS;
	$campos = array("lim_inf","cuota_fija","factor");
	$condiciones=array("limites_trasl.id_limt=cal_imp_trasl.id_lim and ano_curso"=>$ano);

	if($valor_cas >= 2433151){
		$condiciones['lim_inf']=2433151;
	}else{
		$condiciones["($valor_cas BETWEEN lim_inf and lim_sup ) and 1"]=1;
	}
	//echo "$valor_cas >= 2433151\n".$condiciones['lim_inf'];
	if($consultas->SeleccionarTablaFila('cal_imp_trasl, limites_trasl', $campos, $condiciones,'', "id_calculo ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$lim_inf	=$registro["lim_inf"];
			$cuota_foja	=$registro["cuota_fija"];
			$factor		=$registro["factor"];
			$imp_predial=(($valor_cas-$lim_inf)*$factor)+$cuota_foja;
			//echo "$imp_predial=(($valor_cas-$lim_inf)*$factor)+$cuota_foja;";
		}
	}
	$imp['impuesto']= $imp_predial;
	$imp['bimestral']= $imp_predial/6;
	$imp['semestral']= $imp_predial/2;
	return $imp;
}

function redondear_dos_decimal($valor)
{
   $float_redondeado=round($valor, 2);
   return $float_redondeado;
}

function FechaCaptura($fecha)
{
	$fec_d=explode("-",$fecha);
	$dia=$fec_d[2];
	$mes=$fec_d[1];
	$ano=$fec_d[0];

	$_ano=($ano-1988)*372;
	$_mes=($mes-1)*31;
	$_dia=$dia-1;
	$_total=array();
	$_total=$_ano+$_mes+$_dia;
	return $_total;
}



function LineaCaptura($t_p,$ids,$clave,$fecha,$monto)
{
	$line_cap=array();
	$clave=(string)$clave;
	$int = FechaCaptura($fecha);
	$fecha = (string)$int;
	$inicios=(string)"3750$t_p";
	for($j=0;$j<6;$j++)
	{
		$line_cap[$j]=$inicios[$j];
	}
	$id_c=(string)$ids;
        $w="0";
	for($t=6;$t<=24;$t++)
	{

		if($id_c[$w]!="")
		{
			$line_cap[$t]=$id_c[$w];
                $w++;
		}else
		{
			$line_cap[$t]="";
		}
	}
	$monto = number_format($monto, 2);
	$mtn = explode(".",$monto);
	$uni=$mtn[0];
	$dec=$mtn[1];
	$num_monto = array();
	$num_monto = $uni."".$dec;
	$valores=array(7,3,1);
	$importe=array();
	$j=0;
	$k=0;
	for( $i = 15; $i >= 0; $i--)
	{
		if($num_monto[$i]!=""){
		$importe[$k] = $num_monto[$i]*$valores[$j];
		$j++;
		if($j == 3)
		{
			$j=0;
		}
		$k++;
		}
	}
	$m=1;
	for($i=0; $i < $k;$i++)
	{
		$aux=$importe[$m]+$importe[$i];
		$importe[$m]=$aux;
		$m++;
	}

	$factor=$aux%10;
	$o=15;
	for($i=41;$i>=25;$i--)
	{
		$line_cap[$i]=$clave[$o];
		$o--;
	}

	$n=12;
	$p=3;

	for($i=45;$i>41;$i--)
	{
		$line_cap[$i]=$fecha[$p];
		$p--;
	}

	$line_cap[46]=$factor;
	$line_cap[47]=2;

	$tabla_valores=array(	"A"=>"1","B"=>"2","C"=>"3",
				"D"=>"4","E"=>"5","F"=>"6",
				"G"=>"7","H"=>"8","I"=>"9",
				"J"=>"1","K"=>"2","L"=>"3",
				"M"=>"4","N"=>"5","O"=>"6",
				"P"=>"7","Q"=>"8","R"=>"9",
				"S"=>"2","T"=>"3","U"=>"4",
				"V"=>"5","W"=>"6","X"=>"7",
				"Y"=>"8","Z"=>"9"
				);


	$captura=$line_cap;
	foreach($tabla_valores as $key => $value)
	{
		$captura = str_replace($key,$value,$captura);
	}

	$dd=array(11,13,17,19,23);
	$dds=array();
	$p=0;
	for( $i = 47; $i >= 0; $i--)
	{

		if($captura[$i]!='')
		{
			$dds[$i] = $captura[$i]*$dd[$p];
			$p++;
			if($p == 5)
			{
				$p=0;
			}
		}
	}

	$s=1;
	for($i=0; $i < 47;$i++)
	{
		$auxiliar=$dds[$s]+$dds[$i];
		$dds[$s]=$auxiliar;
		$s++;
	}
	$d_d=array();
	$d_d=($auxiliar%97)+1;
	$d_d = (string)$d_d;

	if(strlen($d_d) == "1")
	{
		$line_cap[48]=0;
		$line_cap[49]=$d_d[0];
	}else
	{
		$line_cap[48]=$d_d[0];
		$line_cap[49]=$d_d[1];
	}
	return $line_cap;
}
function SelectConcepto($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("concepto.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("concepto", $campos, "", "nom_concep ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_concep"];
			$nombre = $registro["nom_concep"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function BuscarClave($id_clave)
{
    $consultas = new CONSULTAS;
    $campos = array("id,clave");
    $condicion=array("id"=>$id_clave);
    if($consultas->SeleccionarTablaFila("predial_prueba",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                    $clave = $registro["clave"];
                }
        }
        return $clave;
}
function Ultimo_Dia($mes,$ano)
{
	return strftime("%d", mktime(0, 0, 0, $mes+1, 0, $ano));
}
function Formato_de_numeros($numero)
{
  	$numero=round($numero);
	$numero= number_format($numero, 2, ".","");
	return $numero;
}
function Redondear_2_Dec($valor)
{
	$float_redondeado=round($valor, 2);
	$float_redondeado=number_format($float_redondeado, 2, ".","");
	return $float_redondeado;
}
function BuscarAreas($id_clave)
{
    $consultas = new CONSULTAS;
    $campos = array("nombre_area");
    $condicion=array("id_area"=>$id_clave);
    if($consultas->SeleccionarTablaFila("area",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                    $clave = $registro["nombre_area"];
                }
        }
        return $clave;
}
function BuscarUsuarioDiv($id_usuario)
{
    $consultas = new CONSULTAS;
    $campos = array("nombre","ape_pat","ape_mat","razon");
    $condicion=array("id_cont"=>$id_usuario);
    if($consultas->SeleccionarTablaFila("contribuyente",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                    $reg['nombre'] = $registro["ape_pat"]." ".$registro["ape_mat"]." ".$registro["nombre"];
                    $reg['razon'] = $registro["razon"];
                }
        }
        return $reg;
}
function SelectComerMenu($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("comer_menu.*");
        $condicion=array("status_comer_menu"=> "1");
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("comer_menu", $campos,$condicion, "", "nom_comer_menu ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_comer_menu"];
			$nombre = $registro["nom_comer_menu"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".utf8_encode($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function Salario($ano)
{
	$consultas = new CONSULTAS;
	$campos = array("salario_anual.salario");
	$condiciones=array("ano"=>$ano);

	if($consultas->SeleccionarTablaFila('salario_anual', $campos, $condiciones,'ano', "id_sal_ano ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$salario		=$registro["salario"];
		}
	}
	return $salario;
}
function SaberBimestre()
{
	$dia=date('d');
	$mes=date('m');
	$ano=date('Y');
	$fecha_exp_a  = mktime(0,0,0, $mes,$dia,$ano);
        $mes = date("n",$fecha_exp_a);
	if($mes%2==1)
	{
		$bi_act = ($mes/2)+".5";
		$cob=0;
	}else
	{
		$bi_act =$mes/2;
		$cob=1;
	}
	return $bi_act;

}
function CalcularMulta($impuesto)
{
	$bi_actual = SaberBimestre();
	$total_multa=(($impuesto/6)*$bi_actual);
	return $total_multa;
}
function Salariosss($ano)
{
	$consultas = new CONSULTAS;
	$campos = array("salario_anual.*");
	$condiciones=array("ano"=>$ano);

	if($consultas->SeleccionarTablaFila('salario_anual', $campos, $condiciones,'ano', "id_sal_ano ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$salario	=$registro["salario"];
		}
	}
	return $salario;
}
function SelectNombreClasificacionPago($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;
	$campos = array("nombre_clasif_pago.*");
        $condicion=array("status_nombre_clasif_pago"=> "1");
	if(!empty($_condicion))
	{
		$condicion["seccion"] = $_condicion;
	}
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("nombre_clasif_pago", $campos,$condicion, "", "nombre_clasif_pago ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_nombre_clasif_pago"];
			$nombre = $registro["nombre_clasif_pago"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTFS($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectToma($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;
	if($_condicion=='7')
	{
		$campos = array("t.id_toma,t.toma");
		$condicion=array("t.tipo_toma_id = tt.id_tipo_toma and 1"=> "1");
		$condicion["tt.id_tipo_toma"] = $_condicion;
		$html = "<select name='$id' id='$id' $funcion>";
		$html .= "<option value=''>$caption</option>";
		if($consultas->SeleccionarTablaFila(TIPOTOMA::TABLE_NAME." tt, ".TOMA::TABLE_NAME." t", $campos,$condicion, "", "toma ASC")==CONSULTAS_SUCCESS)
		{
			while($registro =$consultas->Fetch())
			{
				$id = $registro["id_toma"];
				$nombre = $registro["toma"];
				$html .= "<option value='$id' ";
				if($value == $id)
				{
					$html .= "selected='selected'";
				}

				$html .= ">$nombre</option>";
			}
		}
		$html .= "</select>";
	}else{
		$nombre = '';
		$campos = array("t.id_toma,t.toma");
		$condicion=array("t.tipo_toma_id = tt.id_tipo_toma and 1"=> "1");
		$condicion["tt.id_tipo_toma"] = $_condicion;
		$html = "<input type='hidden'  name='$id' id='$id' $funcion ";
		if($consultas->SeleccionarTablaFila(TIPOTOMA::TABLE_NAME." tt, ".TOMA::TABLE_NAME." t", $campos,$condicion, "", "toma ASC")==CONSULTAS_SUCCESS)
		{
			while($registro =$consultas->Fetch())
			{
				$id = $registro["id_toma"];
				$nombre = $registro["toma"];
				$html .= " value='$id' ";
			}
		}
		$html .= " >$nombre";
	}
	return $html;
}
function SelectTipo_toma($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;
	$campos = array("id_tipo_toma,tipo_toma");
        $condicion=array("status_tipo_toma"=> "1");
	if(!empty($_condicion))
	{
		$condicion["gpo_toma"] = $_condicion;
	}
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila(TIPOTOMA::TABLE_NAME, $campos,$condicion, "", "tipo_toma ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_tipo_toma"];
			$nombre = $registro["tipo_toma"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">$nombre</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoComer($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;
	$campos = array("cct.id_cct,cct.nombre_cct");
        $condicion=array("t.tipo_toma_id = tt.id_tipo_toma and cct.tt_id = tt.id_tipo_toma and cct.status_cct"=> "1");
	if(!empty($_condicion))
	{
		$condicion["t.id_toma"] = $_condicion;
	}
	$html =  "";
	if($consultas->SeleccionarTablaFila(TIPOTOMA::TABLE_NAME." tt, ".TOMA::TABLE_NAME." t,".COMERCIO::TABLE_NAME." cct", $campos,$condicion, "", "cct.nombre_cct ASC")==CONSULTAS_SUCCESS)
	{
		if($consultas->NumeroRegistros()>0)
		{
			$html = "<select name='$id' id='$id' $funcion>";
			$html .= "<option value=''>$caption</option>";
			while($registro =$consultas->Fetch())
			{
				$id = $registro["id_cct"];
				$nombre = $registro["nombre_cct"];
				$html .= "<option value='$id' ";
				if($value == $id)
				{
					$html .= "selected='selected'";
				}

				$html .= ">$nombre</option>";
			}
			$html .= "</select>";
		}
	}
	return $html;
}
function SelectClasificacionPago($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;
	$campos = array("vista_class_pago.id_clasif_pago,vista_class_pago.nombre_clasif_pago, vista_class_pago.nombre");
        $condicion=array("seccion"=> "1");
	if(!empty($_condicion))
	{
		$condicion["ano_c"] = $_condicion;
	}
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("vista_class_pago", $campos,$condicion, "", "nombre_clasif_pago ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_clasif_pago"];
			$nombre = $registro["nombre_clasif_pago"];
			$nombre_1 = $registro["nombre"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTFS($nombre." ".$nombre_1)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function multi_in_array($valor, $array)
{
	$result='';
	if(!empty($valor))
	{
		foreach($array as $key => $value)
		{
			if(strstr($value, $valor) !== FALSE)
			{
				$result=$key;
				break;
			}
		}
	}
	return $result;
}
function multi_in_array_especial($valor, $array)
{
	$result='';
	if(!empty($valor))
	{
		foreach($array as $key => $value)
		{
			if(in_array($valor,$value))
			{
				$result=$key;
				break;
			}
		}
	}
	return $result;
}
function SelectGruposBul($id,$funcion,$value,$caption,$_condicion='')
{
	$consultas = new CONSULTAS;
	$campos = array("tipo_calculo.*");
        $condicion=array("estado_tc"=> "1","status_tc"=> "1");
	if(!empty($_condicion))
	{
		$condicion["ano_c"] = $_condicion;
	}
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("tipo_calculo", $campos,$condicion, "", "nombre_tc ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_tipo_calculo"];
			$nombre = $registro["nombre_tc"];

			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".utf8_encode($nombre)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectClasificacionPagoDiv($id,$funcion,$value,$caption,$_condicion)
{
	$consultas = new CONSULTAS;

	$campos = array("vista_cp.id_clasif_pago,vista_cp.nombre_clasif_pago, vista_cp.nombre");
        $condicion=array("vista_cp.`id_clasif_pago` NOT IN
			(SELECT
			  vcp.`id_clasif_pago`
			FROM
			  det_liq_".date('Y')." det,
			  vista_class_pago vcp
			WHERE det.tipo_cuenta = vcp.id_clasif_pago
				AND det.`id_liqp`='12'
			  AND vcp.`seccion`='2'
			  AND det.status_d_l = '1') and vista_cp.seccion"=> "2");
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("vista_class_pago vista_cp ", $campos,$condicion, "", "vista_cp.nombre_clasif_pago ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id = $registro["id_clasif_pago"];
			$nombre = $registro["nombre_clasif_pago"];
			$nombre_1 = $registro["nombre"];
			$html .= "<option value='$id' ";
			if($value == $id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".UTFS($nombre." ".$nombre_1)."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoPredio($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array('status_tipo_predio'=>1);
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("tipo_predio", $campos,$condicion,  "", "id_tipo_predio ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$_id = $registro["id_tipo_predio"];
			$_nom = $registro["nombre_tipo_predio"];
			$html .= "<option value='$_id' ";
			if($value == $_id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$_nom."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectTipoCalculo($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array('estado_tc'=>1,'status_tc'=>'1');
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("tipo_calculo", $campos, $condicion,  "", "id_tipo_calculo ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$_id = $registro["id_tipo_calculo"];
			$_nom = $registro["nombre_tc"];
			$html .= "<option value='$_id' ";
			if($value == $_id)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$_nom."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function TipoPredioTipologia1($ciclo,$ano)
{
	$tipo = "0";
	$consultas = new CONSULTAS;
	$campos = array("tipo_toma_id AS t_id");
	$condicion=array("tc.t_id=t.id_toma and ciclo_id"=>$ciclo,'status_tipo_ciclo'=>'1');
	if($consultas->SeleccionarTablaFila("tipo_ciclo_$ano tc ,tomas t", $campos, $condicion,  "", "id_tipo_ciclo ASC")==CONSULTAS_SUCCESS)
	{
		$arraytt =array();
		while($registro =$consultas->Fetch())
		{
			array_push($arraytt,$registro["t_id"]);
		}
		$tipo = "2";
		if (in_array(1, $arraytt)) {
		   	$tipo = "1";
		}
		if (in_array(7, $arraytt)) {
		   $tipo = "4";
		}
		if(in_array(3, $arraytt) || in_array(4, $arraytt) || in_array(5, $arraytt)) {
		   $tipo = "3";
		}
	}
	return $tipo;
}
function TipoPredio_l()
{
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array("status_tipo_predio"=>'1');
	if($consultas->SeleccionarTablaFila("tipo_predio", $campos, $condicion,  "", "id_tipo_predio ASC")==CONSULTAS_SUCCESS)
	{
		$arraytt =array();
		$arraytt[0] ='NO ES DE NINGUN TIPO';
		while($registro =$consultas->Fetch())
		{
			$arraytt[$registro["id_tipo_predio"]]=$registro["nombre_tipo_predio"];
		}
	}
	return $arraytt;
}
function TipoPagoAgua($id,$tipogrupo)
{
	$html="<br />$grupo<table  border=\"0\" align='center' width=\"100%\"><!--<tr><td colspan='5'  class='Valores_ano' >Valores por A&ntilde;o</td>";
	$html.="</tr>-->";
	$_mes_act=date('m');
	$i='1';
	$total='0';
	$subiva='0';
	$id_ciclo_v=array();
	$var_p_ppp='';
	for($s_ano=date('Y')-6;$s_ano<=date('Y');$s_ano++)
	{
		$MyCiclo = new CICLO;
		$collectiones = $MyCiclo->ColeccionEditar('',$s_ano,'1', '1','ano_ciclo', 'ASC',$id);
		$total_ciclos = $MyCiclo->getTotal();
		if($total_ciclos > 0)
		{
			$iRow='0';
			foreach($collectiones as $collectiones)
			{
				$id_ciclo		= $MyCiclo->getID($collectiones);
				$ano_ciclo		= $MyCiclo->getAno($collectiones);
				$mes_pag		= $MyCiclo->getMes_pag($collectiones);
				$clase_pag		= $MyCiclo->getClase_pag($collectiones);
				$tipo_ciclo		= $MyCiclo->getTipo($collectiones);
				$status_ciclos		= $MyCiclo->getStatus($collectiones);
				$grupo			= $MyCiclo->getGrupo($collectiones);
				$fecha_pago		= $MyCiclo->getFecha_ult($collectiones,'interface');
				//$const 		= $MyCiclo->getConst($collectiones);
				//$ter 			= $MyCiclo->getTer($collectiones);
				//$const_c		= $MyCiclo->getConst_c($collectiones);
				//$ter_c 		= $MyCiclo->getTer_c($collectiones);
				//$valor_cat		= $MyCiclo->getValor($collectiones);
				//$damnificado		= $MyCiclo->getDamnificados($collectiones);
				//$recibo		= $MyCiclo->getRecibo($collectiones);
				if($status_ciclos==1)
				{
					$style_ciclo='style="color: red;"';
					$style_ciclo1='color: red;';
					$var_p_ppp.="$id_ciclo:".$ano_ciclo."|";
					$c_ano="<input type='checkbox' id='ano' name='ano$h' checked='checked' value='$id_ciclo' onclick=\"ValorAno('ano$h','$h');  \">
					<input type='hidden' id='anos' name='anos$h' value='$ano_ciclo' >$ano_ciclo";
				}elseif($status_ciclos==2){
					$style_ciclo='style="color: #000000; font-size: 13px; font-weight:bold;"';
					$style_ciclo1='color: #000000; font-size: 13px; font-weight:bold;';
					$var_p_ppp.="$id_ciclo:".$ano_ciclo."|";
					$c_ano="$ano_ciclo";
				}
				$id_ciclo_v[$i]=$status_ciclos;
				$thisClass  = ((($iRow % 2) == 0) ? "formFieldDk" : "formFieldLt");
				$thisClassC = $thisClass . "C";
				$iRow++;
				$html.="<tr class='THtitle' >
					<td width=\"70px\" >$c_ano</td>
					<td colspan='4'>Ano</td>
					";
				$html.="</tr>";
				$html.="<tr>
					<td colspan='6'>";
				if($status_ciclos==1)
				{
					$MyTipologias = new TIPOLOGIA;
					$coleccion = $MyTipologias->ColeccionLiquidacion('',$s_ano,'20','1','id_tipo_ciclo','ASC',$id_ciclo);
					$html.="<table  border=\"1\" align='center' width=\"100%\">";
					$html.="<tr  class='$thisClass' onmouseover=\"this.className='RowOverTR'\" onmouseout=\"this.className='$thisClass'\" >
							<td width=\"250px\" >Tipo</td>
							<td width=\"50px\">Cantidad</td>
							<td >Importe</td>
							<td >Drenaje</td>
							<td >Recargos</td>
							<td >Actualizacion</td>
							<td >Subsidio Importe</td>
							<td >Subsidio Drenaje</td>
							<td >Subsidio Recargos</td>
							<td >Subsidio Actualizacion</td>
							<td >Subtotal</td>
							</tr>";
					foreach($coleccion as $coleccion)
					{
						$id_tipologia	= $MyTipologias->getID($coleccion);
						$tipo		= $MyTipologias->getTipo($coleccion,'interface');
						$toma		= $MyTipologias->getToma($coleccion,'interface');
						$comercio	= $MyTipologias->getComercio($coleccion,'interface');
						$cantidad	= $MyTipologias->getCantidad($coleccion);
						$sumi		= $MyTipologias->getSuministro($coleccion);
						$sumi_b		= $MyTipologias->getSuministro_bimestral($coleccion);
						$drenaje	= $MyTipologias->getDrenaje($coleccion);
						$_subtotal	= $MyTipologias->getSubtotal($coleccion);
						$iva		= $MyTipologias->getIva($coleccion);
						$MyDescuentos = new DESCUENTOS;
						$col_desc	= $MyDescuentos->Coleccion('', '1', '1', "id_por_mes", "asc",$s_ano,(int)$_mes_act,$tipo_ciclo,$tipogrupo);
						$arrayactualizacion= Actualizacion($s_ano,$sumi_b);
						$arrayrecargos	= Recargos($s_ano,$sumi_b,$arrayactualizacion);
						$actualizacion	= array_sum($arrayactualizacion);
						$recargos 	= array_sum($arrayrecargos);
						$col_desc	= $col_desc[0];
						$id_descuentos	= $MyDescuentos->getID($col_desc);
						$_mes		= $MyDescuentos->getMes($col_desc);
						$_anos	 	= $MyDescuentos->getAno($col_desc);
						$descuento_imp	= $MyDescuentos->getDescuento_importe($col_desc);
						$descuento_dre	= $MyDescuentos->getDescuento_drenaje($col_desc);
						$descuento_rec	= $MyDescuentos->getDescuento_recargos($col_desc);
						$descuento_act	= $MyDescuentos->getDescuento_actalizacion($col_desc);
						//$descuento_iva= $MyDescuentos->getDescuento_iva($col_desc);
						$desc_imp=redondear_dos_decimal((($descuento_imp*$sumi)/100));
						$desc_dre=redondear_dos_decimal((($descuento_dre*$drenaje)/100));
						$desc_rec=redondear_dos_decimal((($descuento_rec*$recargos)/100));
						$desc_act=redondear_dos_decimal((($descuento_act*$actualizacion)/100));
						$subtotal=($sumi+$drenaje+$recargos+$actualizacion)-($desc_imp+$desc_dre+$desc_rec+$desc_act);
						$total=$subtotal+$total;
						$subiva=$subiva+$iva;
						$html.="<tr >
							<td  >$tipo</td>
							<td  >$cantidad</td>
							<td  >$sumi</td>
							<td  >$drenaje</td>";
						$html.="<td >$recargos</td>
							<td >$actualizacion</td>
							<td >$desc_imp</td>
							<td >$desc_dre</td>
							<td >$desc_rec</td>
							<td >$desc_act</td>
							<td >$subtotal<input type='hidden' id='total_par$h' name='total_par$h' value='$subtotal' ></td>";
						$html.="</tr>";
					}

					$html.="</table>";
				}elseif($status_ciclos==2){
					$h=0;
					$html.="<table  border=\"0\" align='center' width=\"100%\">
						<tr   style='color: #6A6C63;' >
						<td colspan='3'>ya ha sido cubierto</td><td colspan='2'>";
					$html.="</td></tr>";
					$html.="</table>";
				}
				$html.="</td></tr>";
				$h++;
				$i++;
			}
		}else{
			$h=0;
			$html.="<tr class='THtitle'  >
				<td >A&ntilde;o</td>
				<td >Meses pagados</td>
				<td >Fecha Pago</td>
				<td >Editar</td>
				<td >Recibo</td>
				<td ></td>";
			$html.="</tr>";
			$html.="<tr >
				<td colspan='6'><div class='MsgErr' >No se acargado ningun cobro en este ano</div></td>
				<td >";
			$html.="</td>";
			$html.="</tr>";
		}
	}
	$html.="<tr  >
		<td ></td>
		<td ></td>
		<td ></td>
		<td class='THtitle'  width=\"100px\">Sub Total</td>
		<td   width=\"100px\" class=\"dato\" style='text-align:right;' >$total</td>";
	$html.="</tr>";
	$html.="<tr  >
		<td ></td>
		<td ></td>
		<td ></td>
		<td class='THtitle'  width=\"100px\">Iva Total</td>
		<td   width=\"100px\" class=\"dato\" style='text-align:right;' >$subiva</td>";
	$html.="</tr>";
	$html.="<tr  >
		<td ></td>
		<td ></td>
		<td ></td>
		<td class='THtitle'  width=\"100px\">Total</td>
		<td   width=\"100px\" class=\"dato\" style='text-align:right;' >".($total+$subiva)."</td>";
	$html.="</tr>";
	$html.="</table>";
	$html.="<input type='hidden' name='total' id='total' value='$h'>";
	return $html;
}
function SelectTipodeposito($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("tipo_desposito.*");

	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTodo("tipo_desposito", $campos, "", "n_tipo_dep ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_tipo_dep"];
			$nombre = $registro["n_tipo_dep"];
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
function SelectEdoMun($id,$funcion,$value,$caption)
{
	$consultas = new CONSULTAS;
	$campos = array("*");
	$condicion=array('muni.id_edo=edos.id_edos and 1'=>1);
	$html = "<select name='$id' id='$id' $funcion>";
	$html .= "<option value=''>$caption</option>";
	if($consultas->SeleccionarTablaFila("muni,edos", $campos,$condicion,  "", "nom_edo ASC")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
			$id_area = $registro["id_mun"];
			$nombre = UTF(UTFS($registro["nom_edo"].",".$registro["nom_mun"]));
			$html .= "<option value='$id_area' ";
			if($value == $id_area)
			{
				$html .= "selected='selected'";
			}

			$html .= ">".$nombre."</option>";
		}
	}
	$html .= "</select>";
	return $html;
}
?>
