<?php
	$_SESSION['URL_REDIREC']	= '';
	$_SESSION['URL_REDIREC_COMMAND']= '';
	if($MySPANEL->ID()!="")
	{
		print ("<script>window.location='/".HOME."'</script>");
		exit;
	}
	if(LOGIN!=$command){
		if(isset($_SERVER['REQUEST_URI'])){
			$_SESSION['URL_REDIREC']	= $_SERVER['REQUEST_URI'];
		}
		$_SESSION['URL_REDIREC_COMMAND']= $command;
	}
?>
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="img/logo1.png" alt="Odapas"> </span>
			</div>
		</header>
		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				<?php include_once("getcookiedata.php"); ?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<div class="well no-padding">
							<form action="php/login.php" id="loginform" name="loginform" method="post" class="smart-form client-form">

								<header>
									Ingresar
								</header>

								<fieldset>

									<section>
										<label class="label">Usuario</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
										<input type="text" name="username" id="username" value=""  >
										</label>
									</section>

									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" id="password" value=""  >
										</label>

									</section>
									<?php
									if(isset($_SESSION['intentos']) && $_SESSION['intentos']>=3)
									{
									?>
									<section>
										<label class="label"><img src="php/class/class.captcha.nuevo.php" id="captcha" alt="captcha" /></label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="text" name="captcha" id="captchaform"  />
											<b class="tooltip tooltip-top-right">
											<i class="fa fa-lock txt-color-teal"></i>
											Introduce la imagen</b>
											</label>

									</section>
									<?php
									}
									?>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Entrar
									</button>
								</footer>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
