<?php
$mMenu = array(
				"Usuarios:usuarios.png"						=> LISTA_MIEMBROS,
);

ksort($mMenu);
?>
<div class='row'>
<?php
$i = 0;
foreach($mMenu as $key => $value)
{
	list($caption, $img)	= explode(":",$key);
	if ($MySPANEL->AccesoPermitido($uiCommand[$value][0],$uiCommand[$value][1], $MySPANEL->USUARIO()) != ACCESS_DENIED)
 	{
    		print ("<div class='col-xs-3'><a href=\"");
		if(substr($value,0,7) == "http://")
		{
			print $value;
		}
		else
		{
			print ($MyLogic->base_url()."index.php?command=".$value);
		}
		print ("\">");
    		print makeHTMLImg("imags/$img", "80", "80","$caption");
		print ("<br />$caption</a></div>\n");
		$i++;
	}
}
?>
</div>
