<?php
if(isset($_SESSION["cookie_http_vars"]) && !empty($_SESSION["cookie_http_vars"]))
{
	foreach($_SESSION["cookie_http_vars"] as $key => $value)
	{
		$FORM[$key] = $value;
	}
	if(MSG_ALERT == "HTML")
	{	
	?>
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-sm-12">
	<?php
		if(isset($FORM['MsgErr']))
			print ('<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i><strong>Error!</strong> '.$FORM['MsgErr'].'</div>');
		if(isset($FORM['MsgSas']))
			print ('<div class="alert alert-success fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-check"></i><strong>Success</strong> '.$FORM['MsgSas'].'</div>');
		if(isset($FORM['MsgWar']))
			print ('<div class="alert alert-warning fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-warning"></i><strong>Warning</strong> '.$FORM['MsgWar'].'</div>');
	?>
			</article>
			<!-- WIDGET END -->
		</div>
	<?php
	}
	elseif(MSG_ALERT == "JAVASCRIPT")
	{
		if(isset($FORM['MsgErr']))		
			print ("<script>alert('".$FORM['MsgErr']."');</script>");
		if(isset($FORM['MsgSas']))		
			print ("<script>alert('".$FORM['MsgSas']."');</script>");
	}
	
	$_SESSION["cookie_http_vars"]=array(); 

}
?>
