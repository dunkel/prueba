<?php
$MyDebug->DebugInclude("php/class/class.usuarios.php");
include_once("php/class/class.usuarios.php");

$id		= isset($CONTEXT['id'])		? Sanitizacion($CONTEXT['id']) 		: "";
$usuario 	= isset($FORM['usuario'])	? Sanitizacion($FORM['usuario'])	: "";
$email	 	= isset($FORM['email'])		? Sanitizacion($FORM['email'])		: "";
$nivel	 	= isset($FORM['nivel'])		? Sanitizacion($FORM['nivel'])		: "";
$nombre		= '';
$ape_pat	= '';
$ape_mat	= '';
$cp		= '';
$estado		= '';
$municipio	= '';
$asentamiento	= '';
$calle		= '';
$numero		= '';
if(!empty($id))
{

	$collection = $MyUser->Coleccion($id);
	$collection = $collection[0];

	$id			= $MyUser->getId($collection);
	$usuario		= $MyUser->getUsername($collection);
	$email			= $MyUser->getEmail($collection);
	$nombre			= $MyUser->getNombre($collection);
	$nivel	 		= $MyUser->getNivel($collection,'sql');
	$ape_pat		= $MyUser->getApe_pat($collection);
	$ape_mat		= $MyUser->getApe_mat($collection);
	$municipio		= $MyUser->getMunicipio($collection);
	$estado			= $MyUser->getEstado($collection);
	$asentamiento		= $MyUser->getAsentamiento($collection);
	$calle			= $MyUser->getCalle($collection);
	$numero			= $MyUser->getNumero($collection);
	$cp			= $MyUser->getCp($collection);
	
	
	
	
	if ($MySPANEL->AccesoPermitido($uiCommand[FORM_OPERADOR_EDIT][0],$uiCommand[FORM_OPERADOR_EDIT][1], $MySPANEL->USUARIO()) == ACCESS_DENIED)
	{
		print ("<script>window.location='?command=".HOME."'</script>");
		exit;
	}
}

?>
<!-- widget grid -->
<section id="widget-grid" class="">
	<!-- START ROW -->
	<div class="row">
	<article class="col-sm-12 col-md-12 col-lg-2">
	</article>
	<article class="col-sm-12 col-md-12 col-lg-8">
	<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-users" data-widget-editbutton="false" data-widget-custombutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

					data-widget-colorbutton="false"
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true"
					data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Formulario</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form name='users' id='users' method='post' action="php/users/submit.users.php"  enctype='multipart/form-data' class="smart-form" >
						<input type="hidden" name="id" id="id" value="<?php print $id; ?>" >
							<fieldset>
								<section >
									<label class="label">Usuarios</label>
									<label class="input">
										<i class="icon-append fa fa-user"></i>
										<input type="text" id='usuario' name='usuario'  maxlength='30' value="<?php echo $usuario;?>" <?php	if(!empty($id)){   ?> disabled='disabled' <?php	} ?> placeholder="Usuario" >
									</label>
								</section>
								<section >
									<label class="label">Nombre</label>
									<label class="input">
										<input type="text" id='nombre' name='nombre'  maxlength='30' value="<?php echo $nombre;?>" placeholder="Nombre" >
									</label>
								</section>
								<section>
									<label class="label">Apellido paterno</label>
									<label class="input">
										<input type="text" id='ape_pat' name='ape_pat'  maxlength='40' value="<?php echo $ape_pat;?>" placeholder="Apellido paterno" >
									</label>
								</section>
								<section>
									<label class="label">Apellido materno</label>
									<label class="input">
										<input type="text" id='ape_mat' name='ape_mat'  maxlength='30' value="<?php echo $ape_mat;?>" placeholder="Apellido materno"  >
									</label>
								</section>
								<section>
									<label class="label">E-mail</label>
									<label class="input">
										<input type="email" id='email' name='email'  maxlength='50' value="<?php print $email; ?>">
									</label>
								</section>
								<section>
									<label class="label">C.P.</label>
									<label class="input">
										<input type="text" id='cp' name='cp'  maxlength='6' value="<?php echo $cp;?>" placeholder="C.P."  >
									</label>
								</section>
								<section>
									<label class="label">Estado</label>
									<label class="input">
										<input type="text" id='estado' name='estado'  maxlength='250' value="<?php echo $estado;?>" placeholder="Estado"  >
									</label>
								</section>
								<section>
									<label class="label">Muncipio</label>
									<label class="input">
										<input type="text" id='municipio' name='municipio'  maxlength='250' value="<?php echo $municipio;?>" placeholder="Muncipio"  >
									</label>
								</section>
								<section>
									<label class="label">Asentamiento</label>
									<label class="input">
										<input type="text" id='asentamiento' name='asentamiento'  maxlength='250' value="<?php echo $asentamiento;?>" placeholder="Asentamiento"  >
									</label>
								</section>
								<section>
									<label class="label">Calle</label>
									<label class="input">
										<input type="text" id='calle' name='calle'  maxlength='250' value="<?php echo $calle;?>" placeholder="Calle"  >
									</label>
								</section>
								<section>
									<label class="label">N&uacute;mero</label>
									<label class="input">
										<input type="text" id='numero' name='numero'  maxlength='250' value="<?php echo $numero;?>" placeholder="numero"  >
									</label>
								</section>
							<?php
								if(!empty($id))
								{
								?>
								<section>
									<label class="label">Contrase&ntilde;a</label>
									<label class="input">
										<input type="password" id='contrasena' name='contrasena'  maxlength='20' value="" placeholder="Contrase&ntilde;a" >
									</label>
								</section>
								<section>
									<label class="label">Confirmar contrase&ntilde;a</label>
									<label class="input">
										<input type="password" id='contrasena1' name='contrasena1'  maxlength='20' value="" placeholder="Confirmar Contrase&ntilde;a" >
									</label>
								</section>
								<?php
								}
								else
								{
									print ("<input type='hidden' name='contrasena' id='contrasena' value='' />");
									print ("<input type='hidden' name='contrasena1' id='contrasena1' value='' />");
								}
								?>
								<?php
	if ($MySPANEL->AccesoPermitido($uiCommand[NIVEL_OPER][0],'', $MySPANEL->USUARIO()) != ACCESS_DENIED)
 	{
?>
								<section>
									<label class="label">Nivel</label>
									<label class="select">
		<select id='nivel' name='nivel' >
		<option value=''>Elige...</option>
		<?php
		foreach($_NIVELUSER as $key => $value)
		{
			print ("<option value='$value' ");
			if($nivel == $value)
			{
				print ("selected");
			}
			print (">$key</option>");
		}
		?>
		</select>
									</label>
								</section>
<?php
	}else{
?>
	<section>
		<label class="label">Nivel</label>
		<label class="select"><?php $nivel;?></label>
	</section>
<?php
		print ("<input type='hidden' name='nivel' id='nivel' value='$nivel' />");
	}
?>
			</fieldset>

							<footer>
								<button type="submit" class="btn btn-primary">Guardar</button>
								<button type="reset" class="btn btn-default">Cancelar</button>
							</footer>

							<div class="message">
								<i class="fa fa-thumbs-up"></i>
								<p>Your message was successfully sent!</p>
							</div>
						</form>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->
	</article>
	<article class="col-sm-12 col-md-12 col-lg-2">
	</article>
	</div>
</section>
