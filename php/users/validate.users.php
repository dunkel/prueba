<?php
session_start();
include_once('../class/class.debug.php');
$MyDebug->SetDebug(0);
$MyDebug->DebugError();
include_once('../util.php');
include_once('../utiles.php');
include_once('../class/class.consultas.php');
include_once('../class/class.logic.panel.php');
include_once("../class/class.correo.php");
include_once("../class/class.usuarios.php");
include_once("../class/class.log.db.php");
include_once("../class/class.uploader.php");

$username = isset($CONTEXT["usuario"])			? Sanitizacion($CONTEXT["usuario"])		: "";

if(!empty($username))
{
	$MyUser = new USERS;
	$MyUser->setUsername($username);
	if($MyUser->ExistUser() == USERS_SUCCESS)
	{
		echo "false";
	}
	else
	{
		echo "true";
	}
}else
{
	echo "false";
}

function ComprovarUser($user)
{
	if (strlen($user)<6 || strlen($user)>20)
	{
    		$msg = "false";
   	}
   	else{
   		$permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.";
   		
		$msg = "true";
		
		for ($i=0; $i<strlen($user); $i++)
		{
      			if (strpos($permitidos, substr($user,$i,1)) === false)
			{
	    			$msg = "false";
      			}	
   		}	
	}
   	return $msg;
}
$MyDebug->Dump();
?>

