<!-- widget grid -->
<section id="widget-grid" class="">
	<!-- START ROW -->
	<div class="row">
	<article class="col-sm-12 col-md-12 col-lg-12">
	<div class="jarviswidget" id="wid-id-usuarios" data-widget-colorbutton="false" data-widget-editbutton="false">
		<header>
			<h2><strong>Lista</strong> <i>Usuarios</i></h2>				
			
		</header>

		<!-- widget div-->
		<div>
			
			<!-- widget edit box -->
			<div class="jarviswidget-editbox">
				<!-- This area used as dropdown edit box -->
				<input class="form-control" type="text">
				<span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>
				
			</div>
			<!-- end widget edit box -->
			
			<!-- widget content -->
			<div class="widget-body no-padding">
				<div class="widget-body-toolbar">
					
					<div class="row">
						
						<div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
							<div class="input-group">

							</div>
						</div>
						<div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
<?php
if ($MySPANEL->AccesoPermitido($uiCommand[FORM_OPERADOR][0],$uiCommand[FORM_OPERADOR][1], $MySPANEL->Usuario()) != ACCESS_DENIED) 
{
?>
							<a class="btn btn-success" href="<?php print $MyLogic->base_url()."index.php?command=".FORM_OPERADOR;?>" >
								<i class="fa fa-plus"></i> <span class="hidden-mobile hidden-tablet">Nuevo</span>
							</a>
<?php
}
?>
						</div>
					</div>
				</div>
				<div class="table-responsive">
										
					<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
						<thead>
							<tr>
								<th class="hasinput" style="width:17%">
									<input type="text" class="form-control" placeholder="Filtrar Usuario" />
								</th>
								<th class="hasinput" style="width:18%">
									
								</th>
								<th class="hasinput" style="width:18%">
									<input type="text" class="form-control" placeholder="Filtrar Nombre" />
								</th>
								<th class="hasinput" style="width:16%">
									<?php 
										echo NIVELUSERS('nivel_b',' class="form-control" ','','Nivel..');
									?>
								</th>
								<th class="hasinput" colspan='2' > </th>
							</tr>
							<tr>
								<th data-hide="phone">Usuario</th>
								<th>Correo electronico</th>
								<th data-class="expand">Nombre</th>
								<th data-class="expand">Nivel</th>
								<th data-hide="phone,tablet">Elimnar</th>
								<th data-hide="phone,tablet">Editar</th>
							</tr>
						</thead>
					</table>
					
				</div>
				
			</div>
			<!-- end widget content -->
			
		</div>
		<!-- end widget div -->					
	</article>
	</div>
</section>
<form name='formid' id='formid' action="" method='post'>
<input type='hidden' name='id' id='id' value="">
<input type='hidden' name='command' id='command' value="">
</form>
