<?php
session_start();
include_once('../class/class.debug.php');
$MyDebug->SetDebug(0);
$MyDebug->DebugError();
include_once('../util.php');
include_once('../utiles.php');
include_once('../class/class.consultas.php');
include_once('../class/class.logic.panel.php');
include_once("../class/class.correo.php");
include_once("../class/class.usuarios.php");
include_once("../class/class.log.db.php");
include_once("../class/class.uploader.php");

$dpage		= isset($CONTEXT['iDisplayStart'])	? 	(int) Sanitizacion($CONTEXT['iDisplayStart']) 	: "1";
$by_n		= isset($CONTEXT['iSortCol_0'])		?	(int) Sanitizacion($CONTEXT['iSortCol_0']) 		: "0";
$by		= isset($CONTEXT["mDataProp_$by_n"])	? 	Sanitizacion($CONTEXT["mDataProp_$by_n"]) 	: "usuario";
$order		= isset($CONTEXT['sSortDir_0'])		? 	Sanitizacion($CONTEXT['sSortDir_0']) 		: "ASC";
$tampag		= isset($CONTEXT['iDisplayLength'])	? 	(int) Sanitizacion($CONTEXT['iDisplayLength']) 	: "10";
$nombre_b	= isset($CONTEXT['sSearch_0'])		? 	Sanitizacion($CONTEXT['sSearch_0'])		: "";
$usuario_b	= isset($CONTEXT['sSearch_1'])		? 	Sanitizacion($CONTEXT['sSearch_1'])		: "";
$nivel_b	= isset($CONTEXT['sSearch_2'])		? 	Sanitizacion($CONTEXT['sSearch_2'])		: "";
$area_b		= isset($CONTEXT['sSearch_3'])		? 	Sanitizacion($CONTEXT['sSearch_3'])		: "";
$email_b	= isset($CONTEXT['sSearch_4'])		? 	Sanitizacion($CONTEXT['sSearch_4'])		: "";
$sEcho		= isset($CONTEXT['sEcho'])		? 	Sanitizacion($CONTEXT['sEcho']) 		: "0";
$page = ($dpage/$tampag)?($dpage/$tampag)+1:1;
$output = array("sEcho" => intval($sEcho),"iTotalRecords" => 0,"iTotalDisplayRecords" => 0,"aaData" => array());
if ($MySPANEL->AccesoPermitido($uiCommand[LISTA_MIEMBROS][0],$uiCommand[LISTA_MIEMBROS][1], $MySPANEL->Usuario()) != ACCESS_DENIED)
{
	$collection	= $MyUser->Coleccion('', $tampag, $page, $by, $order, $nombre_b, $usuario_b, $nivel_b, $email_b);
	$total		= $MyUser->getTotal();
	if($total > 0)
	{
		$row=array();
		$iRow = 0;

		foreach($collection as $collection)
		{
			$id			= $MyUser->getId($collection);
			$username		= $MyUser->getUsername($collection);
			$nombre			= $MyUser->getNombre($collection);
			$apt_p			= $MyUser->getApe_pat($collection);
			$apt_m			= $MyUser->getApe_mat($collection);
			$email			= $MyUser->getEmail($collection);
			$nivel	 		= $MyUser->getNivel($collection,'interface');

			$row["DT_RowId"]	= "row-$id";
			$row['usuario']	= $username;
			$row['mail']		= $email;
			$row['apellido_pat']	= "$apt_p $apt_m $nombre";
			$row['nivel']		= $nivel;
			$eliminar='';
			if ($MySPANEL->AccesoPermitido($uiCommand[DELETE_OPERADOR][0],$uiCommand[DELETE_OPERADOR][1], $MySPANEL->Usuario()) != ACCESS_DENIED)
	 		{
				if($MySPANEL->ID() != $id)
				{
					$eliminar ="<a class='btn btn-xs btn-danger' href=\"Javascript:ConfEliminarUser('$id');\" >";
					$eliminar.="<i class='glyphicon glyphicon-trash'></i></a>";
				}
			}
			$row['eliminar']	=$eliminar;
			$editar='';
			if ($MySPANEL->AccesoPermitido($uiCommand[FORM_OPERADOR_EDIT][0],$uiCommand[FORM_OPERADOR_EDIT][1], $MySPANEL->Usuario()) != ACCESS_DENIED)
			{
				$editar="<a class='btn btn-xs btn-primary' href=\"javascript:SubmitID('$id', '".FORM_OPERADOR_EDIT."');\">";
				$editar.="<i class='fa fa-pencil'></i></a>";
			}

			$row['editar']		=$editar;
			$iRow++;
			array_push($output['aaData'],$row);
		}
		$output['iTotalDisplayRecords']=$total;
		$MyUser->Coleccion('', $tampag, $page, $by, $order);
		$output['iTotalRecords']=$MyUser->getTotal();
	}
}
$MyDebug->Dump();
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');
echo json_encode($output);
?>
