<?php
session_start();
include_once('../class/class.debug.php');
$MyDebug->SetDebug(0);
$MyDebug->DebugError();
include_once('../util.php');
include_once('../utiles.php');
include_once('../class/class.consultas.php');
include_once('../class/class.logic.panel.php');
include_once("../class/class.correo.php");
include_once("../class/class.usuarios.php");
include_once("../class/class.log.db.php");
include_once("../class/class.uploader.php");

$id			= isset($CONTEXT["id"])			? Sanitizacion($CONTEXT["id"])		: "";
$contrasena		= isset($CONTEXT["contrasena"])		? Sanitizacion($CONTEXT["contrasena"])	: "";
$usuario		= isset($CONTEXT["usuario"])		? Sanitizacion($CONTEXT["usuario"])	: "";
$email			= isset($CONTEXT["email"])		? Sanitizacion($CONTEXT["email"])	: "";
$nivel			= isset($CONTEXT["nivel"])		? Sanitizacion($CONTEXT["nivel"])	: "1";
$nombre			= isset($CONTEXT["nombre"])		? Sanitizacion(Mayusculas($CONTEXT["nombre"]))	: "";
$ape_pat		= isset($CONTEXT["ape_pat"])		? Sanitizacion(Mayusculas($CONTEXT["ape_pat"]))	: "";
$ape_mat		= isset($CONTEXT["ape_mat"])		? Sanitizacion(Mayusculas($CONTEXT["ape_mat"]))	: "";
$cp			= isset($CONTEXT["cp"])			? Sanitizacion($CONTEXT["cp"])		: "";
$estado			= isset($CONTEXT["estado"])		? Sanitizacion($CONTEXT["estado"])	: "";
$municipio		= isset($CONTEXT["municipio"])		? Sanitizacion($CONTEXT["municipio"])	: "";
$asentamiento		= isset($CONTEXT["asentamiento"])	? Sanitizacion($CONTEXT["asentamiento"]): "";
$calle			= isset($CONTEXT["calle"])		? Sanitizacion($CONTEXT["calle"])	: "";
$numero			= isset($CONTEXT["numero"])		? Sanitizacion($CONTEXT["numero"])	: "";

$MyUser->setUsername($usuario);
$MyUser->setEmail($email);
$MyUser->setNombre($nombre);
$MyUser->setNivel($nivel);
$MyUser->setApe_pat($ape_pat);
$MyUser->setApe_mat($ape_mat);
$MyUser->setCp($cp);
$MyUser->setEstado($estado);
$MyUser->setMunicipio($municipio);
$MyUser->setAsentamiento($asentamiento);
$MyUser->setCalle($calle);
$MyUser->setNumero($numero);

if(empty($id))
{
	if ($MySPANEL->AccesoPermitido($uiCommand[FORM_OPERADOR][0],$uiCommand[FORM_OPERADOR][1], $MySPANEL->Usuario()) == ACCESS_DENIED)
	{
		$location= "../".LISTA_MIEMBROS;
		$http_vars["MsgErr"] = "No tiene los sufucientes permisos para editar este usuario";
	}
	else
	{
		if($MyUser->ExistUser()	== USERS_SUCCESS)
		{
			$http_vars["MsgErr"] = "El nombre de usuario ya existe";
			$location = $MyLogic->base_url()."index.php?command=".FORM_OPERADOR;
		}
		else
		{
			if ($MySPANEL->AccesoPermitido($uiCommand[NIVEL_OPER][0],'', $MySPANEL->Usuario()) == ACCESS_DENIED)
			{
				$nivel = $MySPANEL->Nivel();
			}
			$result = $MyUser->GuardaUsuario();

			if($result == USERS_VACIO)
			{
				$http_vars["MsgErr"] = "Favor de llenar todos los campos obligatorios";
				$location = $MyLogic->base_url()."index.php?command=".FORM_OPERADOR;
			}
			elseif($result == USERS_SUCCESS)
			{
				$http_vars["MsgSas"] = "El usuario [$usuario] se agrego correctamente";
				$location = $MyLogic->base_url()."index.php?command=".LISTA_MIEMBROS;
			}
			else
			{
				$http_vars["MsgErr"] = "Error al guardar el usuario [$usuario]";
				$location = $MyLogic->base_url()."index.php?command=".FORM_OPERADOR;
			}
		}
	}
}
else
{

	if ($MySPANEL->AccesoPermitido($uiCommand[FORM_OPERADOR_EDIT][0],$uiCommand[FORM_OPERADOR_EDIT][1],  $MySPANEL->Usuario()) == ACCESS_DENIED)
	{
		$location= "../index.php?command=".LISTA_MIEMBROS;
		$http_vars["MsgErr"] = "No tiene los sufucientes permisos para editar este usuario";
	}
	else
	{
		if ($MySPANEL->AccesoPermitido($uiCommand[NIVEL_OPER][0],'', $MySPANEL->Usuario()) == ACCESS_DENIED)
		{
			$nivel = $MySPANEL->Nivel();
		}
		echo "<pre>";
		$MyUser->setId($id);
		var_dump($MyUser);
		$result = $MyUser->ActualizaUsuario($contrasena);
		if($result == USERS_VACIO)
		{
			$http_vars["MsgErr"] = "Favor de llenar todos los campos obligatorios";
			$location = $MyLogic->base_url()."index.php?command=".FORM_OPERADOR_EDIT."&id=$id";
		}
		elseif($result == USERS_SUCCESS)
		{
			$http_vars["MsgSas"] = "El usuario se actualizo correctamente";
			$location = $MyLogic->base_url()."index.php?command=".LISTA_MIEMBROS;
		}
		else
		{
			$http_vars["MsgErr"] = "Error al actualizar el usuario";
			$location = $MyLogic->base_url()."index.php?command=".FORM_OPERADOR_EDIT."&id=$id";
		}
	}
}

$_SESSION["cookie_http_vars"] = $http_vars;
$MyDebug->Dump();
header("HTTP/1.1 302 Moved Temporarily");
header("Location: $location");
?>
