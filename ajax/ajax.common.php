<?php
require ('xajax/xajax.inc.php');
$xajax = new xajax(); 
$xajax->setCharEncoding('UTF-8');
$xajax->decodeUTF8InputOn();

function SelectArt($id,$m,$a)
{
	global $_Months;
	$respuesta = new xajaxResponse('UTF-8');	
	$consultas = new CONSULTAS;
	$html.="<a href='javascript:void(0);' onclick=\"xajax_SelectArt('$id','$m','$a')\">".$_Months[$m]." de ".$a."</a><br /><br />";
	$campos = array("editorial.*","titulo","friendly","Month(fecha.fecha) as mes","Day(fecha.fecha) as dia", "Year(fecha.fecha) as ano","AresTestAuthTable_Temp.ID");
			
	$condiciones = array("estado" => "1","editorial.usuario"=>$id,"Month(fecha.fecha)"=> $m,"Year(fecha.fecha)"=> $a);
		
	$inner = array("editorial:AresTestAuthTable_Temp" => "editorial.usuario=AresTestAuthTable_Temp.ID",
			":fecha" => "editorial.id_fecha=fecha.id"
			);

	if($consultas->QueryINNERJOIN($inner, $campos, $condiciones, "","dia DESC") == CONSULTAS_SUCCESS)
	{
		while($registro = $consultas->Fetch())
			{
				$titulo = $registro["titulo"];
				$friendly= $registro["friendly"];
				$html.="<dd><a href='/muchasmadres/index.php?local=blog&id=".$friendly."'>".$titulo."</a></dd><br />";
				$_SESSION["username_activo"] = $id;
				$_SESSION["fecha_activa_m"] = $m;
				$_SESSION["fecha_activa_a"] = $a;
			}
	}
	else
	{
		$html.= "no hay resultados";
	}

	$respuesta->addAssign("articulo$id-$m-$a","innerHTML",$html);
	return $respuesta;
}
function SelecionarTramite($id,$div,$div1,$div2,$nom,$cantidad,$predio)
{
	$respuesta = new xajaxResponse('UTF-8');
	$MyTramLiq = new TRAMLIQ;
	
	$collection = $MyTramLiq->Coleccion($id);
	if($collection != TRAL_ERROR)
	{
		$id		= $MyTramLiq->getID($collection[0]);
		$fundamento	= $MyTramLiq->getFundamento($collection[0]);
		$cuenta		= $MyTramLiq->getCuenta($collection[0]);
		$costo		= redondear_dos_decimal($MyTramLiq->getCosto($collection[0]));
		$costo = $costo*$cantidad;
		$html  ="$cuenta";
		$html1 ="$fundamento";
		$html2 ="<table border='1' align='center' width='100%'>
		<tr>
			<th class='THtitle'>Importe</th>
			<th class='THtitle'>Actulaizaci&oacute;n</th>
			<th class='THtitle'>Recargos</th>
			<th class='THtitle'>Multas</th>
			<th class='THtitle'>Gastos Ejec.</th>
			<th class='THtitle'>Sub. Accesosrios</th>
			<th class='THtitle'>Sub. Otros</th>
		</tr>
		<tr>
			<td><input type='text' id='importe$nom' name='importe$nom' value='$costo' size='8'></td>
			<td><input type='text' id='actualizacion$nom' name='actualizacion$nom' value='0' size='8'></td>
			<td><input type='text' id='recargos$nom' name='recargos$nom' value='0' size='8'></td>
			<td><input type='text' id='multas$nom' name='multas$nom' value='0' size='8'></td>
			<td><input type='text' id='gastos$nom' name='gastos$nom' value='0' size='8'></td>
			<td><input type='text' id='subsidios$nom' name='subsidios$nom' value='0' size='8'></td>
			<td><input type='text' id='otros_subsidios$nom' name='otros_subsidios$nom' value='0' size='8'></td>
		</tr>";

		$html2 .="
		</table>";
	}else
	{
		$html  ="No existe";
		$html1 ="";
		$html2 ="<table border='0' align='center' width='100%'>
		<tr>
			<th class='THtitle'>Importe</th>
			<th class='THtitle'>Actulaizaci&oacute;n</th>
			<th class='THtitle'>Recargos</th>
			<th class='THtitle'>Multas</th>
			<th class='THtitle'>Gastos Ejec.</th>
			<th class='THtitle'>Sub. Accesosrios</th>
			<th class='THtitle'>Sub. Otros</th>
		<tr>
		<tr>
			<td><input type='hidden' id='importe$nom' name='importe$nom' value='0.00'></td>
			<td><input type='hidden' id='actualizacion$nom' name='actualizacion$nom' value='0.00'></td>
			<td><input type='hidden' id='recargos$nom' name='recargos$nom' value='0.00'></td>
			<td><input type='hidden' id='multas$nom' name='multas$nom' value='0.00'></td>
			<td><input type='hidden' id='gastos$nom' name='gastos$nom' value='0.00'></td>
			<td><input type='hidden' id='subsidios$nom' name='subsidios$nom' value='0.00'></td>
			<td><input type='hidden' id='otros_subsidios$nom' name='otros_subsidios$nom' value='0.00'></td>
		</table>";
	}
    	$respuesta->addAssign("$div", "innerHTML", $html);
    	$respuesta->addAssign("$div1", "innerHTML", $html1);
    	$respuesta->addAssign("$div2", "innerHTML", $html2);
	return $respuesta;
}
function NuevoPropietario($id_predio,$razon,$nom,$ape_pat,$ape_mat,$folio_td)
{
	$respuesta = new xajaxResponse('UTF-8');
	global $http_vars;
	$MyPropiedad = new PROPIEDAD;
	
	if(!empty($id_predio))
	{
	
		if($MyPropiedad->GuardaNuevoPropiedad($id_predio,strtoupper($razon),strtoupper($nom),strtoupper($ape_pat),strtoupper($ape_mat),$folio_td) == PROP_SUCCESS)
		{
			$http_vars["MsgSas"]= "El propietario $razon $nom $ape_pat $ape_mat se agrego corectamente ";
		}
		else
		{
			$http_vars["MsgErr"] = "Error al agregar el propietario $razon $nom $ape_pat $ape_mat";
		}
	}
	$_SESSION["cookie_http_vars"] = $http_vars;
	$respuesta->addScript("document.nuevo_prop.submit();");
	return $respuesta;
}
function BuscarCalle($id,$div,$div1)
{
	$respuesta = new xajaxResponse('UTF-8');
	$consultas = new CONSULTAS;
	$campos = array("calle.calles","tipo_calle.nom_tipo_calle");
	$condicion=array("calle.tipo_calle=tipo_calle.id_tipo_calle and id_calle"=>$id,
			"status_calle"=>"1","status_tipo_calle"=>"1");
	if($consultas->SeleccionarTablaFila("tipo_calle,calle",$campos,$condicion, "", "")==CONSULTAS_SUCCESS)
	{
		while($registro =$consultas->Fetch())
		{
                	$nombre		= $registro["calles"];
                	$tipo		= $registro["nom_tipo_calle"];
			$html.="<input type='hidden' id='calle' name='calle' value='$nombre' /> $nombre";
			$html1.="$tipo <input type='hidden' id='calle_tipo' name='calle_tipo' value='$tipo' />";
                }
        }

	$respuesta->addAssign("$div","innerHTML",$html);
	$respuesta->addAssign("$div1","innerHTML",$html1);
	return $respuesta;
}
function FiltrarCalle($zona,$tipo_calle)
{
	$respuesta = new xajaxResponse('UTF-8');
	$respuesta->addScript("console.log($tipo_calle,$zona);");
	$respuesta->addScript("$('#SelectCalle').html(\"".SelectCalle('calle','','','Selecciona...',$zona,$tipo_calle)."\");");
	return $respuesta;
}
function Filtrar_tipo_Calle($id,$funcion,$value,$caption,$zona,$tipo_calle)
{
	$respuesta = new xajaxResponse('UTF-8');
	$html = Select_Tipo_Calle($id,'',$value,$caption,$zona);
	$respuesta->addAssign("SelectCalle","innerHTML",$html);
	return $respuesta;
}

function BusqClave($clave,$div,$div2='')
{
	$respuesta = new xajaxResponse('UTF-8');
	$MyPredial = new PREDIAL;
	if(strlen($clave)==1){
		$clave=$clave."000000000000";
	}if(strlen($clave)==2){
		$clave=$clave."00000000000";
	}if(strlen($clave)==3){
		$clave=$clave."0000000000";
	}if(strlen($clave)==4){
		$clave=$clave."000000000";
	}if(strlen($clave)==5){
		$clave=$clave."00000000";
	}if(strlen($clave)==6){
		$clave=$clave."0000000";
	}if(strlen($clave)==7){
		$clave=$clave."000000";
	}if(strlen($clave)==8){
		$clave=$clave."00000";
	}if(strlen($clave)==9){
		$clave=$clave."0000";
	}if(strlen($clave)==10){
		$clave=$clave."000";
	}if(strlen($clave)==11){
		$clave=$clave."00";
	}if(strlen($clave)==12){
		$clave=$clave."0";
	}
	global $MyPredial;
	if(!empty($clave))
	{
		$clave="087$clave";
		$collection = $MyPredial->ColeccionClave('','1','1' ,'id','ASC', $clave);
		if($collection != CLAV_ERROR)
		{
			$id 			= $MyPredial->getID($collection[0]);
			if($id)
			{
				$ant_clave		= $MyPredial->getAnt_Clave($id);
			        $colonia		= $MyPredial->getColonia($id,'interface');
			        $domicilio		= $MyPredial->getDom($id);
			        $calle			= $MyPredial->getCalle($id);
			        $mz_p			= $MyPredial->getMz_p($id);
			        $smz_p			= $MyPredial->getSMz_p($id);
			        $lt_p			= $MyPredial->getLt_p($id);
			        $depto_p		= $MyPredial->getDepto_p($id);
			        $edif_p			= $MyPredial->getEdif_p($id);
				$numero			= $MyPredial->getNum($id);
				$num_i			= $MyPredial->getNum_i($id);
				$cp			= $MyPredial->getCP($id);
				$foto			= $MyPredial->getFoto($id);
				$d_foto			= $MyPredial->getD_Foto($id);
				$plano			= $MyPredial->getPlano($id);
				$notificacion		= $MyPredial->getNotificacion($id);
			        $bloqueo		= $MyPredial->getBloqueo($id);
				$tipo			= $MyPredial->getTipo($id,'interface');
				$tipo_i			= $MyPredial->getTipo($id,'interface');
				$obs_dam		= $MyPredial->getObs_dam($id);
				$obs_sis		= $MyPredial->getObs_sis($id);
				$ext_1			= $MyPredial->getExt_1($id);
				$ext_2			= $MyPredial->getExt_2($id);
				$ext_3			= $MyPredial->getExt_3($id);
				
				$html="<input type='hidden' name='clave' id='clave' value=\"$id\" />
					<table border=\"0\" align='center' width=\"100%\">
					<tr>
					<td align='center' colspan='2' class=\"DatProp\" >Colonia:</td>
					<td align='center' colspan='6'  style=\" height: 40px;\" class=\"datos\">$colonia";
				$html.="</td></tr><tr>
					<td align='center' colspan='2' class=\"DatProp\" >Calle:</td>
					<td align='center' colspan='6' style=\"height: 40px;\" class=\"datos\">$calle</td></tr>
					<tr height=\"40px\" >
					<td align='center' width=\"100px\" class=\"DatProp\" >Mz.</td>
					<td align='center'  width=\"100px\" class=\"datos\">$mz_p
					</td>
					<td align='center' width=\"100px\" class=\"DatProp\" >Lt.</td>
					<td align='center'  width=\"100px\" class=\"datos\">$lt_p
					</td>
					<td align='center' width=\"100px\" class=\"DatProp\">N. Ext:</td>
					<td align='center'  width=\"100px\" class=\"datos\">$numero
					</td>
					<td align='center' width=\"100px\" class=\"DatProp\" >N. Int:</td>
					<td align='center'   width=\"100px\" height=\"40px\" class=\"datos\">$num_i
					</td></tr>
					<tr height=\"40px\">
					<td align='center' width=\"100px\" class=\"DatProp\" >Depto.</td>
					<td align='center'  width=\"100px\" class=\"datos\">$depto_p
					</td>
					<td align='center' width=\"100px\" class=\"DatProp\" >Edif.</td>
					<td align='center'   width=\"100px\" class=\"datos\">$edif_p
					</td>
					<td align='center' width=\"100px\" class=\"DatProp\" >Sup. Mz.</td>
					<td align='center'  width=\"100px\" height=\"40px\" class=\"datos\">$smz_p
					</td><td align='center' width=\"100px\" class=\"DatProp\">Cod. Post.</td>
					<td align='center'   width=\"100px\" class=\"datos\">$cp
					</td></tr></table >";
				$html1=$clave;
			}else{
				$html="<input type='hidden' name='clave' id='clave' value=\"$id\" />
				<table style=\" width: 100%;\" ><tr>	
				<td style=\"text-align: center\">No existe la clave</td>
				</tr>
				</table>";
			}
		}else{
			$html="<input type='hidden' name='clave' id='clave' value=\"$id\" />
				<table style=\" width: 100%;\" ><tr>	
				<td style=\"text-align: center\">No existe la clave</td>
				</tr>
				</table>";
		}
	}else{
		$html="<input type='hidden' name='clave' id='clave' value=\"$id\" />
				<table style=\" width: 100%;\" ><tr>	
				<td style=\"text-align: center\">No existe la clave</td>
				</tr>
				</table>";
	}

	$respuesta->addAssign("$div","innerHTML",$html);
	$respuesta->addAssign("$div2","innerHTML",$html1);
	return $respuesta;	
}
?>
