<?php
include_once("php/class/class.consultas.php");
include_once("php/class/class.usuarios.php");
include_once("php/util.php");
include_once("ajax.common.php");

function EliminarUser($id)
{
	$respuesta = new xajaxResponse('UTF-8');
	$MyUser = new USERS;
	$MyUser->Id(Sanitizacion($id));
	if($MyUser->EliminarUsuario() == USERS_SUCCESS)
	{
		$msg = "El usuario se elimino correctamente";
		$color='#659265';
	}
	else
	{
		$msg = "Error al eliminar el usuario";
		$color='#a90329';

	}
	$script="$.smallBox({
				title : 'Eliminar Usuario',
				content : '$msg',
				color : '$color',
				iconSmall : 'fa fa-check fa-2x fadeInRight animated',
				timeout : 4000
			});
			$('#row-$id').fadeOut('slow');";
	$respuesta->addScript($script);
	return $respuesta;
}

$xajax->registerFunction("EliminarUser");
?>
