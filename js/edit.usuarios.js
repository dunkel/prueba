$(document).ready(function(){
	var $orderForm = $("#users").validate({
		// Rules for form validation
		rules : {
			email: {
				required: true,
				email:true,
			},
			contrasena: {
				rangelength : [4,20]
			},
			contrasena1: {
				equalTo: '#contrasena'
			},
			nombre: {
				required:true
			},
			ape_pat: {
				required:true
			},
			ape_mat: {
				required:true
			},
			area: {
				required:true
			},
			nivel: {
				required:true
			}
		},

		// Messages for form validation
		messages : {
			email : {
				required : 'Introduca un correo',
				email : 'Introduce un correo valido'
			},
			contrasena : {
				rangelength : 'Introduca una cantraseña que este entre 4 y 10 carates'
			},
			contrasena1 : {
				equalTo : 'Repite la misma contraseña'
			},
			nombre : {
				required : 'Introdusca el nombre'
			},
			ape_pat : {
				required : 'Introdusca el apellido paterno'
			},
			ape_mat : {
				required : 'Introdusca el apellido materno'
			},
			area : {
				required : 'Selecione una area'
			},
			nivel : {
				required : 'Selecione un nivel'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
});
