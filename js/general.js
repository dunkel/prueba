$(document).ready(function(){
//	var cajera = $("#nombrecajera").val();
//	var area = $("#area").val();
//	var fecha = $("#fechas").val();
//	var title = $("title").html().split('-');
//	var ccabecera = '<table width="100%" cellspacing="0" cellpadding="0" border="0">'+
//	'<tbody><tr><th width="10%" height="80" align="center" colspan="1"></th>'+
//	'<th width="80%" height="80" colspan="3" style="text-align: center">H. AYUNTAMIENTO DE NEZAHUALCOYOTL<br>TESORERIA MUNICIPAL<br>'+title[1].toUpperCase()+'</th>'+
//	'<th width="10%" height="80" align="center" colspan="1"></th></tr><tr>'+
//	'<th width="10%" height="80" align="center" colspan="1"></th>'+
//	'<th width="24%" height="80" align="center" colspan="1">Area:'+area+'</th>'+
//	'<th width="23%" height="80" align="center" colspan="1" id="fecha_imp">Fecha de:'+fecha+'</th>'+
//	'<th width="23%" height="80" align="center" colspan="1">Cajera: '+cajera+'</th>'+
//	'<th width="10%" height="80" align="center" colspan="1"></th>'+
//	'</tr></tbody></table>';
	var title ='';
	var ccabecera ='';
	var responsiveHelper_datatable_tabletools = undefined;
	var urlAjax = "reporte/general/lista.ajax.general.php";
	var otable = $('#datatable_tabletools').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'><'col-sm-6 col-xs-12'p>>",
		"aoColumns": [
			{"mData": 'nombre_clasif_pago',"bSearchable":false},
			{"mData": 'cuenta_contable',"bSearchable":false},
			{"mData": "numero","bSearchable":false},
			{"mData": "sum_imp","bSearchable":false},
			{"mData": "sum_dre","bSearchable":false},
			{"mData": "sum_apo","bSearchable":false},
			{"mData": 'sum_rec',"bSearchable":false},
			{"mData": "sum_act","bSearchable":false},
			{"mData": "sum_iva","bSearchable":false},
			{"mData": "sum_e_im","bSearchable":false},
			{"mData": 'sum_e_d',"bSearchable":false},
			{"mData": "sum_e_r","bSearchable":false},
			{"mData": "sum_e_apo","bSearchable":false},
			{"mData": "sum_e_a","bSearchable":false},
			{"mData": "total_pag","bSearchable":false},			
			],
			
		"iDisplayLength"    : 1000000000,
		"bProcessing"       : true,
		"bServerSide"       : true,
		"bFilter"           : true,
		"bDestroy"          : true,
		"sAjaxSource"       : urlAjax,
		"sServerMethod"     : "POST",
		"sPaginationType"   : "full_numbers",
	        "oTableTools": {
	        	 "aButtons": [
		             "copy",
		             "csv",
		             "xls",
		               {
		                    "sExtends": "pdf",
		                    "sTitle": title.toUpperCase(),
		                    "sPdfMessage": ccabecera,
		                    "sPdfSize": "letter"
		                },
	                	{
				    "sExtends": "text",
				    "sButtonText": "Imprimir",
				    "sTitle": title.toUpperCase(),
				    "sMessage": ccabecera, 
				    "fnClick": function( nButton, oConfig ) {
				        this.fnPrint( true, oConfig );
				    	$('#fecha_imp').html('Fecha de:'+$('#dateselect_filter').val());
				    },
				    "bShowAll": false,
				    "bFooter": false,
			            	"bHeader": false,
				}
		             ],
		            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		},
		"autoWidth" : true,
		"preDrawCallback" : function() {
			if (!responsiveHelper_datatable_tabletools) {
				responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
				responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
				responsiveHelper_datatable_tabletools.respond();
		},
		"oLanguage": {
		    "sFirst"        : "Primera página",
		    "sProcessing"   : "Cargando información...",
		    "sSearch"       : "Buscar",
		    "sZeroRecords"  : "No se ha encontrado ningún resultado.",
		    "sNext"         : "Siguiente",
		    "sInfo"         : "Mostrando del _START_ a la _END_ de un total de  _TOTAL_",
		    "sLengthMenu"   : 'Mostrar <select class="input-small">'+
		    '<option value="1000000000">Todos</option>'+
		    '</select>',
		    "oPaginate"     : {
		        "sFirst"    : "Primera",
		        "sNext"     : "Siguiente",
		        "sPrevious" : "Anterior",
		        "sLast"     : "Última"
		    }
		},
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			// Remove the formatting to get integer data for summation
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			var columnas = api.column();
			if(!$.isEmptyObject(data)){
				$.each(columnas, function( index, value ) {
					$.each(value, function( llave, valor ) {
						if(llave>1){
							var total = api
							.column( llave )
							.data()
							.reduce( function (a, b) {
								return intVal(a) + intVal(b);
							} );
							if(llave>2){
								$( api.column( llave ).footer() ).html(
									'$'+ total +''
								);
							}else{
								$( api.column( llave ).footer() ).html(
									 total
								);
							}
						}
					});
				});
			}else{
				$.each(columnas, function( index, value ) {
					$.each(value, function( llave, valor ) {
						if(llave>1){
							$( api.column( llave ).footer() ).html('');
						}
					});
				});
			}
		}
	});
	$("#datatable_tabletools thead th input[type=text]").on( 'keyup change', function () {    	
	        otable
	            .column( $(this).parent().index()+':visible' )
	            .search( this.value )
	            .draw();
			$('#fechas').val(this.value);
			$('#fecha_imp').html('Fecha de:'+this.value);       
	});
	$("#datatable_tabletools thead th select").on( 'keyup change', function () {    	
	        otable
	            .column( $(this).parent().index()+':visible' )
	            .search( this.value )
	            .draw();   
	});
});
