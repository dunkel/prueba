$(document).ready(function() {
    var $orderForm = $("#users").validate({rules: {usuario: {required: true, rangelength: [6, 20], remote: {url: "php/users/validate.users.php", type: "post", async: false, data: {usuario: function() {
                            return $("#usuario").val();
                        }}}, pattern: /^[a-zA-Z0-9\.\-\_]+$/}, email: {required: true, email: true, }, password: {rangelength: [4, 10]}, password1: {equalTo: '#password'}, nombre: {required: true}, ape_pat: {required: true}, ape_mat: {required: true}, cp: {required: true}, nivel: {required: true}, municipio: {required: true}, estado: {required: true}, asentamiento: {required: true}, calle: {required: true}, numero: {required: true}}, messages: {usuario: {required: 'Introduca un nombre de usuario', remote: jQuery.format("El usuario ya existe"), pattern: jQuery.format("El nombre de usuario contiene caracteres no permitidos"), rangelength: 'El usuario deve tener entre 6 y 20 caracteres'}, email: {required: 'Introduca un correo', email: 'Introduce un correo valido'}, password: {required: 'Introduca una cantraseña'}, password1: {equalTo: 'Repite la misma contraseña'}, nombre: {required: 'Introdusca el nombre'}, ape_pat: {required: 'Introdusca el apellido paterno'}, ape_mat: {required: 'Introdusca el apellido materno'}, area: {required: 'Selecione una area'}, nivel: {required: 'Selecione un nivel'}, cp: {required: 'Introdusca el C.P.'}, estado: {required: 'Introdusca el Estado'}, municipio: {required: 'Introdusca el Muncipio'}, calle: {required: 'Introdusca la Calle'}, numero: {required: 'Introdusca el Número'}}, errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }, submitHandler: function(form) {
            form.submit();
        }});
    var responsiveHelper_datatable_tabletools = undefined;
    var urlAjax = "php/users/lista.ajax.usuarios.php";
    var otable = $('#datatable_tabletools').DataTable({"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-6 hidden-xs'T>r>" + "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>", "aoColumns": [{"mData": 'usuario'}, {"mData": 'mail'},{"mData": "apellido_pat"}, {"mData": "nivel"}, {"mData": "eliminar", "bSortable": false, "bSearchable": false}, {"mData": "editar", "bSortable": false, "bSearchable": false}], "iDisplayLength": 10, "bProcessing": true, "bServerSide": true, "bFilter": true, "bDestroy": true, "sAjaxSource": urlAjax, "sServerMethod": "POST", "sPaginationType": "full_numbers", "aaSorting": [[0, "desc"]], "oTableTools": {"aButtons": ["copy", "csv", "xls", {"sExtends": "pdf", "sTitle": "SmartAdmin_PDF", "sPdfMessage": "SmartAdmin PDF Export", "sPdfSize": "letter"}, {"sExtends": "print", "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>","bShowAll": false}], "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"}, "autoWidth": true, "preDrawCallback": function() {
            if (!responsiveHelper_datatable_tabletools) {
                responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
            }
        }, "rowCallback": function(nRow) {
            responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        }, "drawCallback": function(oSettings) {
            responsiveHelper_datatable_tabletools.respond();
        }, "oLanguage": {"sFirst": "Primera página", "sProcessing": "Cargando información...", "sSearch": "Buscar", "sZeroRecords": "No se ha encontrado ningún resultado.", "sNext": "Siguiente", "sInfo": "Mostrando del _START_ a la _END_ de un total de _TOTAL_ ", "sLengthMenu": 'Mostrar <select class="input-small">' + '<option value="10">10</option>' + '<option value="25">25</option>' + '<option value="50">50</option>' + '<option value="100">100</option>' + '</select>', "sInfoFiltered": " - filtrado de _MAX_ elementos", "oPaginate": {"sFirst": "Primera", "sNext": "Siguiente", "sPrevious": "Anterior", "sLast": "Última"}}});
    $("#datatable_tabletools thead th input,#datatable_tabletools thead th select").on('keyup change', function() {
        otable.column($(this).parent().index() + ':visible').search(this.value).draw();
    });
});
function ConfEliminarUser(id) {
    $.SmartMessageBox({title: "¿desea borrar este registro?", buttons: "[Aceptar][Cancelar]"}, function(ButtonPress, Value) {
        if (ButtonPress == "Cancelar") {
            return 0;
        } else {
            xajax_EliminarUser(id);
        }
    });
}
