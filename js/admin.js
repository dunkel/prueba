function ValidaFormUser()
{
	var obj=document.getElementById('users');
	var controles = Array(
	Array("usuario",  	"Usuario"),
  	Array("email",		"E-mail")
	);

	var mails = Array(
	Array("email",		"E-mail")
	);

	var respuesta=ValidaFormularios(controles);

	if(respuesta == 0)
	{
  		return false;
	}

	var checkOK = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz-_.";
  	var checkStr = obj.usuario.value;
  	var allValid = true;

	for (i = 0; i < checkStr.length; i++)
	{
   		ch = checkStr.charAt(i);
   		for (j = 0; j < checkOK.length; j++)
    	if (ch == checkOK.charAt(j))
    	break;
   		if (j == checkOK.length)
		{
   			allValid = false;
   			break;
  		}
  	}

	if (!allValid)
	{
  		alert("El nombre de usuario contiene caracteres no permitidos");
    		obj.usuario.focus();
   		return false;
	}

	var respuesta = ValidaMails(mails);
	if(respuesta == 0)
	{
		return false;
	}

	if(obj.contrasena.value != obj.contrasena1.value)
	{
	 	alert("Las contrase&ntilde;as son diferentes");
	 	obj.contrasena.focus();
	 	return false;
	}

	return true;
}
function ConfEliminarUser(id)
{
	valor=confirm('¿Borrar este registro?');
	if(valor==true)
	{
		xajax_EliminarUser(id);
	}
}

function mostrar(blo,off,on)
{
	OCULTO="none";
	VISIBLE="block";
	if(blo != "")
	{
		document.getElementById(blo).style.display=VISIBLE;

	}
	if(on != "")
	{
		document.getElementById(on).style.display=OCULTO;
	}
	if(off != "")
	{
		document.getElementById(off).style.display=VISIBLE;
	}
}
function ocultar(blo,off,on)
{

	OCULTO="none";
	VISIBLE="block";
	if(blo != "")
	{
		document.getElementById(blo).style.display=OCULTO;
	}
	if(off != "")
	{
		document.getElementById(off).style.display=OCULTO;
	}
	if(on != "")
	{
		document.getElementById(on).style.display=VISIBLE;
	}
}
function VerSelect()
{
	var concep = document.diversos.concep.value;
	xajax_ClaveConcepto(concep);
	xajax_CostoConcepto(concep);
}
function insert(obj,destino)
{
  if (obj.value.length==13) destino.focus();
}
