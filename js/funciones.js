// DO NOT REMOVE : GLOBAL FUNCTIONS!
$(document).ready(function() {
	pageSetUp();
	jQuery.validator.addMethod("pattern", function(value, element, param) {
	  if (this.optional(element)) {
	    return true;
	  }
	  if (typeof param === 'string') {
	    param = new RegExp('^(?:' + param + ')$');
	  }
	  return param.test(value);
	}, "Invalid format.");
})
var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
function ValidaFormularios(controles)
{
	for ( i = 0, n = controles.length; i < n; i++ )
 	{
	var iControl = controles[i];
  	var e = document.getElementById(iControl[0]);
  		if (e)
  		{
   			if ( e.value.length == 0 )
   			{
    			alert("El campo " + iControl[1] + " esta  vacio.");
    			e.focus();
    			return 0;
   			}
  		}
 	}
}

function ValidaMails(mails)
{
	var filtro = /^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;
	
	for ( i = 0, n = mails.length; i < n; i++ )
 	{
		var iMail = mails[i];
  		var e = document.getElementById(iMail[0]);
		if (e)
  		{
   			if ( e.value.length > 0 )
   			{
				var s = e.value;
				if (!filtro.test(s))
				{
					alert("El campo " + iMail[1] + " no es valido.");
    				e.focus();
    				return 0;
				}
			}			
   		}
  	}	
}

function ValidaURL(url) {
var re=/^(http:|https:|ftp:)\/\/\w+(\.\w+)*(\-\w+)?\.\w{2,3}(\:\d{2,6})?(\/{1,2}(\:|\-|\w|\.|\?|\/|\=|\&|\%|\@|\\|\,)*)?$/;
return re.test(url);
}


function Numeros(e)
{
	var key = (document.all) ? e.keyCode : e.which;
	if (key < 48 || key > 57)
	{
		return false;
	}
	else
	{
		return true;	
	}
}

function PaginacionSubmit(i,por,order)
{
	document.paginar.page.value=i;
	document.paginar.por.value=por;
	document.paginar.order.value=order;
	document.paginar.submit();
	
}

function SubmitID(id, command)
{
	var myForm = document.getElementById('formid');	
	myForm.command.value = command;
	myForm.id.value = id;
	myForm.submit();
}
function SubmitIDComer(id,us, command)
{
	var myForm = document.getElementById('formid');	
	myForm.command.value = command;
	myForm.id.value = id;
	myForm.us.value = us;
	myForm.submit();
}
function SubmitIDInd(id,id_ciclo,zona,mz,lt,ano_ciclo,command)
{
	var myForm = document.getElementById('formidind');	
	myForm.command.value 	= command;
	myForm.id_predio.value 	= id;
	myForm.id_ciclo.value 	= id_ciclo;
	myForm.zona.value 	= zona;
	myForm.mz.value 	= mz;
	myForm.lt.value 	= lt;
	myForm.ano_ciclo.value 	= ano_ciclo;
	myForm.submit();
}
/* DIVS DESPLEGABLE */

function mostrar(blo,off,on)
{
	OCULTO="none";
	VISIBLE="block";
	if(blo != "")
	{
		$('#'+blo).css('display',VISIBLE);
	}
	if(on != "")
	{
		$('#'+on).css('display',OCULTO);
	}
	if(off != "")
	{
		$('#'+off).css('display',VISIBLE);
	}
}
function ocultar(blo,off,on)
{
	OCULTO="none";
	VISIBLE="block";
	if(blo != "")
	{
		$('#'+blo).css('display',OCULTO);
	}
	if(off != "")
	{
		$('#'+off).css('display',OCULTO);
	}
	if(on != "")
	{
		$('#'+on).css('display',VISIBLE);
	}
}


/////////////////Login/////////////////////////////
function ValidaLogin()
{
	var obj = document.getElementById('autentificacion');
	
	var controles = Array(
  	Array("usuario",   	"Usuario"),
  	Array("password",	"Contrase�a")
	);
	
	var respuesta = ValidaFormularios(controles);
 	
	if(respuesta == 0)
	{
		return false;
	}

	return true
}


function ValidaEmail()
{
	var obj = document.getElementById('forgotp');
	
	var controles = Array(
  	Array("user",   	"Usuario"),
  	Array("mail",		"E-mail")
	);
	
	var mails = Array(
  	Array("mail",		"E-mail")
	);
	
	var respuesta = ValidaFormularios(controles);
 	if(respuesta == 0)
	{
		return false;
	}
	
	var respuesta = ValidaMails(mails);
 	if(respuesta == 0)
	{
		return false;
	}
	return true
}

function popUp(URL) 
{
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=660,height=420,left = 262,top = 259');");
}
function OpWif(uri,options)
{
	var wOpenRef ;
	var name = Math.floor(Math.random()*1001);
	wOpenRef = window.open(uri,name,options);
}
function Ver(form,nombre,div)
{
    var ch = document[form][nombre].checked;
    if(ch == false)
    {
        ocultar(div,'','');
    }else{
        mostrar(div,'','');
    }
}
function contar(obj,destino) 
{
  if (obj.value.length==obj.maxLength) destino.focus();
}
function redondear(num)
{ 		
	var original=parseFloat(num);
	if ((original*100%100)>=0.5)
	{
		var result=Math.round(original*100)/100+0.01;
	}
	else
	{
		var result=Math.round(original*100)/100; 		
	}
	return parseFloat(result);
}
function BuscarNombre(div)
{
	var nomb	= document.busq_n.nombre_b.value;
	var ape_p	= document.busq_n.ape_pat_b.value;
	var ape_m	= document.busq_n.ape_mat_b.value;
	var calle	= document.busq_n.calle_b.value;
	var razon	= document.busq_n.razon_b.value;
	xajax_BuqNombre(nomb,ape_p,ape_m,calle,razon,div);
	return false;
}
function ConfEliminarClave(clave)
{
	valor=confirm('�Seguro que desea BORRAR esta Clave '+clave+'?');
	if(valor==true)
	{
		return true;
	}
	return false;
}
function SubmitIDC(id, command,fecha,cajera)
{
	var myForm = document.getElementById('formid');	
	myForm.command.value = command;
	myForm.id.value = id;
	myForm.fecha.value = fecha;
	myForm.cajera.value = cajera;
	myForm.submit();
}
function buscarcalle(value)
{
	if(value){
		xajax_BuscarCalle(value,'id_calle','tipo_calle');
	}
}
function conMayusculas(field) {
            field.value = field.value.toUpperCase()
}
