/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.5.47-0ubuntu0.14.04.1-log : Database - prueba
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`prueba` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `prueba`;

/*Table structure for table `logdatabase` */

DROP TABLE IF EXISTS `logdatabase`;

CREATE TABLE `logdatabase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabla` varchar(150) NOT NULL,
  `id_rel` int(11) NOT NULL,
  `type` varchar(150) NOT NULL,
  `detalle` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `usuario` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `mac` varchar(17) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_r` (`id_rel`),
  KEY `fecha_log` (`fecha`),
  KEY `usuario` (`usuario`),
  KEY `typ` (`type`),
  KEY `table` (`tabla`),
  KEY `ip` (`ip`),
  KEY `mac` (`mac`),
  CONSTRAINT `logdatabase_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `logdatabase` */

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(18) DEFAULT NULL,
  `passwd` varchar(50) DEFAULT NULL,
  `nombre` varchar(250) NOT NULL,
  `apellido_pat` varchar(250) NOT NULL,
  `apellido_mat` varchar(250) DEFAULT NULL,
  `mail` varchar(150) NOT NULL,
  `cp` int(5) unsigned zerofill DEFAULT NULL,
  `estado` varchar(250) DEFAULT NULL,
  `municipio` varchar(250) DEFAULT NULL,
  `asentamiento` varchar(250) DEFAULT NULL,
  `calle` varchar(250) DEFAULT NULL,
  `numero` varchar(250) DEFAULT NULL,
  `nivel` int(1) NOT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ultimoacceso` date DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `PASSWD` (`passwd`),
  KEY `NIVEL` (`nivel`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`usuario`,`passwd`,`nombre`,`apellido_pat`,`apellido_mat`,`mail`,`cp`,`estado`,`municipio`,`asentamiento`,`calle`,`numero`,`nivel`,`fecha`,`ultimoacceso`,`status`) values (1,'dunkel','06e95112ae112198a7103d9aac2f336a7660649d','JULIO','SANCHEZ','RUIZ','dunkel015@hotmail.com',55120,'Mexico','ecatepec','ecatepec','zacatepetl','36',2,'2016-03-11 20:55:17','2016-03-11',1),(2,'maribel','b6ed1911291a48247dc52dbf53fe79fa1d2112f6','MARIBEL','MORALES','CORONA','marobeth14@hotmail.com',55120,'estado de mexico','ecatepec','la florida','zacatepetl','36 c',2,'2016-03-11 22:18:52',NULL,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
