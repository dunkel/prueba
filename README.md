INGENIA AGENCY
   Prueba Práctica Back-End PHP 
   Tiempo Estimado: 4 - 6 horas.
 ----------------------------------------------------*/

############################################################   
 BACK-END (PHP)
############################################################ 
 
Se deberá desarrollar una aplicación que permita crear una cuenta de usuario, la aplicación deberá considerar las siguientes funcionalidades
Alta de cuenta de usuario
Inicio de sesión
Modificar información de la cuenta de usuario
Eliminar cuenta de usuario
La información que la aplicación es la siguiente:
Nombre(s)
Apellidos
Correo electrónico
Dirección
Código postal
Estado
Municipio
Asentamiento (colonia, pueblo, barrio, etc)
Calle
Numero
Contraseña
Toda la información es obligatoria


 * Se deberá desarrollar el servicio que reciba los datos de los formularios y los almacene en una Base de Datos.
 
* El aspirante deberá definir la estructura de dicha Base de Datos.

* La Base de Datos deberá estar definida en MySQL.

* La aplicación deberá ser sumamente segura
   
############################################################
 CONSIDERACIONES TÉCNICAS Y DE DISEÑO
############################################################

* Es de la elección del aspirante el definir qué DOCTYPE utilizará para
   la codificación HTML de la maqueta.
 
* Queda a elección del aspirante el definir cómo mostrar los mensajes de error correspondientes y cómo será el envío de información con los servicios de Back-End.

* El aspirante puede definir que métricas de seguridad utilizar para el desarrollo de la aplicación.

* El aspirante puede utilizar algún framework de desarrollo en PHP

* El aspirante puede utilizar MVC o programación estructurada.

 * El tiempo estimado para realizar la prueba en su totalidad está planeado entre 4 a 6 horas.
   
############################################################
 ENTREGABLES
############################################################

 * La prueba deberá contemplar la siguiente estructura:
 
   /_
     |_/css (Capeta que contiene definiciones de estilos y elementos gráficos de presentación)
     |    |_/fonts (Carpeta que contiene las Web Fonts utilizadas en el proyecto)
     |    |_/img (Carpeta que contiene las imágenes utilizadas para la presentación)
     |
     |_/js (Carpeta que contiene archivos con funcionalidad JavaScript)
     |
     |_/php (Carpeta que contiene archivos con funcionalidad PHP)
     |
     |_/sql (Carpeta que deberá contener el archivo .sql con la definición de la BD utilizada)
     |
     |_index file (Punto de acceso a la página-sistema)