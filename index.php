<?php
session_start();
include_once('php/class/class.debug.php');
$MyDebug->SetDebug(0);
$MyDebug->DebugError();
include_once('php/utiles.php');
include_once('php/util.php');
include_once('php/class/class.consultas.php');
include_once('php/class/class.login.panel.php');
include_once('php/class/class.logic.panel.php');
include_once('php/class/class.uploader.php');
include_once('php/class/class.correo.php');
include_once("php/class/class.log.db.php");
$command = isset($_POST['command'])	? 	$_POST['command'] 	: $_GET['command'];
$MyLogic->Logic($command);
include_once($MyLogic->MyAjaxFile());
include_once(PATH_AJAX."ajax.requests.php");
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php print ("PANEL DE CONTROL - ".$MyLogic->Title($command)); ?></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support is under construction
			 This RTL CSS will be released in version 1.5
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>smartadmin-rtl.min.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo PATH_CSS;?>demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="screen and (max-device-width: 320px)">
		<?php

			if($MyLogic->MyCSSFile() != "")
			{
				if(is_array($MyLogic->MyCSSFile())){
					foreach ($MyLogic->MyCSSFile() as $key => $link_css)
					{
						print ("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"".PATH_CSS.$link_css."\">\n");
					}
				}else{
					print ("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"".PATH_CSS.$MyLogic->MyCSSFile()."\">\n");
				}
			}

		?>
</head>
<body class="smart-style-3 ">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<?php
	if($MyLogic-> MyCommand()!=LOGIN){
		include_once("php/header.php");
		include_once("php/menu.php");
?>
	<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href='index.php'>Home</a></li><?php
					if(!empty($breadcrumb)){
						echo $breadcrumb;
					}
					?>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->
			<div id="content">
<?php

		include_once("php/getcookiedata.php");
		include_once($MyLogic->MyPHPFile());
?>
			</div>
		</div>
		<!-- END MAIN PANEL -->
<?php
		include_once("php/footer.php");
	}else{
		include_once($MyLogic->MyPHPFile());
	}

?>
<!--================================================== -->
<?php
$MyDebug->Dump();
?>
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo PATH_JS;?>plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script>
	if (!window.jQuery) {

		document.write('<script src="<?php echo PATH_JS;?>libs/jquery-2.0.2.min.js"><\/script>');
	}
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
	if (!window.jQuery.ui) {
		document.write('<script src="<?php echo PATH_JS;?>libs/jquery-ui-1.10.3.min.js"><\/script>');
	}
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo PATH_JS;?>app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo PATH_JS;?>plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo PATH_JS;?>bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo PATH_JS;?>notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo PATH_JS;?>smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo PATH_JS;?>plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo PATH_JS;?>plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo PATH_JS;?>plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo PATH_JS;?>plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo PATH_JS;?>plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo PATH_JS;?>plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo PATH_JS;?>plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo PATH_JS;?>plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="<?php echo PATH_JS;?>demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo PATH_JS;?>app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo PATH_JS;?>speech/voicecommand.min.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo PATH_JS;?>plugin/jquery-form/jquery-form.min.js"></script>

<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="<?php echo PATH_JS;?>plugin/flot/jquery.flot.cust.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/flot/jquery.flot.tooltip.min.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="<?php echo PATH_JS;?>plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo PATH_JS;?>plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/clockpicker/clockpicker.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/bootstrap-tags/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/noUiSlider/jquery.nouislider.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/ion-slider/ion.rangeSlider.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/knob/jquery.knob.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/x-editable/moment.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/x-editable/jquery.mockjax.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/x-editable/x-editable.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/typeahead/typeahead.min.js"></script>
<script src="<?php echo PATH_JS;?>plugin/typeahead/typeaheadjs.min.js"></script>

<!-- Full Calendar -->
<script src="<?php echo PATH_JS;?>plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

<script src="<?php echo PATH_JS;?>funciones.js" language='javascript' type='text/javascript'></script>
<?php
if($MyLogic->MyJSFile() != "")
{
	if(is_array($MyLogic->MyJSFile())){
		foreach ($MyLogic->MyJSFile() as $key => $link_js)
		{
			print ("<script src='".PATH_JS.$link_js."' language='javascript' type='text/javascript'></script>\n");
		}
	}else{
		print ("<script src='".PATH_JS.$MyLogic->MyJSFile()."' language='javascript' type='text/javascript'></script>\n");
	}
}
$xajax->printJavascript("xajax/");
?>
</body>
</html>
